package com.softbay.android.baseproject.ui.hilt

import androidx.activity.viewModels
import androidx.databinding.library.baseAdapters.BR
import baycon.android.common.di.SampleUtil
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityHiltBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HiltActivity: BaseActivity<ActivityHiltBinding, HiltViewModel>() {
    override val viewModel: HiltViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_hilt

    @Inject
    lateinit var sampleUtil: SampleUtil

    override fun initViewsAndEvents() {
        sampleUtil.sample()
    }
}