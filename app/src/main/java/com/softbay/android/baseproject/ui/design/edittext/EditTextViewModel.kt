package com.softbay.android.baseproject.ui.design.edittext

import androidx.lifecycle.MutableLiveData
import com.softbay.android.baseproject.base.BaseViewModel

class EditTextViewModel: BaseViewModel() {
    val displayInputText = MutableLiveData<String>()
}