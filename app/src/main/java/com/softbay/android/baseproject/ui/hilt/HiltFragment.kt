package com.softbay.android.baseproject.ui.hilt

import androidx.fragment.app.viewModels
import baycon.android.common.di.SampleUtil
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseFragment
import com.softbay.android.baseproject.databinding.ActivityHiltBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HiltFragment: BaseFragment<ActivityHiltBinding, HiltViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_hilt

    override val viewModel: HiltViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    @Inject
    lateinit var sampleUtil: SampleUtil

    override fun initViewsAndEvents() {
        sampleUtil.sample()
    }

}