package com.softbay.android.baseproject.ui.design.button

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.softbay.android.baseproject.base.BaseViewModel

class ButtonViewModel: BaseViewModel() {

    val displayState = MutableLiveData(false)

    fun changeState() {
        displayState.value = !(displayState.value ?: true)
    }

}