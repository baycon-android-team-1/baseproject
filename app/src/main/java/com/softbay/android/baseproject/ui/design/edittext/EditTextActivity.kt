package com.softbay.android.baseproject.ui.design.edittext

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityEditTextBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EditTextActivity : BaseActivity<ActivityEditTextBinding, EditTextViewModel>() {

    override val viewModel: EditTextViewModel by viewModels()

    override val bindingVariable: Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_edit_text

    override fun initViewsAndEvents() {
    }

}