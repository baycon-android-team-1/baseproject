package com.softbay.android.baseproject.ui.design

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.base.BaseViewModel
import com.softbay.android.baseproject.databinding.ActivityMenuBinding
import com.softbay.android.baseproject.ui.design.api.ApiActivity
import com.softbay.android.baseproject.ui.design.button.ButtonActivity
import com.softbay.android.baseproject.ui.design.contact.ContactActivity
import com.softbay.android.baseproject.ui.design.dialog.DialogActivity
import com.softbay.android.baseproject.ui.design.edittext.EditTextActivity
import com.softbay.android.baseproject.ui.design.notification.NotificationActivity
import com.softbay.android.baseproject.ui.design.recyclerview.RecyclerViewActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class MenuActivity : BaseActivity<ActivityMenuBinding, BaseViewModel>() {
    override val viewModel: BaseViewModel? = null

    override val bindingVariable: Int? = null

    override fun getLayoutId(): Int = R.layout.activity_menu

    override fun initViewsAndEvents() {

        dataBinding.toolbar.setSupportActionBar( this )

        dataBinding.run {

            btnDialog.setOnClickListener {
                startActivity(Intent(this@MenuActivity, DialogActivity::class.java))
            }

            btnEditText.setOnClickListener {

                startActivity(Intent(this@MenuActivity, EditTextActivity::class.java))

            }

            btnButton.setOnClickListener {

                startActivity(Intent(this@MenuActivity, ButtonActivity::class.java))

            }

            btnRv.setOnClickListener {

                startActivity(Intent(this@MenuActivity, RecyclerViewActivity::class.java))

            }

            btnContact.setOnClickListener {

                startActivity(Intent(this@MenuActivity, ContactActivity::class.java))

            }

            btnNoti.setOnClickListener {

                startActivity(Intent(this@MenuActivity, NotificationActivity::class.java))

            }

            btnApi.setOnClickListener {

                startActivity(Intent(this@MenuActivity, ApiActivity::class.java))

            }


        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menu ?: return false
        menuInflater.inflate( R.menu.menu_sample, menu )

        val menuItem = menu.findItem( R.id.sample_item )
        menuItem.title = "test"
        menuItem.isVisible = true

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when ( item.itemId ) {

            R.id.sample_item -> {
                Timber.i("click sample item 1")
                true
            }

            R.id.sample_item_2 -> {
                Timber.i("click sample item 2")
                true
            }

            android.R.id.home -> {
                Timber.i("click sample home (back press)")
                onBackPressed()
                return true
            }

            else -> {
                return super.onOptionsItemSelected(item)
            }

        }
    }

}