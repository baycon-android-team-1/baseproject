package com.softbay.android.baseproject.api.repository

import baycon.android.common.api.performOperation
import com.softbay.android.baseproject.api.contract.CommonDataSource
import com.softbay.android.baseproject.api.parse.ResultPostGet
import com.softbay.android.baseproject.api.parse.ResultPostPost

/** this is for sample not copied */
class CommonRepository( private val commonDataSource: CommonDataSource) {
    fun postGet(postId: Int, saveCall: suspend (ResultPostGet)->Unit) = performOperation(
        { commonDataSource.postGet( postId ) },
        { saveCall( it ) }
    )

    fun postPost(postId: Int, saveCall: suspend (ResultPostPost)->Unit) = performOperation(
        { commonDataSource.postPost ( postId ) },
        { saveCall( it ) }
    )

    fun postPut(postId: Int, saveCall: suspend (ResultPostPost)->Unit) = performOperation(
        { commonDataSource.postPut ( 1, "test", "test", postId ) },
        { saveCall( it ) }
    )
}