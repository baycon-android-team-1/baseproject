package com.softbay.android.baseproject.ui.design.api

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import baycon.android.common.model.BaseModel
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.api.parse.ResultPostGet
import com.softbay.android.baseproject.base.BaseViewHolder
import com.softbay.android.baseproject.databinding.ActivityApiItemBinding

class ApiAdapter: ListAdapter<BaseModel, BaseViewHolder>( CountryDiffUtil ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ActivityApiItemBinding>( inflater, R.layout.activity_api_item, parent, false )
        return ApiViewHolder(binding)

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBindView( getItem( position ) )
    }

    inner class ApiViewHolder( private val binding: ActivityApiItemBinding ): BaseViewHolder( binding.root ) {

        override fun onBindView(item: BaseModel) {

            binding.run {

                this.item = item as ResultPostGet.Post?

            }

        }

    }

    companion object CountryDiffUtil: DiffUtil.ItemCallback<BaseModel>(){
        override fun areItemsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            return oldItem==newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            return oldItem==newItem
        }
    }
}