package com.softbay.android.baseproject.ui.design.recyclerview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import baycon.android.common.model.BaseModel
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseViewHolder
import com.softbay.android.baseproject.base.HeaderViewHolder
import com.softbay.android.baseproject.base.ItemClickListener
import com.softbay.android.baseproject.databinding.ActivityRecyclerViewItemBinding
import com.softbay.android.baseproject.databinding.CommonHeaderBinding
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemDragListener
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemTouchHelperListener

class RecyclerViewListAdapter( private val itemClickListener: ItemClickListener, private val itemDragListener: ItemDragListener): ListAdapter<BaseModel, BaseViewHolder>( CountryDiffUtil ),
    StickyHeaderHandler, ItemTouchHelperListener {

    override fun getAdapterData(): MutableList<*> {
        return currentList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            BaseModel.HEADER -> {

                val binding = DataBindingUtil.inflate<CommonHeaderBinding>( inflater, R.layout.common_header, parent, false )
                HeaderViewHolder(binding)

            }
            else -> {

                val binding = DataBindingUtil.inflate<ActivityRecyclerViewItemBinding>( inflater, R.layout.activity_recycler_view_item, parent, false )
                RecyclerViewViewHolder(binding, itemDragListener)

            }

        }

    }

    override fun getItemViewType(position: Int): Int {
        return getItem( position ).viewType
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBindView( getItem(position) )
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {

        val items = ArrayList<BaseModel>()
        items.addAll( currentList )

        val target = items[fromPosition]

        items.removeAt( fromPosition )
        items.add( toPosition, target )

        submitList( items )

        return true
    }

    override fun onItemSwipe(position: Int) {
        submitList(currentList - getItem( position ))
    }


    inner class RecyclerViewViewHolder(private val binding: ActivityRecyclerViewItemBinding, private val onDragListener: ItemDragListener): BaseViewHolder( binding.root ) {
        @SuppressLint("ClickableViewAccessibility")
        override fun onBindView(item: BaseModel) {

            binding.run {

                this.item = item

                parent.setOnClickListener {

                    itemClickListener.onClick( it, absoluteAdapterPosition + 1, false )

                }

                ivDrag.setOnTouchListener { _, event ->
                    if(event.action == MotionEvent.ACTION_DOWN){
                        onDragListener.onStartDrag(this@RecyclerViewViewHolder)
                    }
                    false
                }

            }

        }

    }

    companion object CountryDiffUtil: DiffUtil.ItemCallback<BaseModel>(){
        override fun areItemsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            return oldItem==newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            return oldItem==newItem
        }
    }
}