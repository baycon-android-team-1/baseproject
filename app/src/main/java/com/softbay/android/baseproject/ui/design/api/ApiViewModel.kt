package com.softbay.android.baseproject.ui.design.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import baycon.android.common.api.Resource
import baycon.android.common.model.BaseModel
import baycon.android.common.utils.lifecycle.ListLiveData
import baycon.android.common.utils.lifecycle.SingleLiveEvent
import com.softbay.android.baseproject.api.parse.ResultPostGet
import com.softbay.android.baseproject.api.parse.ResultPostPost
import com.softbay.android.baseproject.api.repository.CommonRepository
import com.softbay.android.baseproject.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class ApiViewModel @Inject constructor( private val commonRepository: CommonRepository): BaseViewModel() {

    val displayValue = MutableLiveData(1)

    val startSearch = SingleLiveEvent<Any>()

    val startPost = SingleLiveEvent<Any>()

    val startPut = SingleLiveEvent<Any>()

    val items = ListLiveData<BaseModel>()

    fun clickStartSearch() {

        startSearch.call()

    }

    fun clickStartPost() {

        startPost.call()

    }

    fun clickStartPut() {

        startPut.call()

    }

    fun postGet(): LiveData<Resource<ResultPostGet>> {

        return commonRepository.postGet(displayValue.value ?: 0) {

            withContext( Dispatchers.Main) {

                items.clear( false )
                items.addAll( it )

            }

        }

    }

    fun postPost(): LiveData<Resource<ResultPostPost>> {

        return commonRepository.postPost(displayValue.value ?: 0) {

        }
    }

    fun postPut(): LiveData<Resource<ResultPostPost>> {

        return commonRepository.postPut(displayValue.value ?: 0) {

        }
    }

}