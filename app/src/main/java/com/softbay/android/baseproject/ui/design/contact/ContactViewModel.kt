package com.softbay.android.baseproject.ui.design.contact

import androidx.lifecycle.MutableLiveData
import baycon.android.common.contact.ContactSync
import baycon.android.common.db.contact.ContactEntity
import baycon.android.common.model.BaseModel
import com.softbay.android.baseproject.base.BaseViewModel
import com.softbay.android.baseproject.ui.design.recyclerview.sticky.StickyModel
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@HiltViewModel
class ContactViewModel @Inject constructor(private val contactSync: ContactSync,
                                           private val compareContactMap: HashMap<String, ContactEntity> ): BaseViewModel() {

    val contactListObserve = contactSync.observeList()
    private val contactAllList = ArrayList<BaseModel>()

    val filterItems = MutableLiveData<List<BaseModel>>()

    val searchText = MutableLiveData<String>()

    fun reloadData( contacts:ArrayList<ContactEntity> ) {

        contactAllList.clear()
        for( contactModel in contacts ) {

            if( contactAllList.isEmpty() ) {

                val cHeader = StickyModel()
                cHeader.viewType = BaseModel.HEADER
                cHeader.title = contactModel.indexChosung

                contactAllList.add( cHeader )

            } else {

                val before = contactAllList[contactAllList.size - 1]

                if( before is ContactEntity) {

                    val cCho = contactModel.indexChosung
                    val beCho = before.indexChosung

                    if( cCho != beCho ) {

                        val cHeader = StickyModel()
                        cHeader.viewType = BaseModel.HEADER
                        cHeader.title = contactModel.indexChosung

                        contactAllList.add( cHeader )

                    }

                }

            }

            contactModel.isCheckState = if( compareContactMap.containsKey( "${contactModel.contactId}@${contactModel.contactPhoneNumber}" ) ) {
                2
            } else {
                0
            }

            contactAllList.add( contactModel )

        }

        filterItems.value = contactAllList
    }

    fun filter( filterString: String ) {

        filterItems.value = if( filterString.isEmpty() ) {
            contactAllList
        } else {
            contactAllList.filter {
                it is ContactEntity && it.viewType == BaseModel.BODY &&
                        (   it.contactPhoneNumber.toLowerCase(Locale.getDefault()).indexOf( filterString ) > -1  ||
                                it.fullChosung.toLowerCase(Locale.getDefault()).indexOf( filterString ) > -1 ||
                                it.userName.toLowerCase(Locale.getDefault()).indexOf( filterString ) > -1 )

            }
        }

    }



}