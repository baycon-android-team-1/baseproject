package com.softbay.android.baseproject.ui.design.contact

import androidx.activity.viewModels
import com.brandongogetap.stickyheaders.StickyLayoutManager
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityContactBinding
import dagger.hilt.android.AndroidEntryPoint

/** If you use this Contact, you have to Contact Permission manually */
@AndroidEntryPoint
class ContactActivity: BaseActivity<ActivityContactBinding, ContactViewModel>() {

    override val viewModel: ContactViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_contact

    private val adapter: ContactAdapter by lazy { ContactAdapter() }
    override fun initViewsAndEvents() {

        val stickyLayoutManager = StickyLayoutManager( this, adapter )
        stickyLayoutManager.elevateHeaders( true )

        dataBinding.rvContact.setHasFixedSize( true )
        dataBinding.rvContact.layoutManager = stickyLayoutManager
        dataBinding.rvContact.adapter = adapter

        viewModel.contactListObserve.observe( this, {
            viewModel.reloadData( it )
        })

        viewModel.filterItems.observe( this, {

            adapter.submitList( it )

        })

        viewModel.searchText.observe( this, {

            viewModel.filter( it )

        })

    }
}