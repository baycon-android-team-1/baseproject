package com.softbay.android.baseproject.api.parse

import baycon.android.common.model.BaseModel

class ResultPostGet : ArrayList<ResultPostGet.Post>() {

    class Post: BaseModel() {
        val body: String = ""
        val email: String = ""
        val id: Int = 0
        val name: String = ""
        val postId: Int = 0
    }

}