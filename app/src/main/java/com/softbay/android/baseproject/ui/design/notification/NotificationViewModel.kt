package com.softbay.android.baseproject.ui.design.notification

import baycon.android.common.utils.lifecycle.SingleLiveEvent
import com.softbay.android.baseproject.base.BaseViewModel

class NotificationViewModel: BaseViewModel() {

    val defaultNotificationClick = SingleLiveEvent<Unit>()

    fun onClickDefaultNotification() {
        defaultNotificationClick.call()
    }
}