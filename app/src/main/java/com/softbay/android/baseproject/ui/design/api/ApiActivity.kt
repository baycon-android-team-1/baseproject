package com.softbay.android.baseproject.ui.design.api

import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import baycon.android.common.api.Resource
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityApiBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ApiActivity : BaseActivity<ActivityApiBinding, ApiViewModel>() {

    override val viewModel: ApiViewModel by viewModels()

    override val bindingVariable: Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_api

    private val adapter = ApiAdapter()

    override fun initViewsAndEvents() {

        viewModel.startSearch.observe(this, {

            viewModel.postGet().observe(this, {

                when ( it.status ) {

                    Resource.Status.LOADING -> {

                        Timber.d("get loading")

                    }
                    Resource.Status.SUCCESS -> {

                        Timber.d("get success")

                    }
                    Resource.Status.ERROR -> {

                        Timber.d("get error ${it.message}")

                    }

                }

            })
        })

        viewModel.startPost.observe(this, {

            viewModel.postPost().observe(this, {

                when ( it.status ) {

                    Resource.Status.LOADING -> {

                        Timber.d("post loading")

                    }
                    Resource.Status.SUCCESS -> {

                        Timber.d("post success")
                        Toast.makeText(this, "Post Success ${it.data?.id}", Toast.LENGTH_SHORT).show()

                    }
                    Resource.Status.ERROR -> {

                        Timber.d("post error ${it.message}")

                    }

                }

            })
        })

        viewModel.startPut.observe(this, {

            viewModel.postPut().observe(this, {

                when ( it.status ) {

                    Resource.Status.LOADING -> {

                        Timber.d("put loading")

                    }
                    Resource.Status.SUCCESS -> {

                        Timber.d("put success")
                        Toast.makeText(this, "Put Success ${it.data?.id}", Toast.LENGTH_SHORT).show()

                    }
                    Resource.Status.ERROR -> {

                        Timber.d("put error ${it.message}")

                    }

                }

            })
        })



        viewModel.items.observe( this, {

            adapter.submitList( it )
            adapter.notifyDataSetChanged()

        })

        dataBinding.rv.apply {

            adapter = this@ApiActivity.adapter
            layoutManager = LinearLayoutManager( this@ApiActivity )

        }

    }

}