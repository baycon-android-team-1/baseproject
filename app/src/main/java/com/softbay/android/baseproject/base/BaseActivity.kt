package com.softbay.android.baseproject.base

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.softbay.android.baseproject.ui.dialog.SampleDialog

abstract class BaseActivity<T: ViewDataBinding, V: BaseViewModel>: AppCompatActivity() {

    lateinit var dataBinding: T

    abstract val viewModel: V?

    abstract val bindingVariable: Int?

    @LayoutRes
    abstract fun getLayoutId(): Int

    abstract fun initViewsAndEvents()

    val pr = SampleDialog()

    val pr2 = SampleDialog

    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        DataBindingUtil.setContentView<T>(this, getLayoutId() ).apply {
            dataBinding = this
            lifecycleOwner = this@BaseActivity
            bindingVariable?.let { setVariable( it, viewModel ) }
            executePendingBindings()
        }

        viewModel?.let { lifecycle.addObserver( it ) }

        initViewsAndEvents()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel?.let { lifecycle.removeObserver( it ) }

    }

    /**
     * 뒤로 가기 관련
     */
    override fun onBackPressed() {
        setResult( Activity.RESULT_CANCELED, Intent() )
        finish()
    }

    /**
     * Toast 출력
     */

    fun showToast( @StringRes resId: Int ) {

        if ( toast != null ) toast?.cancel()
        toast = Toast.makeText( this, resId, Toast.LENGTH_SHORT )
        toast?.show()

    }

    fun cancelToast() {

        toast?.cancel()

    }

}