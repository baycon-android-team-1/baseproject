package com.softbay.android.baseproject.ui.design.recyclerview

import android.util.Log
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.brandongogetap.stickyheaders.StickyLayoutManager
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.base.ItemClickListener
import com.softbay.android.baseproject.databinding.ActivityRecyclerViewBinding
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemDragListener
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemTouchHelperCallback
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@AndroidEntryPoint
class RecyclerViewActivity: BaseActivity<ActivityRecyclerViewBinding, RecyclerViewViewModel>(), ItemDragListener {

    override val viewModel: RecyclerViewViewModel by viewModels()

    override val bindingVariable: Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_recycler_view

    private val itemClickListener = object : ItemClickListener {

        override fun onClick(view: View, position: Int, isLongClick: Boolean) {}

    }

//    private val rvAdapter = RecyclerViewAdapter( itemClickListener, this )
    private val rvAdapter = RecyclerViewListAdapter( itemClickListener, this )


    private val touchHelperCallback by lazy { ItemTouchHelperCallback( rvAdapter ) }

    private val touchHelper = ItemTouchHelper( touchHelperCallback )

    override fun initViewsAndEvents() {

        viewModel.items.observe(this, {

//            rvAdapter.setItems(it)
            rvAdapter.submitList(it)
            rvAdapter.notifyDataSetChanged()

        })

        val stickyLayoutManager = StickyLayoutManager( this, rvAdapter )
        stickyLayoutManager.elevateHeaders( true )

        dataBinding.rv.run {

            layoutManager = stickyLayoutManager
            adapter = rvAdapter
            touchHelper.attachToRecyclerView( this )
            addOnScrollListener( onScrollListener )

        }

    }

    private val onScrollListener = object : RecyclerView.OnScrollListener(){
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            if( recyclerView.layoutManager is LinearLayoutManager) {
                val totalItemCount = rvAdapter.itemCount
                val layoutManager: LinearLayoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastVisibleItem = layoutManager.findLastCompletelyVisibleItemPosition() + 1

                if( totalItemCount != 0 && lastVisibleItem == totalItemCount && totalItemCount < 60 ) {

                    viewModel.addItems( lastVisibleItem - 2 )


                }

            }

        }

    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {

        touchHelper.startDrag(viewHolder)

    }

}