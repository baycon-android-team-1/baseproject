package com.softbay.android.baseproject.ui.notification

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.ui.design.notification.NotificationActivity

object DefaultNotification {

    // Notification Channel
    private const val NOTIFICATION_ID_EVENT = "sample notification"
    private const val NOTIFICATION_NAME_EVENT = "test notification"

    // Notification Code
    private const val NOTIFICATION_CODE_DEFAULT = 8001024

    // Push Data
    private const val PUSH_DATA_PUSH_KEY_MSG_TYPE = "MT"
    private const val PUSH_DATA_PUSH_TYPE_LINK = "LINK"
    private const val PUSH_DATA_PUSH_KEY_URL = "URL"

    fun startNotification( context: Context,
                           title: String?, content: String?, type: String, url: String?,
                           mipmapId: Int = R.mipmap.ic_launcher, multiline: String? = null ) {

        val notificationManager = context.getSystemService( Context.NOTIFICATION_SERVICE ) as NotificationManager

        // version for O upper
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel( NOTIFICATION_ID_EVENT, NOTIFICATION_NAME_EVENT, importance ).apply {
                enableLights( false )
                enableVibration( true )
                setShowBadge( false )
            }
            notificationManager.createNotificationChannel( notificationChannel )

        }

        // Make Intent go Activity When Notification Click
        val processIntent = Intent( context, NotificationActivity::class.java )
        processIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK )
        processIntent.putExtra( PUSH_DATA_PUSH_KEY_MSG_TYPE, type)

        if( type == PUSH_DATA_PUSH_TYPE_LINK && !url.isNullOrEmpty() )
            processIntent.putExtra( PUSH_DATA_PUSH_KEY_URL, url)

        val pIntent = PendingIntent.getActivity( context, System.currentTimeMillis().toInt(), processIntent, 0)

        // builder
        val builder = NotificationCompat.Builder( context, NOTIFICATION_ID_EVENT )
        builder.setLargeIcon( BitmapFactory.decodeResource( context.resources, R.mipmap.ic_launcher ) )
        builder.setSmallIcon( mipmapId )
        builder.setTicker( content )
        builder.setWhen( System.currentTimeMillis() )
        builder.setNumber( 0 )
        multiline?.let { builder.setStyle( NotificationCompat.BigTextStyle().bigText( it ) ) }
        builder.setContentTitle( title ?: context.getString( R.string.app_name ) )
        builder.setContentText( content ?: "" )
        builder.setContentIntent( pIntent )
        builder.setAutoCancel( true )

        /** set Notification when Expand */

        /* set Picture */
//        builder.setStyle( NotificationCompat.BigPictureStyle()
//            .bigPicture( ContextCompat.getDrawable(context, R.drawable.flag_aland_islands)?.toBitmap(100, 100 )) )

        /* set Text */
//        builder.setStyle( NotificationCompat.BigTextStyle().bigText( "txt for test") )

        /* set Inbox */
//        builder.setStyle(
//            NotificationCompat.InboxStyle()
//                .addLine("line 1")
//                .addLine("line 2"))


        val notification: Notification = builder.build()

        notificationManager.notify( NOTIFICATION_CODE_DEFAULT, notification )

    }

    fun cancelNotification( context: Context ) {

        val notificationManager = context.getSystemService( Context.NOTIFICATION_SERVICE ) as NotificationManager
        notificationManager.cancel( NOTIFICATION_CODE_DEFAULT )
    }

}