package com.softbay.android.baseproject.ui.hilt

import baycon.android.common.di.SampleUtil
import com.softbay.android.baseproject.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class HiltViewModel @Inject constructor(val sampleUtil: SampleUtil): BaseViewModel() {

    fun hiltSampleStart() {
        sampleUtil.sample()
    }

}