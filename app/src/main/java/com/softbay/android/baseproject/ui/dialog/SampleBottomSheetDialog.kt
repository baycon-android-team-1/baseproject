package com.softbay.android.baseproject.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.LifecycleOwner
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.softbay.android.baseproject.databinding.ViewSampleBottomSheetDialogBinding

class SampleBottomSheetDialog: BottomSheetDialogFragment() {

    companion object {

        private const val SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY = "SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY"
        private const val RESULT_CLICK_SAMPLE_1 = "RESULT_CLICK_SAMPLE_1"
        private const val RESULT_CLICK_SAMPLE_2 = "RESULT_CLICK_SAMPLE_2"
        private const val RESULT_SHOW_DIALOG = "RESULT_SHOW_DIALOG"

        fun display( fragmentManager: FragmentManager,
                     lifecycleOwner: LifecycleOwner,
                     clickSample1: (() -> Unit)? = null,
                     clickSample2: (() -> Unit)? = null ) {

            SampleBottomSheetDialog().run {

                fragmentManager.setFragmentResultListener( SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY, lifecycleOwner, { requestKey, result ->

                    if ( requestKey == SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY ) {

                        when {

                            result.containsKey( RESULT_CLICK_SAMPLE_1 ) -> clickSample1?.invoke()
                            result.containsKey( RESULT_CLICK_SAMPLE_2 ) -> clickSample2?.invoke()

                        }

                    }

                })

                show (fragmentManager, RESULT_SHOW_DIALOG)
            }

        }
    }

    private lateinit var binding: ViewSampleBottomSheetDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        binding = ViewSampleBottomSheetDialogBinding.inflate( inflater, container, false )
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnTest1.setOnClickListener {

            setFragmentResult( SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY, bundleOf( RESULT_CLICK_SAMPLE_1 to "" ))
            dismiss()

        }

        binding.btnTest2.setOnClickListener {

            setFragmentResult( SAMPLE_BOTTOM_SHEET_DIALOG_REQUEST_KEY, bundleOf( RESULT_CLICK_SAMPLE_2 to "" ))
            dismiss()

        }

    }
}