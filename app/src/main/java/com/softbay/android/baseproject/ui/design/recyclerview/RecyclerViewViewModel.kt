package com.softbay.android.baseproject.ui.design.recyclerview

import android.util.Log
import baycon.android.common.model.BaseModel
import baycon.android.common.model.HeaderModel
import baycon.android.common.utils.lifecycle.ListLiveData
import com.softbay.android.baseproject.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RecyclerViewViewModel @Inject constructor(): BaseViewModel() {

    val items = ListLiveData<BaseModel>()

    init {

        for (i in 1..30) {

            if ( i % 10 == 1) {

                val header = HeaderModel()
                header.viewType = BaseModel.HEADER
                header.title = "HEADER"
                items.add(header)

            }
            val item = BaseModel()
            item.title = "$i item"
            items.add(item)

        }

    }

    fun addItems(lastIndex: Int) {

        val newItems = ArrayList<BaseModel>()

        var cnt = 1

        for ( i in lastIndex..lastIndex.plus( 29 ) ) {

            if ( cnt % 10 == 1) {

                val header = HeaderModel()
                header.viewType = BaseModel.HEADER
                header.title = "HEADER"
                newItems.add(header)

            }

            val item = BaseModel()
            item.title = "$i item"
            newItems.add(item)

            cnt++

        }

        items.addAll(newItems)

    }

}