package com.softbay.android.baseproject.ui.design.recyclerview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import baycon.android.common.model.BaseModel
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseViewHolder
import com.softbay.android.baseproject.base.HeaderViewHolder
import com.softbay.android.baseproject.base.ItemClickListener
import com.softbay.android.baseproject.databinding.ActivityRecyclerViewItemBinding
import com.softbay.android.baseproject.databinding.CommonHeaderBinding
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemDragListener
import com.softbay.android.baseproject.ui.design.recyclerview.deco.ItemTouchHelperListener

class RecyclerViewAdapter( private val onItemClick: ItemClickListener, private val itemDragListener: ItemDragListener ): RecyclerView.Adapter<BaseViewHolder>(), StickyHeaderHandler, ItemTouchHelperListener {

    private var items = ArrayList<BaseModel>()

    fun setItems(items: ArrayList<BaseModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            BaseModel.HEADER -> {

                val binding = DataBindingUtil.inflate<CommonHeaderBinding>( inflater, R.layout.common_header, parent, false )
                HeaderViewHolder(binding)

            }
            else -> {

                val binding = DataBindingUtil.inflate<ActivityRecyclerViewItemBinding>( inflater, R.layout.activity_recycler_view_item, parent, false )
                RecyclerViewViewHolder(binding, itemDragListener)

            }

        }

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBindView( items[position] )
    }

    override fun getItemCount(): Int = items.size

    override fun getAdapterData(): MutableList<*> = items

    override fun getItemId(position: Int): Long = items[position].hashCode().toLong()

    override fun getItemViewType(position: Int): Int = items[position].viewType

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        val item = items[fromPosition]
        items.removeAt(fromPosition)
        items.add(toPosition, item)
        notifyItemMoved(fromPosition, toPosition)
        return true
    }

    override fun onItemSwipe(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class RecyclerViewViewHolder(private val binding: ActivityRecyclerViewItemBinding, private val onDragListener: ItemDragListener): BaseViewHolder( binding.root ) {
        @SuppressLint("ClickableViewAccessibility")
        override fun onBindView(item: BaseModel) {

            binding.run {

                this.item = item

                parent.setOnClickListener {

                    onItemClick.onClick( it, absoluteAdapterPosition + 1, false )

                }

                ivDrag.setOnTouchListener { _, event ->
                    if(event.action == MotionEvent.ACTION_DOWN){
                        onDragListener.onStartDrag(this@RecyclerViewViewHolder)
                    }
                    false
                }

            }

        }

    }

}