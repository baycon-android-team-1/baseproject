package com.softbay.android.baseproject

import android.app.Application
import baycon.android.common.contact.ContactSync
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class AppApplication: Application() {

    @Inject
    lateinit var contactSync: ContactSync

    override fun onCreate() {
        super.onCreate()

        contactSync.startSync()

    }
}