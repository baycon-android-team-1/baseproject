package com.softbay.android.baseproject.ui.design.contact

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import baycon.android.common.db.contact.ContactEntity
import baycon.android.common.model.BaseModel
import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseViewHolder
import com.softbay.android.baseproject.base.HeaderViewHolder
import com.softbay.android.baseproject.databinding.CommonHeaderBinding
import com.softbay.android.baseproject.databinding.ViewContactItemBinding

class ContactAdapter : ListAdapter<BaseModel, BaseViewHolder>( ContactDiffUtil ), StickyHeaderHandler {

    override fun getAdapterData(): MutableList<*> {
        return currentList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            BaseModel.HEADER -> {
                val binding = DataBindingUtil.inflate<CommonHeaderBinding>(
                    layoutInflater,
                    R.layout.common_header,
                    parent,
                    false
                )
                HeaderViewHolder(binding)
            }
            else -> {
                val binding = DataBindingUtil.inflate<ViewContactItemBinding>(
                    layoutInflater,
                    R.layout.view_contact_item,
                    parent,
                    false
                )
                ContactViewHolder(binding)
            }
        }

    }

    override fun getItemViewType(position: Int): Int {
        return getItem(position).viewType
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val movie = getItem(position)
        holder.onBindView(movie)
    }

    inner class ContactViewHolder(private val binding: ViewContactItemBinding) :
        BaseViewHolder(binding.root) {
        override fun onBindView(item: BaseModel) {
            if (item is ContactEntity) {
                binding.contactEntity = item
                binding.executePendingBindings() //데이터가 수정되면 즉각 바인

            }

        }
    }

    companion object ContactDiffUtil : DiffUtil.ItemCallback<BaseModel>() {
        override fun areItemsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            //각 아이템들의 고유한 값을 비교해야 한다.
            return oldItem == newItem
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: BaseModel, newItem: BaseModel): Boolean {
            return oldItem == newItem
        }
    }
}