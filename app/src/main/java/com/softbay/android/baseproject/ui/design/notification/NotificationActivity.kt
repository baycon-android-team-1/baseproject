package com.softbay.android.baseproject.ui.design.notification

import androidx.activity.viewModels
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityNotificationBinding
import com.softbay.android.baseproject.ui.notification.DefaultNotification
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NotificationActivity: BaseActivity<ActivityNotificationBinding, NotificationViewModel>() {
    override val viewModel: NotificationViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_notification

    override fun initViewsAndEvents() {

        viewModel.defaultNotificationClick.observe( this, {
            DefaultNotification.startNotification( this, "title", "content", "msgType", "link Url", R.mipmap.ic_launcher )
        })

    }

}