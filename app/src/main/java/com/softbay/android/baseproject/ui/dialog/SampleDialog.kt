package com.softbay.android.baseproject.ui.dialog

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.LifecycleOwner
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.ViewSampleDialogBinding

/** for Dialog Fragment Sample */
class SampleDialog: DialogFragment() {

    companion object {

        private const val SAMPLE_DIALOG_REQUEST_KEY = "SAMPLE_DIALOG_REQUEST_KEY"
        private const val RESULT_DIALOG_KEY_OK = "RESULT_DIALOG_KEY_OK"
        private const val RESULT_DIALOG_KEY_CANCEL = "RESULT_DIALOG_KEY_CANCEL"
        private const val SHOW_DIALOG = "SHOW_DIALOG"

        fun display(fragmentManager: FragmentManager,
                    lifecycleOwner: LifecycleOwner,
                    clickCancel: (() -> Unit)? = null,
                    clickOk: (() -> Unit)? = null ) {

            SampleDialog().run {

                fragmentManager.setFragmentResultListener(SAMPLE_DIALOG_REQUEST_KEY, lifecycleOwner, { requestKey, result ->
                    if ( requestKey == SAMPLE_DIALOG_REQUEST_KEY ) {

                        when {

                            result.containsKey( RESULT_DIALOG_KEY_OK ) -> clickOk?.invoke()
                            result.containsKey( RESULT_DIALOG_KEY_CANCEL ) -> clickCancel?.invoke()

                        }

                    }
                })

                show( fragmentManager, SHOW_DIALOG )

            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    override fun getTheme(): Int = R.style.AppTheme_FullScreenDialog

    lateinit var dataBinding: ViewSampleDialogBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?, ): View {

        // dialog DataBinding
        dataBinding = ViewSampleDialogBinding.inflate(inflater, container, false)

        // dialog Setting
        dialog?.window?.setBackgroundDrawable( ColorDrawable( Color.TRANSPARENT ) )
//        dialog?.window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT  )
//        dialog?.window?.requestFeature( Window.FEATURE_NO_TITLE )
//        dialog?.window?.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        dialog?.setCanceledOnTouchOutside( false )

        // for dialog margin
//        val inset = InsetDrawable( ColorDrawable( Color.TRANSPARENT ), 20.dp )
//        dialog?.window?.setBackgroundDrawable( inset )

        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dataBinding.btnOk.setOnClickListener {
            setFragmentResult( SAMPLE_DIALOG_REQUEST_KEY, bundleOf( RESULT_DIALOG_KEY_OK to ""))
            dismiss()
        }

        dataBinding.btnCancel.setOnClickListener {
            setFragmentResult( SAMPLE_DIALOG_REQUEST_KEY, bundleOf( RESULT_DIALOG_KEY_CANCEL to ""))
            dismiss()
        }

    }
}