package com.softbay.android.baseproject.ui.design.toolbar

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.AppBarLayout
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.SampleToolbarBinding

class SampleToolbar @JvmOverloads constructor( context: Context, attrs: AttributeSet? = null ): AppBarLayout(context, attrs) {

    private val binding: SampleToolbarBinding = SampleToolbarBinding.inflate( LayoutInflater.from( context ), this, false )

    init {

        addView( binding.root )

        attrs?.let {

            val attr = context.obtainStyledAttributes( it, R.styleable.SampleToolbar )
            val title = attr.getString( R.styleable.SampleToolbar_toolbar_title )

            attr.recycle()

            setTitle( title )

        }

    }

    fun setTitle( title: String? ) {

        binding.tvTitle.text = title
        binding.toolbar.title = ""

    }

    fun setSupportActionBar( activity: AppCompatActivity, isHomeAsUpEnabled: Boolean = true) {

        activity.setSupportActionBar( binding.toolbar )
        activity.supportActionBar?.setDisplayHomeAsUpEnabled( isHomeAsUpEnabled )

    }

}