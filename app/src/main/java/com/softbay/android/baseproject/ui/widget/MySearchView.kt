package com.softbay.android.baseproject.ui.widget

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.WidgetSearchViewBinding

class MySearchView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0): RelativeLayout(context, attrs, defStyleAttr) {

    private val binding: WidgetSearchViewBinding = WidgetSearchViewBinding.inflate( LayoutInflater.from( context ), this, false )

    init {
        addView( binding.root )

        attrs?.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.MySearchView)
            val searchIcon = a.getResourceId(R.styleable.MySearchView_search_icon, -1 )
            val deleteIcon = a.getResourceId(R.styleable.MySearchView_delete_icon, -1 )
            val bg = a.getResourceId(R.styleable.MySearchView_bg, -1 )
            val hintString:String? = a.getString( R.styleable.MySearchView_hint)
            val hintColor = a.getColor( R.styleable.MySearchView_color_hint, -1 )
            val textColor = a.getColor( R.styleable.MySearchView_color_text, -1 )
            val isPhone = a.getBoolean( R.styleable.MySearchView_is_phone, false )

            if( hintColor != -1 )
                binding.etInput.setHintTextColor( hintColor )
            if( textColor != -1)
                binding.etInput.setTextColor( textColor )
            if( isPhone )
                binding.etInput.inputType = InputType.TYPE_CLASS_PHONE

            if( bg != -1 )
                binding.root.setBackgroundResource( bg )
            if( searchIcon != -1 )
                binding.ivHint.setImageResource( searchIcon )
            if( deleteIcon != -1 )
                binding.btnDelete.setImageResource( deleteIcon )

            binding.etInput.hint = hintString ?: ""

            a.recycle()

        }

        binding.btnDelete.setOnClickListener{
//            binding.etInput.setText( "" )
            searchText?.value = ""
        }
    }

//    constructor(context: Context) : this(context, null)
//    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
//    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    private var searchText: MutableLiveData<String>? = null

    fun setSearchText( searchText: MutableLiveData<String> ){
        this.searchText = searchText
        binding.searchText = searchText
    }

}