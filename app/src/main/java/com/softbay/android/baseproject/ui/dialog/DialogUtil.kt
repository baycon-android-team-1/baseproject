package com.softbay.android.baseproject.ui.dialog

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.ViewDialogDefaultBinding

interface DialogDefaultClickListener {

    fun onOk( obj: Any? )
    fun onCancel( obj: Any? )

}

object DialogUtil {

    /** make Default Alert Dialog */
    fun showDefaultDialog(context: Context, layoutInflater: LayoutInflater,
                          title: String?, content: Any, subContent: String?,
                          cancelText: String?, confirmText: String?, isAutoCancel: Boolean,
                          dialogClickListener: DialogDefaultClickListener?, obj: Any? ) {

        val binding = ViewDialogDefaultBinding.inflate( layoutInflater )
        // use Style which you want
        val builder = AlertDialog.Builder( context, R.style.AppTheme_FullScreenDialog )

        // setting Default Dialog title ( tv_title ) default visibility = gone
        title?.let {
            binding.tvTitle.visibility = View.VISIBLE
            binding.tvTitle.text = it
        }

        // setting Default Dialog MainContent ( tv_content )
        if ( content is String || content is CharSequence ) {

            binding.tvContent.text = content.toString()

        }

        // setting Default Dialog SubContent ( tv_sub_content ) default visibility = gone
        subContent?.let {

            binding.tvSubContent.visibility = View.VISIBLE
            binding.tvSubContent.text = subContent

        }

        // dialog builder setting
        builder.setView( binding.root )
        builder.setCancelable( isAutoCancel )

        // dialog setting
        val dialog = builder.create()
        // dialog Full Screen Setting
        dialog?.window?.setLayout( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT )
        dialog.window?.setBackgroundDrawable( ColorDrawable( Color.TRANSPARENT ) )
        dialog.show()

        // btn right Setting ( btn_right )
        binding.btnRight.text = confirmText
        binding.btnRight.setOnClickListener {
            dialogClickListener?.onOk( obj )
            dialog.dismiss()
        }

        // btn_left Setting  ( btn_left ) btn_left default visibility = gone
        cancelText?.let {

            binding.btnLeft.visibility = View.VISIBLE
            binding.btnLeft.text = cancelText
            binding.btnLeft.setOnClickListener {
                dialogClickListener?.onCancel( obj )
                dialog.dismiss()
            }

        }

    }
}