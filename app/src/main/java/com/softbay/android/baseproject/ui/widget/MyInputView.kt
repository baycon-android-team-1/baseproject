package com.softbay.android.baseproject.ui.widget

import android.content.Context
import android.graphics.Color
import android.text.*
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.lifecycle.MutableLiveData
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.WidgetInputViewBinding

//이종한
class MyInputView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0): RelativeLayout(context, attrs, defStyleAttr) {

    private var maxLength = 0
    private var minLength = 1

    private val binding: WidgetInputViewBinding = WidgetInputViewBinding.inflate( LayoutInflater.from( context ), this, false )

    private val textChangeListener = object : TextWatcher{
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            drawLength()
        }

    }

    init {
        addView( binding.root )

        attrs?.let {

            val a = context.obtainStyledAttributes(attrs, R.styleable.MyInputView)

            val title:String? = a.getString( R.styleable.MyInputView_android_text)
            val hint:String? = a.getString( R.styleable.MyInputView_android_hint)
            val inputType:Int = a.getInt( R.styleable.MyInputView_android_inputType, InputType.TYPE_CLASS_TEXT )
            val gravity:Int = a.getInt( R.styleable.MyInputView_android_gravity, Gravity.START )
            val headerColor = a.getColor( R.styleable.MyInputView_color_header, -1 )
            val inputColor = a.getColor( R.styleable.MyInputView_color_input, -1 )

            maxLength = a.getInt( R.styleable.MyInputView_max_length,0 )
            minLength = a.getInt( R.styleable.MyInputView_min_length,1 )

            if( headerColor != -1 )
                binding.tvTitle.setTextColor( headerColor )

            if( inputColor != -1 )
                binding.etInput.setTextColor( inputColor )

            val isLength = a.getBoolean( R.styleable.MyInputView_is_length, true )

            binding.tvLength.visibility = if( isLength ) View.VISIBLE else View.GONE

            binding.etInput.gravity = gravity

            if( maxLength > 0 )
                binding.etInput.filters = arrayOf( InputFilter.LengthFilter( maxLength ) )

            if( title != null ) {
                binding.tvTitle.text = title
            } else {
                binding.tvTitle.visibility = View.GONE
            }

            if( hint != null ) {
                binding.etInput.hint = hint
            }

            binding.etInput.inputType = inputType

            if( maxLength > 0 ) {
                binding.etInput.filters = arrayOf( InputFilter.LengthFilter( maxLength ) )
                binding.etInput.addTextChangedListener( textChangeListener )
                drawLength()
            } else {
                binding.tvLength.visibility = View.GONE
            }

            a.recycle()
        }

        binding.btnDelete.setOnClickListener{
            binding.etInput.setText( "" )
        }
    }

    fun setInputText( searchText: MutableLiveData<String>){
        binding.inputText = searchText
    }

    private fun drawLength(){
        if( maxLength > 0 ) {

            val drawText = "${binding.etInput.text.toString().length} / $maxLength"
            if( binding.etInput.text.toString().length > maxLength ) {

                val sp = SpannableStringBuilder( drawText )

                sp.setSpan( ForegroundColorSpan( Color.RED ), 0, drawText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

                binding.tvLength.text = sp

            } else {

                binding.tvLength.text = drawText

            }

        }
    }

    fun addInputFilter( inputFilter:InputFilter ) {

        if( maxLength > 0 )
            binding.etInput.filters = arrayOf( inputFilter, InputFilter.LengthFilter( maxLength ) )
        else
            binding.etInput.filters = arrayOf( inputFilter )

    }

}