package com.softbay.android.baseproject.ui.design.dialog

import androidx.activity.viewModels
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.databinding.ActivityDialogBinding
import com.softbay.android.baseproject.ui.dialog.SampleBottomSheetDialog
import com.softbay.android.baseproject.ui.dialog.DialogDefaultClickListener
import com.softbay.android.baseproject.ui.dialog.DialogUtil
import com.softbay.android.baseproject.ui.dialog.SampleDialog
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class DialogActivity : BaseActivity<ActivityDialogBinding, DialogViewModel>() {
    override val viewModel: DialogViewModel by viewModels()

    override val bindingVariable: Int
        get() = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_dialog

    override fun initViewsAndEvents() {

        with ( viewModel ) {

            fragmentDialogShowClick.observe(this@DialogActivity, {
                // Fragment Dialog show
                SampleDialog.display( supportFragmentManager, this@DialogActivity, { clickOk() }, { clickCancel() })
            })

            alertDialogShowClick.observe(this@DialogActivity, {
                // Default Alert Dialog Show
                DialogUtil.showDefaultDialog(
                    this@DialogActivity, layoutInflater,
                    "testTitle", "test Content", null,
                    "Cancel", "confirm", true, defaultAlertDialogClickListener, null)
            })

            bottomSheetDialogClick.observe(this@DialogActivity, {
                // Bottom Sheet Dialog Show
                SampleBottomSheetDialog.display( supportFragmentManager, this@DialogActivity,
                    { clickBottomSheetBtn() }, { clickBottomSheetBtn2() })

                // another show way
//                SampleBottomSheetDialog().show( supportFragmentManager, "" )
            })

        }
    }

    /** Fragment Dialog Click Event */
    private fun clickOk() {
        Timber.i("DialogFragment(SampleDialog) click Ok Button")
    }

    private fun clickCancel() {
        Timber.i("DialogFragment(SampleDialog) click Cancel Button")
    }

    /** Default Dialog Click Event */
    private val defaultAlertDialogClickListener = object : DialogDefaultClickListener {
        override fun onOk(obj: Any?) {
            Timber.i("Default Alert Dialog Confirm Click")
        }

        override fun onCancel(obj: Any?) {
            Timber.i("Default Alert Dialog Cancel Click")
        }

    }

    /** BottomSheetFragment click Event */
    private fun clickBottomSheetBtn() {
        Timber.i("BottomSheetDialog(SampleBottomSheetDialog) click btn")
    }

    private fun clickBottomSheetBtn2() {
        Timber.i("BottomSheetDialog(SampleBottomSheetDialog) click btn2")
    }

}