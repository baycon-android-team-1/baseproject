package com.softbay.android.baseproject.ui.design.button

import androidx.activity.viewModels
import com.softbay.android.baseproject.BR
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.base.BaseActivity
import com.softbay.android.baseproject.base.BaseViewModel
import com.softbay.android.baseproject.databinding.ActivityButtonBinding

class ButtonActivity : BaseActivity<ActivityButtonBinding, BaseViewModel>() {
    override val viewModel: ButtonViewModel by viewModels()

    override val bindingVariable: Int = BR.viewModel

    override fun getLayoutId(): Int = R.layout.activity_button

    override fun initViewsAndEvents() {
    }

}