package com.softbay.android.baseproject.api.contract

import baycon.android.common.api.RetrofitCreator
import baycon.android.common.api.contract.BaseDataSource
import com.softbay.android.baseproject.api.make.MakePostPost
import com.softbay.android.baseproject.api.make.MakePostPut
import com.softbay.android.baseproject.api.parse.ResultPostGet
import com.softbay.android.baseproject.api.parse.ResultPostPost
import retrofit2.Response
import retrofit2.http.*

class CommonDataSource: BaseDataSource() {

    suspend fun postGet(postId: Int ) = getResult {
        RetrofitCreator.createDefault( ICommonService::class.java, RetrofitCreator.BASE_URL ).getPostGet( postId )
    }

    suspend fun postPost(postId: Int) = getResult {
        RetrofitCreator.createDefault( ICommonService::class.java, RetrofitCreator.BASE_URL ).getPostPost( MakePostPost( postId ) )
    }

    suspend fun postPut(postId: Int, title: String, body: String, id: Int) = getResult {
        RetrofitCreator.createDefault( ICommonService::class.java, RetrofitCreator.BASE_URL ).getPostPut( MakePostPut( id, title, body, postId ) )
    }

}

interface ICommonService {

    @GET("/comments")
    suspend fun getPostGet(@Query("postId") postId: Int): Response<ResultPostGet>

    @POST("/posts")
    suspend fun getPostPost(@Body makePost: MakePostPost ): Response<ResultPostPost>

    @PUT("/posts/1")
    suspend fun getPostPut(@Body make: MakePostPut ): Response<ResultPostPost>
}