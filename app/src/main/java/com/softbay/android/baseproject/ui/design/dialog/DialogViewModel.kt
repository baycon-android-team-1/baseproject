package com.softbay.android.baseproject.ui.design.dialog

import baycon.android.common.utils.lifecycle.SingleLiveEvent
import com.softbay.android.baseproject.base.BaseViewModel

class DialogViewModel: BaseViewModel() {

    val alertDialogShowClick = SingleLiveEvent<Unit>()
    val fragmentDialogShowClick = SingleLiveEvent<Unit>()
    val bottomSheetDialogClick = SingleLiveEvent<Unit>()

    fun onAlertDialogShowClick() {
        alertDialogShowClick.call()
    }

    fun onFragmentDialogShowClick() {
        fragmentDialogShowClick.call()
    }

    fun onBottomSheetDialogClick() {
        bottomSheetDialogClick.call()
    }
}