package com.softbay.android.baseproject.ui.dialog

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.databinding.ViewSampleBottomSheetItemBinding

/** This is Sample for how to make Fragment Dialog Item */
class SampleBottomSheetDialogItem @JvmOverloads constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int = 0)
    : RelativeLayout(context, attrs, defStyleAttr) {

    private val binding: ViewSampleBottomSheetItemBinding =
        ViewSampleBottomSheetItemBinding.inflate( LayoutInflater.from( context ), this, false )

    init {


        addView( binding.root )

        if( attrs != null ) {

            val a = context.obtainStyledAttributes(attrs, R.styleable.SampleBottomSheetDialogItem)
            val icon = a.getResourceId(R.styleable.SampleBottomSheetDialogItem_icon, -1)
            val title: String? = a.getString(R.styleable.SampleBottomSheetDialogItem_title )

            if( icon != -1 )
                binding.ivIcon.setImageResource( icon )

            binding.tvTitle.text = title
            a.recycle()

        }
    }

}