package com.softbay.android.baseproject.ui.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.core.content.ContextCompat;

import com.softbay.android.baseproject.R;


public class AuthEditText extends AppCompatEditText {
    public static final String XML_NAMESPACE_ANDROID = "http://schemas.android.com/apk/res/android";

    private float mSpace = 10; //24 dp by default, space between the lines
    private float mCharSize;
    private float mNumChars = 4;
    private float mLineSpacing = 8; //8dp by default, height of the text from our lines
    private int mMaxLength = 4;

    private int viewType;

    private OnClickListener mClickListener;

    private float mLineStroke = 2; //1dp by default
    private float mLineStrokeSelected = 2; //2dp by default
    private Paint mLinesPaint;
    private Paint mTextPaint;
    int[][] mStates = new int[][]{
            new int[]{android.R.attr.state_selected}, // selected
            new int[]{android.R.attr.state_focused}, // focused
            new int[]{-android.R.attr.state_focused}, // unfocused
    };

    int[] mColors = new int[]{
            Color.GREEN,
            Color.BLACK,
            Color.GRAY
    };

    ColorStateList mColorStates = new ColorStateList(mStates, mColors);

    public AuthEditText(Context context) {
        super(context);
    }

    public AuthEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public AuthEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AuthEditText);

        viewType = a.getInt( R.styleable.AuthEditText_viewType, 0 );

        a.recycle();

        float multi = context.getResources().getDisplayMetrics().density;
        mLineStroke = multi * mLineStroke;
        mLineStrokeSelected = multi * mLineStrokeSelected;
        mLinesPaint = new Paint(getPaint());
        mLinesPaint.setStrokeWidth(mLineStroke);

        mTextPaint = new Paint( getPaint() );
        mTextPaint.setColor( ContextCompat.getColor( context, R.color.c010 ) );

        mColors[0] = ContextCompat.getColor( context, R.color.colorAccent );
        mColors[1] = ContextCompat.getColor( context, R.color.c006 );
        mColors[2] = ContextCompat.getColor( context, R.color.c006 );

        setBackgroundResource(0);
        mSpace = multi * mSpace; //convert to pixels for our density
        mLineSpacing = multi * mLineSpacing; //convert to pixels for our density
        mMaxLength = attrs.getAttributeIntValue(XML_NAMESPACE_ANDROID, "maxLength", 4);
        mNumChars = mMaxLength;

        //Disable copy paste
        super.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
        // When tapped, move cursor to end of text.
        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(getText().length());
                if (mClickListener != null) {
                    mClickListener.onClick(v);
                }
            }
        });

    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        mClickListener = l;
    }

    @Override
    public void setCustomSelectionActionModeCallback(ActionMode.Callback actionModeCallback) {
        throw new RuntimeException("setCustomSelectionActionModeCallback() not supported.");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        int availableWidth = getWidth() - getPaddingRight() - getPaddingLeft();
        mCharSize = ( (int)( availableWidth * 0.7 )- ( mSpace * ( mNumChars - 1 ) ) ) / mNumChars;

        int startX = getPaddingLeft() + (int) ( ( availableWidth - mCharSize * mNumChars - ( mNumChars - 1 ) * mSpace ) / 2 );
        int bottom = getHeight() - getPaddingBottom();

        //Text Width
        Editable text = getText();
        int textLength = text.length();
        float[] textWidths = new float[textLength];
        mTextPaint.getTextWidths(getText(), 0, textLength, textWidths);

        if ( viewType == 0 ) {

            for (int i = 0; i < mNumChars; i++) {
                updateColorForLines(i == textLength );
//            canvas.drawLine(startX, bottom, startX + mCharSize, bottom, mLinesPaint);

                if( getText().length() <= i ) {
                    canvas.drawLine(startX, bottom, startX + mCharSize, bottom, mLinesPaint);
                }
                if( getText().length() > i ) {
                    float middle = startX + mCharSize / 2;
                    canvas.drawLine(startX, bottom, startX + mCharSize, bottom, mLinesPaint);
                    canvas.drawText(text, i, i + 1, middle - textWidths[i] / 2, bottom - mLineSpacing, mTextPaint );
                }



                startX += mCharSize + mSpace;
            }

        } else {

            for (int i = 0; i < mNumChars; i++) {
                updateColorForLines(i == textLength );
//            canvas.drawLine(startX, bottom, startX + mCharSize, bottom, mLinesPaint);

                if( getText().length() <= i ) {
                    canvas.drawCircle( startX + mCharSize / 2, (int)( getHeight() / 2 ), 25, mLinesPaint );
                }
                if( getText().length() > i ) {
                    float middle = startX + mCharSize / 2;
                    canvas.drawText(text, i, i + 1, middle - textWidths[i] / 2, bottom - mLineSpacing, mTextPaint );
                }



                startX += mCharSize + mSpace;
            }
        }
    }


    private int getColorForState(int... states) {
        return mColorStates.getColorForState(states, Color.GRAY);
    }

    /**
     * @param next Is the current char the next character to be input?
     */
    private void updateColorForLines(boolean next) {
        if (isFocused()) {
            mLinesPaint.setStrokeWidth(mLineStrokeSelected);
            mLinesPaint.setColor(getColorForState(android.R.attr.state_focused));
            if (next) {
                mLinesPaint.setColor(getColorForState(android.R.attr.state_selected));
            }
        } else {
            mLinesPaint.setStrokeWidth(mLineStroke);
            mLinesPaint.setColor(getColorForState(-android.R.attr.state_focused));
        }
    }
}