package com.softbay.android.baseproject.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import baycon.android.common.model.BaseModel
import com.softbay.android.baseproject.databinding.CommonHeaderBinding

interface ItemClickListener {

    fun onClick(view: View, position: Int, isLongClick: Boolean )

}

interface ItemResourceClickListener {

    fun onClick(resourceId:Int, position: Int, isLongClick: Boolean )

}

abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun onBindView( item: BaseModel)

}

class HeaderViewHolder(private val binding: CommonHeaderBinding): BaseViewHolder( binding.root ){
    override fun onBindView( item: BaseModel) {
        binding.baseModel = item
        binding.executePendingBindings()
    }
}