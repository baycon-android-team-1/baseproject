package baycon.android.common.notification

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import androidx.core.app.NotificationCompat
import baycon.android.common.constant.sharedpreference.AppPreference
import com.softbay.android.baseproject.R
import com.softbay.android.baseproject.ui.design.MenuActivity

object DefaultNotification {

    fun startNotification( context: Context, appPreference: AppPreference,
                           title: String?, content: String?, type: String, url: String?, mipmapId: Int = R.mipmap.ic_launcher ) {

        val notificationManager = context.getSystemService( Context.NOTIFICATION_SERVICE ) as NotificationManager
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !appPreference.isCreateEventNotification ) {

            appPreference.isCreateEventNotification = true

            val notificationChannel =

                NotificationChannel (
                    "NOTIFICATION_ID_EVENT",
                    "NOTIFICATION_NAME_EVENT",
                    NotificationManager.IMPORTANCE_HIGH ).apply {

                        enableLights( false )
                        enableVibration( true )
                        setShowBadge( false )

                }

            notificationManager.createNotificationChannel( notificationChannel )

        } else {

            val processIntent = Intent( context, MenuActivity::class.java ).apply {

                addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK )
                putExtra( "MSG_TYPE", type )

                if ( type == "TYPE_LINK" && !url.isNullOrEmpty() ) {
                    putExtra( "KEY_URL", url )
                }

            }

            val pinIntent = PendingIntent.getActivity( context, System.currentTimeMillis().toInt(), processIntent, 0 )

            val builder = NotificationCompat.Builder( context, "NOTIFICATION_ID_EVENT" ).apply {

                setLargeIcon( BitmapFactory.decodeResource( context.resources, R.mipmap.ic_launcher ) )
                setSmallIcon( mipmapId )
                setTicker( content )
                setWhen( System.currentTimeMillis() )
                setNumber( 0 )
                setContentTitle( title ?: "sampleTitle" )
                setContentText( content ?: "SampleContent" )
                setContentIntent( pinIntent )
                setAutoCancel( true )

            }

            val notification : Notification = builder.build()
            notificationManager.notify( 101010, notification )

        }

    }

    fun cancelNotification( context: Context ) {

        val notificationManager = context.getSystemService( Context.NOTIFICATION_SERVICE ) as NotificationManager
        notificationManager.cancel( 101010 )

    }

}