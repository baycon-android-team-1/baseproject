package baycon.android.common.model

import androidx.room.Ignore

open class BaseModel {

    companion object {
        const val HEADER = 0
        const val BODY = 1
//        const val BODY_COUNTRY = 2
//        const val BODY_HEADER_FILE = 3
    }

    @Ignore
    var viewType: Int = BODY

    @Ignore
    var title:String = ""

    @Ignore
    var sort = 0

}