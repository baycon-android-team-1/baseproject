package baycon.android.common.contact.foreground

import android.app.Activity
import android.app.Application
import android.os.Bundle
import baycon.android.common.constant.sharedpreference.AppPreference
import baycon.android.common.contact.ContactSync
import javax.inject.Inject

class Foreground @Inject constructor( private val application: Application,
                                      private val appPreference: AppPreference,
                                      private val contactSync: ContactSync ): Application.ActivityLifecycleCallbacks {

    init {

        appPreference.isContactUpdate = false
        application.registerActivityLifecycleCallbacks( this )

    }

    fun release() {

        application.unregisterActivityLifecycleCallbacks( this )

    }

    private var running = 0

    enum class AppStatus {

        BACKGROUND, RETURNED_TO_FOREGROUND, FOREGROUND

    }

    private var appStatus: AppStatus = AppStatus.BACKGROUND

    fun isBackground(): Boolean = appStatus.ordinal == AppStatus.BACKGROUND.ordinal

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {}

    override fun onActivityStarted(activity: Activity) {

        if ( appPreference.isContactUpdate ) {

            appPreference.isContactUpdate = false
            contactSync.startSync()

        }

        if ( ++running == 1 ) {

            appStatus = AppStatus.RETURNED_TO_FOREGROUND

        } else if ( running > 1 ) {

            appStatus = AppStatus.FOREGROUND

        }


    }

    override fun onActivityResumed(activity: Activity) {}

    override fun onActivityPaused(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {

        if ( --running == 0 ) {

            appStatus = AppStatus.BACKGROUND

        }

    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {}

    override fun onActivityDestroyed(activity: Activity) {}

}