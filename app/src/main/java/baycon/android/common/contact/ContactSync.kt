package baycon.android.common.contact

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.provider.ContactsContract
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import baycon.android.common.constant.LanguageCode
import baycon.android.common.db.AppDatabase
import baycon.android.common.db.contact.ContactEntity
import baycon.android.common.utils.device.DeviceInfoUtil
import baycon.android.common.utils.string.StringUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet

class ContactSync @Inject constructor( private val context: Context,
                                       private val appDatabase: AppDatabase,
                                       private val deviceInfoUtil: DeviceInfoUtil) {

    private val contactMap = MutableLiveData<HashMap<String, ContactEntity>>().apply { HashMap<String, ContactEntity>() }
    private val contactList = MutableLiveData<ArrayList<ContactEntity>>().apply { ArrayList<ContactEntity>() }

    private var firstSync = true

    fun observeMap(): LiveData<HashMap<String, ContactEntity>> {
        return contactMap
    }

    fun observeList(): LiveData<ArrayList<ContactEntity>> {
        return  contactList
    }

    private fun updateContact() {

        val tempList = appDatabase.contactDao().getContactList()

        val tempContactMap = HashMap<String, ContactEntity>()
        val tempContactList = ArrayList<ContactEntity>()

        if( tempList.isNotEmpty() ) {

            val japanList = ArrayList<ContactEntity>()
            val chinaList = ArrayList<ContactEntity>()
            val engList = ArrayList<ContactEntity>()
            val koList = ArrayList<ContactEntity>()

            for( contactModel in tempList ) {
                if( contactModel.userName.isNotEmpty() ) {
                    contactModel.searchFullNumber = contactModel.contactPhoneNumber

                    val charArray = contactModel.userName.toCharArray()
                    val charType = Character.UnicodeBlock.of(charArray[0])

                    contactModel.fullChosung = StringUtil.convertToChosung( contactModel.userName).toUpperCase( Locale.getDefault() )
                    val firstChosung = contactModel.fullChosung.substring(0 , 1)

                    /** convertToChosung Katacana 함수로 인해 주석처리 함 */
//                    contactModel.indexChosung = when( firstChosung ) {
//                        "あ", "い", "う", "え", "お" -> "あ"
//                        "か", "き", "く", "け", "こ" -> "か"
//                        "さ", "し", "す", "せ", "そ" -> "さ"
//                        "た", "ち", "つ", "て", "と" -> "た"
//                        "な", "に", "ぬ", "ね", "の" -> "な"
//                        "は", "ひ", "ふ", "へ", "ほ" -> "は"
//                        "ま", "み", "む", "め", "も" -> "ま"
//                        "や", "ゆ", "よ" -> "や"
//                        "ら", "り", "る", "れ", "ろ" -> "ら"
//                        "わ", "を", "ん" -> "わ"
//                        else -> firstChosung
//                    }

                    contactModel.indexChosung = firstChosung

                    when {
                        // 국가 언어별 정렬은 BayCall03 참조
//                        StringUtil.isChinese( charType ) -> {
//                            val pinyinArray = PinyinHelper.toTongyongPinyinStringArray( charArray[0])
//                            if( pinyinArray != null && pinyinArray.isNotEmpty() && pinyinArray[0].isNotEmpty() ) {
//                                contactModel.indexChosung = pinyinArray[0].substring(0, 1).toUpperCase(
//                                    Locale.getDefault())
//                            }
//                            engList.add( contactModel )
//                        }
                        StringUtil.isJapan( charType ) -> {
                            japanList.add( contactModel )
                        }
                        StringUtil.isEng( charArray[0] ) -> {
                            engList.add( contactModel )
                        }
                        StringUtil.isHangle( charType ) -> {
                            koList.add( contactModel )
                        }
                        else -> {
                            tempContactList.add( contactModel )
                        }
                    }

                    tempContactMap["${contactModel.contactId}@${contactModel.contactPhoneNumber}"] = contactModel
                    tempContactMap["@${contactModel.contactPhoneNumber}"] = contactModel
                }

            }


            when( deviceInfoUtil.getDeviceLanguage() ) {
                LanguageCode.LANGUAGE_JAPAN -> {
                    if( koList.isNotEmpty() )
                        tempContactList.addAll( 0, koList )
                    if( engList.isNotEmpty() )
                        tempContactList.addAll( 0, engList )
                    if( chinaList.isNotEmpty() )
                        tempContactList.addAll( 0, chinaList )
                    if( japanList.isNotEmpty() )
                        tempContactList.addAll( 0, japanList )

                }
                else -> {
                    if( chinaList.isNotEmpty() )
                        tempContactList.addAll( 0, chinaList )
                    if( japanList.isNotEmpty() )
                        tempContactList.addAll( 0, japanList )
                    if( koList.isNotEmpty() )
                        tempContactList.addAll( 0, koList )
                    if( engList.isNotEmpty() )
                        tempContactList.addAll( 0, engList )

                }
            }

        }

        contactList.setValue( tempContactList )
        contactMap.value = tempContactMap

    }

    private var isSync = false

    private val DEFAULT_PROJECTION = arrayOf(
        ContactsContract.Data.CONTACT_ID, //Contact 고유 아이디
        ContactsContract.Data.DISPLAY_NAME, //Contact 저장 이름
        ContactsContract.Data.DATA1, //Contact 전화 번호
        ContactsContract.RawContacts.VERSION, //Contact 한 row별 업데이트 버전
        ContactsContract.Data.PHOTO_ID )

//    ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER

    fun startSync() {

        if (ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        if (!isSync) {
            isSync = true

            CoroutineScope(Dispatchers.IO).launch {

                val cur = context.contentResolver.query(
                    ContactsContract.Data.CONTENT_URI,
                    DEFAULT_PROJECTION,
                    "${ContactsContract.Data.MIMETYPE} = ?",
                    arrayOf(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE),
                    "${ContactsContract.Data.CONTACT_ID} ASC"
                )

                val insertMap = HashMap<String, ContactEntity>()
                val contactCompareMap = appDatabase.contactDao().getContactListCompareMap()
                val nameCompareMap = HashSet<String>()

                if (cur != null) {

                    val contactIdIdx = cur.getColumnIndex(ContactsContract.Data.CONTACT_ID)
                    val contactVersionIdx = cur.getColumnIndex(ContactsContract.RawContacts.VERSION)
                    val displayNameIdx = cur.getColumnIndex(ContactsContract.Data.DISPLAY_NAME)
                    val phoneNumberIdx = cur.getColumnIndex(ContactsContract.Data.DATA1)
                    val photoIdIdx = cur.getColumnIndex(ContactsContract.Data.PHOTO_ID)
                    while (cur.moveToNext()) {

                        val contactId = cur.getInt(contactIdIdx)
                        val contactsVersion = cur.getInt(contactVersionIdx)
                        val displayName = cur.getString(displayNameIdx)
                        val phoneNumber = cur.getString(phoneNumberIdx)
                        val photoId = cur.getInt(photoIdIdx)

                        if (displayName == null || phoneNumber == null)
                            continue

                        if (phoneNumber.length in 3..20) {

                            val compareKey = "$phoneNumber@$contactId"

                            if (!nameCompareMap.contains(compareKey)) {

                                var contactModel = contactCompareMap[compareKey]

                                if (contactModel != null) {

                                    if (displayName != contactModel.userName) {
                                        contactModel.isUpdate = true
                                    }

                                    if (!contactModel.isTempInAddress) {
                                        contactModel.isUpdate = true
                                    }

                                    contactModel.userName = displayName
                                    contactModel.contactId = contactId
                                    contactModel.contactVersion = contactsVersion
                                    contactModel.displayPhoneNumber = phoneNumber
                                    contactModel.contactPhoneNumber =
                                        phoneNumber.replace(Regex("[^0-9+]"), "")
                                    contactModel.isInAddress = true
                                    contactModel.photoId = photoId
                                    contactCompareMap[compareKey] = contactModel

                                } else {

                                    contactModel = ContactEntity()

                                    contactModel.userName = displayName
                                    contactModel.contactId = contactId
                                    contactModel.contactVersion = contactsVersion
                                    contactModel.displayPhoneNumber = phoneNumber
                                    contactModel.contactPhoneNumber =
                                        phoneNumber.replace(Regex("[^0-9+]"), "")
                                    contactModel.isInAddress = true
                                    contactModel.photoId = photoId
                                    insertMap[compareKey] = contactModel
                                }

                                nameCompareMap.add(compareKey)
                            }

                        }
                    }


                    var isDataChange = false
                    val updateList =
                        contactCompareMap.values.filter { it.isUpdate || !it.isInAddress }

                    if (updateList.isNotEmpty()) {

                        isDataChange = true
                        appDatabase.contactDao().commitContact(updateList)
                    }

                    val insertList = insertMap.values.filter { it.contactPhoneNumber.isNotEmpty() }

                    if (insertList.isNotEmpty()) {

                        isDataChange = true
                        appDatabase.contactDao().commitContact(insertList)

                    }

                    if (isDataChange || firstSync) {

                        firstSync = false

                        launch(Dispatchers.Main) {

                            updateContact()

                        }


                    }

                    cur.close()

                }

                isSync = false
            }

        }
    }
}