package baycon.android.common.contact

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.database.ContentObserver
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import androidx.core.content.ContextCompat
import baycon.android.common.constant.sharedpreference.AppPreference
import baycon.android.common.contact.foreground.Foreground
import javax.inject.Inject

class ContactObserver @Inject constructor( private val appContext: Context,
                                           private val appPreference: AppPreference,
                                           private val foreground: Foreground,
                                           private val contactSync: ContactSync ) {


    private var isRegister = false

    private val observer = object : ContentObserver( Handler( Looper.getMainLooper() ) ) {

        override fun onChange(selfChange: Boolean) {
            super.onChange(selfChange)

            if ( foreground.isBackground() ) {
                appPreference.isContactUpdate = false
            } else {
                contactSync.startSync()
            }

        }

    }

    init {

        registerObserver()

    }

    fun registerObserver() {

        if ( !isRegister && ContextCompat.checkSelfPermission( appContext, Manifest.permission.READ_CONTACTS ) == PackageManager.PERMISSION_GRANTED ) {

            appContext.contentResolver.registerContentObserver( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, false, observer )
            isRegister = true

        }

    }

    fun unRegisterObserver() {

        if ( isRegister ) {

            appContext.contentResolver.unregisterContentObserver( observer )
            isRegister = false

        }

    }

}