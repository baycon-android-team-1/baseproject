package baycon.android.common.extension

import android.content.res.Resources
import android.graphics.*


/** Int to DP */
val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

/** reset bitmap Bright & Contrast */
fun Bitmap.setBrightnessContrast(brightness:Float = 0.0F, contrast:Float = 1.0F ): Bitmap {

    val bitmap = copy(Bitmap.Config.ARGB_8888,true)
    val paint = Paint()

    // brightness -200..200, 0 is default
    // contrast 0..2, 1 is default
    // you may tweak the range
    val matrix = ColorMatrix(
        floatArrayOf(
            contrast, 0f, 0f, 0f, brightness,
            0f, contrast, 0f, 0f, brightness,
            0f, 0f, contrast, 0f, brightness,
            0f, 0f, 0f, 1f, 0f
        )
    )

    val filter = ColorMatrixColorFilter(matrix)
    paint.colorFilter = filter

    Canvas( bitmap ).drawBitmap(this,0f,0f, paint)
    return bitmap
}