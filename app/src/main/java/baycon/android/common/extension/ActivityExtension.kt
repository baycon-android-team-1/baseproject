package baycon.android.common.extension

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.view.WindowCompat


// TODO: api 30 window InsetController
/** Control Status Bar */
@Suppress("DEPRECATION")
fun Activity.setFullScreenLightStatusBar(isLightStatusBar:Boolean ) {

    window.statusBarColor = Color.TRANSPARENT
    if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R ) {
        WindowCompat.setDecorFitsSystemWindows( window, false )
        WindowCompat.getInsetsController( window, window.decorView )?.apply {
            isAppearanceLightStatusBars = isLightStatusBar
        }
    } else {
        if( isLightStatusBar ) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        } else {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
        }
    }
}

/** Control HardWare KeyBoard */
fun Activity.hideKeyboard() {

    val view = this.currentFocus
    view?.let {
        val imm: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow( view.windowToken, 0 )
    }

}


fun Activity.statusBarHeight(): Int {

    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId)
    else 0

}
