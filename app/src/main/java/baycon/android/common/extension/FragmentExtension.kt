package baycon.android.common.extension

import android.widget.FrameLayout
import androidx.fragment.app.Fragment


fun Fragment.hideKeyboard() {

    val view = requireActivity().hideKeyboard()

}

fun Fragment.statusBarHeight(): Int {

    val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
    return if (resourceId > 0) resources.getDimensionPixelSize(resourceId)
    else 0

}

fun Fragment.marginTopForStatusBar() {

    (this.view?.layoutParams as FrameLayout.LayoutParams).topMargin = this.statusBarHeight()

}