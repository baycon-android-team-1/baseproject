package baycon.android.common.di

import android.content.Context
import androidx.room.Room
import com.softbay.android.baseproject.api.repository.CommonRepository
import baycon.android.common.contact.ContactSync
import baycon.android.common.db.AppDatabase
import baycon.android.common.db.contact.ContactEntity
import baycon.android.common.utils.device.DeviceInfoUtil
import baycon.android.common.constant.sharedpreference.AppPreference
import com.softbay.android.baseproject.api.contract.CommonDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn ( SingletonComponent::class )
class HiltDependenciesModule {

    @Provides
    @Singleton
    fun provideSampleUtil( @ApplicationContext context: Context): SampleUtil = SampleUtil( context )


    @Provides
    @Singleton
    fun provideAppPreference( @ApplicationContext context: Context ) = AppPreference( context )

    @Provides
    @Singleton
    fun provideDeviceInfoUtil( @ApplicationContext context: Context ) = DeviceInfoUtil( context )

    @Provides
    @Singleton
    fun provideAppDatabase( @ApplicationContext context: Context ) =
        Room.databaseBuilder( context, AppDatabase::class.java, "base_db")
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    /** Contact */

    @Provides
    @Singleton
    fun provideContactDao( db: AppDatabase ) = db.contactDao()

    @Provides
    @Singleton
    fun provideContactSync( @ApplicationContext context: Context,
                            appDatabase: AppDatabase,
                            deviceInfoUtil: DeviceInfoUtil ) = ContactSync( context, appDatabase, deviceInfoUtil )

    @Provides
    @Singleton
    fun provideCompareContactMap(): HashMap<String, ContactEntity> {
        return HashMap()
    }

    @Provides
    @Singleton
    fun provideSelectedContactList(): ArrayList<ContactEntity> {
        return ArrayList()
    }

    @Provides
    @Singleton
    fun provideCommonDataSource(): CommonDataSource = CommonDataSource()

    @Provides
    @Singleton
    fun provideCommonRepository( commonDataSource: CommonDataSource ): CommonRepository = CommonRepository( commonDataSource )
}