package baycon.android.common.di

import android.content.Context
import timber.log.Timber
import javax.inject.Inject

class SampleUtil @Inject constructor( private val context: Context ) {

    fun sample() {
        Timber.i("inject sample util")
    }
}