package baycon.android.common.db

import androidx.room.Database
import androidx.room.RoomDatabase
import baycon.android.common.db.contact.ContactDao
import baycon.android.common.db.contact.ContactEntity

@Database( entities = [ ( ContactEntity::class) /* add another entity */ ], version = 1, exportSchema = false )
abstract class AppDatabase: RoomDatabase() {

    abstract fun contactDao(): ContactDao
}