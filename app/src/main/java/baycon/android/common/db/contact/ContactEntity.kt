package baycon.android.common.db.contact

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import baycon.android.common.constant.CommonCode
import baycon.android.common.model.BaseModel

/** table name & primary key set another value which if you want, this is only sample code */
@Entity(tableName = "Contact", primaryKeys = [ "contactId", "contactPhoneNumber"])
class ContactEntity: BaseModel(), Cloneable {

    @ColumnInfo( name = "contactId" )
    var contactId: Int = -1

    @ColumnInfo( name = "contactVersion" )
    var contactVersion: Int = 0

    @ColumnInfo( name = "userName" )
    var userName: String = ""

    @ColumnInfo( name = "contactPhoneNumber" )
    var contactPhoneNumber: String = ""

    @ColumnInfo( name = "photoId" )
    var photoId: Int = -1

    @ColumnInfo( name = "isFavorite" )
    var isFavorite: Boolean = false

    @ColumnInfo( name = "isInAddress" )
    var isInAddress: Boolean = false

    @ColumnInfo( name = "displayPhoneNumber" )
    var displayPhoneNumber = ""

    @Ignore
    var isUpdate = false
    @Ignore
    var isTempInAddress: Boolean = false

    @Ignore
    var displayColorIndex: Int = 0
    @Ignore
    var searchFullNumber = ""
    @Ignore
    var indexChosung = ""
    @Ignore
    var fullChosung = ""
    @Ignore
    var isCheckState = CommonCode.ContactCheckState.NONE

    companion object {
        fun getContactModel( contactMap: HashMap<String, ContactEntity>?, phoneNumber: String?, contactId: Int ): ContactEntity? {

            phoneNumber ?: return null

            var contactModel: ContactEntity? = null
            if ( contactId >= 0 ) {

                contactModel = contactMap?.get( "$contactId@$phoneNumber" )

            }

            contactModel ?: return null

            return contactMap?.get( "@$phoneNumber" )
        }
    }

    public override fun clone(): Any {
        return super.clone()
    }
}