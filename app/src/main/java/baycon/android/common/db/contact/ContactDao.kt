package baycon.android.common.db.contact

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import baycon.android.common.db.BaseDao
import baycon.android.common.utils.country.CountryUtil
import baycon.android.common.utils.phonenumber.PhoneNumberUtilsCompat

@Dao
abstract class ContactDao: BaseDao<ContactEntity> {

    companion object {

        fun getNumberToName( nationalId: String, phoneId: String,
                             contactMap: HashMap<String, ContactEntity>?,
                             countryUtil: CountryUtil ): String {

            val searchFullNumber = "@+$nationalId$phoneId"
            val searchNumber = "@$phoneId"
            val contactEntity = contactMap?.get( searchFullNumber ) ?: contactMap?.get( searchNumber )

            val countryItem = countryUtil.countryCodeTable[ nationalId ]

            return contactEntity?.userName ?: PhoneNumberUtilsCompat.formatPhoneNumber( phoneId, countryItem?.iSOCountryCode ?: "JP" )
        }

    }

    @Query( "SELECT * FROM Contact ORDER BY userName COLLATE LOCALIZED ASC" )
    abstract fun getContactList(): List<ContactEntity>

    @Query( "DELETE FROM Contact" )
    abstract fun deleteAll()

    @Query( "SELECT * FROM Contact WHERE contactId = :contactId" )
    abstract fun searchContact( contactId: Int ): ContactEntity?

    @Query( "SELECT * FROM Contact WHERE contactPhoneNumber = :contactPhoneNumber LIMIT 0, 1" )
    abstract fun searchContact( contactPhoneNumber: String ): ContactEntity?

    @Query( "SELECT * FROM Contact WHERE contactId = :contactId and contactPhoneNumber = :contactPhoneNumber" )
    abstract fun searchContact( contactId: Int, contactPhoneNumber: String ): ContactEntity?

    open fun getContactListCompareMap(): HashMap<String, ContactEntity> {

        val contactInfoCompareMap = HashMap<String, ContactEntity>()

        val contactList = getContactList()

        for( contactModel in contactList ) {
            contactModel.isTempInAddress = contactModel.isInAddress
            contactModel.isInAddress = false
            contactInfoCompareMap[ "${contactModel.contactPhoneNumber}@${contactModel.contactId}"] = contactModel
        }


        return contactInfoCompareMap
    }

    @Transaction
    open fun commitContact(contactList: List<ContactEntity>) {

        for( contact in contactList ) {

            when( contact.isInAddress ){
                true -> insert( contact )
                false -> delete( contact )
            }

        }

    }

}