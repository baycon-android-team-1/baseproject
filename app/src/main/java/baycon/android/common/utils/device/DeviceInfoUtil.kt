package baycon.android.common.utils.device

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.Settings
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import baycon.android.common.constant.CommonCode
import baycon.android.common.constant.LanguageCode
import com.scottyab.rootbeer.RootBeer
import java.util.*
import javax.inject.Inject

@Suppress("DEPRECATION")
class DeviceInfoUtil @Inject constructor(private val appContext: Context /* , private val appPreference: AppPreference */ ) {

    lateinit var defaultCountryIso: String

    init {
        reloadDefaultCountry()
    }

    fun reloadDefaultCountry(){
//        defaultCountryIso = appPreference.simCountry ?: getRealSimCountry()
        defaultCountryIso = getRealSimCountry()
    }

//    fun simCountry():String = appPreference.simCountry ?: getRealSimCountry()
    fun simCountry():String = getRealSimCountry()

    fun networkCountry():String {

//        var networkCountry = getRealNetworkCountry()
//        if( networkCountry.isEmpty() ) {
//            networkCountry = appPreference.networkCountry ?: ""
//        }
//
//        return networkCountry
        return getRealNetworkCountry()

    }

    /** language with appPreference defauly : Japan Language*/
//    fun language():String {
//        val lan:String? = appPreference.language

//        val strDeviceLanguage = getDeviceLanguage() /* original comments */

//        return lan ?: LanguageCode.LANGUAGE_JAPAN
//        return lan ?: LanguageUtil.getLanguageFromDeviceLanguage( strDeviceLanguage )  /* original comments */
//    }

    /**
     * gradle write in 2021.09.07
     * gradle with : implementation 'com.scottyab:rootbeer-lib:0.0.7'
     */
    fun getRooting(): String {

        return try {
                    val rootBeer = RootBeer(appContext)
                    return if(rootBeer.isRooted) {
                        CommonCode.RootingType.ROOTING
                    } else {
                        CommonCode.RootingType.NOT_ROOTING
                    }
                } catch(e: Exception) {
                    CommonCode.RootingType.CHECK_ERROR
                }

    }


    fun getDeviceLanguage(): String {
        return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            appContext.resources.configuration.locales[0].language
        } else {
            appContext.resources.configuration.locale.language
        }

    }


    /**
     * 현재 위치국가를 현재 심 정보로 가져온다.
     */
    fun getRealNetworkCountry(): String {
        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return tManager.networkCountryIso.toUpperCase(Locale.getDefault())
    }


    /**
     * 통신 가입국가를 현재 심 정보로 가져온다.
     */
    fun getRealSimCountry(): String {
        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return tManager.simCountryIso.toUpperCase(Locale.getDefault())
    }

    /**
     * 유심 전화번호를 가져온다.
     * TODO : Add ManifestPermission : READ_PHONE_STATE
     */
    @SuppressLint("MissingPermission", "HardwareIds")
    fun getSimPhoneNumber(): String {
        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager


        return if( ContextCompat.checkSelfPermission( appContext, Manifest.permission.READ_PHONE_STATE ) == PackageManager.PERMISSION_GRANTED ) tManager.line1Number ?: "" else ""
    }

    /** get simSerialNumber with appPreference( CustomSharePreference ) */
//    fun getSimSerialNumber(): String {
//        return when{
//            appPreference.simNumber != null -> appPreference.simNumber ?: ""
//            appPreference.simNumber29 != null -> appPreference.simNumber29 ?: ""
//            Build.VERSION.SDK_INT > Build.VERSION_CODES.P -> {
//                if( appPreference.simNumber29.isNullOrEmpty() ){
//                    appPreference.simNumber29 = getRealSimSerialNumber()
//                }
//                appPreference.simNumber29!!
//            }
//            else -> {
//                if( appPreference.simNumber.isNullOrEmpty() ){
//                    appPreference.simNumber = getRealSimSerialNumber()
//                }
//                appPreference.simNumber!!
//            }
//        }
//    }

    /** setSimSerialNumber with appPreference( CustomSharedPreference ) */
//    fun setSimSerialNumber() {
//        if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.P ) {
//            appPreference.simNumber = null
//            appPreference.simNumber29 = getRealSimSerialNumber()
//        } else {
//            appPreference.simNumber29 = null
//            appPreference.simNumber = getRealSimSerialNumber()
//        }
//    }

    /**
     * 심 시리얼 넘버
     * @param context
     * @return
     */
    @SuppressLint("MissingPermission", "HardwareIds")
    fun getRealSimSerialNumber(): String {

        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

//        return if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.R ) {
//            tManager.subscriptionId.toString()
//        }else
        return if ( Build.VERSION.SDK_INT > Build.VERSION_CODES.P ) {
            val sm = SubscriptionManager.from( appContext )
            if( sm.accessibleSubscriptionInfoList != null && sm.accessibleSubscriptionInfoList.size > 0 ){
                sm.accessibleSubscriptionInfoList[0].iccId
            } else if( ( tManager.line1Number ?: "" ).isNotEmpty() ){
                tManager.line1Number
            } else {
                getSimMno()
            }
        } else {
            if( ContextCompat.checkSelfPermission( appContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED )
                tManager.simSerialNumber ?: ""
            else ""
        }
    }

    /**
     * 디바이스 os 버전
     * @return
     */
    fun getOSVersion(): String {
        return Build.VERSION.RELEASE
    }

    /**
     * 앱 버전
     */
    fun getClientVersion():String{
        var ver = "1.0.0"

        try {
            val i = appContext.packageManager.getPackageInfo( appContext.packageName, 0 )
            ver = i.versionName
        } catch(e: Exception) {}

        return ver
    }

    /**
     * 디바이스 아이디
     */
    @SuppressLint("HardwareIds", "MissingPermission")
    fun getDeviceId(): String{

        /* deviceId with appPreference */
//        var deviceId:String? = appPreference.deviceId
        var deviceId: String? = null

        if( deviceId.isNullOrEmpty() ) {
            try {
                val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                if(ContextCompat.checkSelfPermission( appContext, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    deviceId = Settings.Secure.getString( appContext.contentResolver, Settings.Secure.ANDROID_ID ) ?: when {
                        Build.VERSION.SDK_INT > Build.VERSION_CODES.P -> {
                            Settings.Secure.getString( appContext.contentResolver, Settings.Secure.ANDROID_ID )
                        }
                        Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
                            tManager.imei ?: tManager.meid ?: ""
                        } else -> {
                            tManager.deviceId ?: ""
                        }
                    }

                }

                if(deviceId.isNullOrEmpty() || deviceId == "0") {
                    deviceId = UUID.randomUUID().toString()
                }

//                appPreference.deviceId = deviceId
            }catch( e:Exception ){}
        }

        return deviceId ?: ""
    }

    /**
     * 통신 가입 통신사 가져오기
     * @return
     */
    fun getSimMno(): String {

        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        return tManager.simOperator.toUpperCase(Locale.getDefault())

    }

    /**
     * 통신 가입 통신사 가져오기
     * @return
     */
    fun getNetworkMno(): String {

        val tManager = appContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager

        return tManager.networkOperator.toUpperCase(Locale.getDefault())

    }

    /**
     * 디바이스 모델
     */
    fun getDeviceModel(): String{
        return Build.MODEL
    }


    /** Add AndroidManifest Permission Network State */
    @SuppressLint("MissingPermission")
    fun getNetWorkState(): String {
        try {
            val connectivityManager = appContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q ) {
                val capabilities: NetworkCapabilities? = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
                if( capabilities != null ) {
                    return when {
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> "NT0001"
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> "NT0003"
                        capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> "NT0003"
                        else -> return "NT0000"
                    }
                } else {
                    return "NT0000"
                }
            } else {

                val mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                val wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                val activeInfo = connectivityManager.activeNetworkInfo

                //			int dNetType = wimax.getType(); //
                val dNetSubtype = activeInfo?.subtype // LTE 여부

                return if (wifi != null && wifi.isAvailable && wifi.isConnected) { // Wifi
                    "NT0001"
                } else if ( dNetSubtype != null && dNetSubtype == 13) { // LTE
                    "NT0003"
                } else if (mobile != null && mobile.isAvailable && mobile.isConnected) { // 3g
                    "NT0002"
                } else {
                    "NT0001"
                }
            }
        } catch(e: Exception) {
            return "NT0000"
        }

    }

}