package baycon.android.common.utils.file

import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.DatabaseUtils
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.text.TextUtils
import android.webkit.MimeTypeMap
import androidx.core.content.FileProvider
import androidx.documentfile.provider.DocumentFile
import okhttp3.ResponseBody
import timber.log.Timber
import java.io.*
import java.text.DecimalFormat

object FileHelperKt {

    private const val DOCUMENTS_DIR = "documents"
    // configured android:authorities in AndroidManifest (https://developer.android.com/reference/android/support/v4/content/FileProvider)
    private const val AUTHORITY = "YOUR_AUTHORITY.provider"
    private const val HIDDEN_PREFIX = "."

    private val DEBUG = false // Set to true to enable logging
    /**
     * File and folder comparator. TODO Expose sorting option method
     */
    var sComparator = { f1: File, f2: File ->
        // Sort alphabetically by lower case, which is much cleaner
        f1.name.toLowerCase().compareTo(
            f2.name.toLowerCase()
        )
    }
    /**
     * File (not directories) filter.
     */
    var sFileFilter = { file: File ->
        val fileName = file.getName()
        // Return files only (not directories) and skip hidden files
        file.isFile() && !fileName.startsWith(HIDDEN_PREFIX)
    }
    /**
     * Folder (directories) filter.
     */
    var sDirFilter = { file: File ->
        val fileName = file.name
        // Return directories only and skip hidden directories
        file.isDirectory && !fileName.startsWith(HIDDEN_PREFIX)
    }

    @Suppress("DEPRECATION")
    val downloadsDir: File
        get() = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)


    fun copyFile( srcFilePath:String, descFilePath:String ) {
        val srcFile = File( srcFilePath )

        if( srcFile.exists() ) {
            FileInputStream( srcFile ).use {input ->

                FileOutputStream( File( descFilePath ) ).use { output ->
                    input.copyTo( output )
                }

            }

        }
    }

    fun copyInternal( context:Context, parent:File, uri: Uri? ):File? {
        if( uri == null )
            return null

        val pfd = context.contentResolver.openInputStream(uri)
        return pfd?.use { input ->
            var ret: File? = null
            DocumentFile.fromSingleUri(context, uri)?.apply {
                this.name?.let { name ->
                    ret = File( parent, name)
                    ret?.let {
                        FileOutputStream(it).use { output ->
                            input.copyTo(output)
                        }
                    }

                }
            }
            return ret
        }
    }

    /**
     * Gets the extension of a file name, like ".png" or ".jpg".
     *
     * @param uri
     * @return Extension including the dot("."); "" if there is no extension;
     * null if uri was null.
     */
    fun getExtension(uri: String?): String? {
        if (uri == null) {
            return null
        }

        val dot = uri.lastIndexOf(".")
        return if (dot >= 0) {
            uri.substring(dot)
        } else {
            // No extension.
            ""
        }
    }

    /**
     * @return Whether the URI is a local one.
     */
    fun isLocal(url: String?): Boolean {
        return url != null && !url.startsWith("http://") && !url.startsWith("https://")
    }

    /**
     * @return True if Uri is a MediaStore Uri.
     * @author paulburke
     */
    fun isMediaUri(uri: Uri): Boolean {
        return "media".equals(uri.authority!!, ignoreCase = true)
    }

    /**
     * Convert File into Uri.
     *
     * @param file
     * @return uri
     */
    fun getUri(file: File?): Uri? {
        return if (file != null) Uri.fromFile(file) else null
    }

    /**
     * Returns the path only (without file name).
     *
     * @param file
     * @return
     */
    fun getPathWithoutFilename(file: File?): File? {
        if (file != null) {
            if (file.isDirectory) {
                // no file to be split off. Return everything
                return file
            } else {
                val filename = file.name
                val filepath = file.absolutePath

                // Construct path without file name.
                var pathwithoutname = filepath.substring(
                    0,
                    filepath.length - filename.length
                )
                if (pathwithoutname.endsWith("/")) {
                    pathwithoutname = pathwithoutname.substring(0, pathwithoutname.length - 1)
                }
                return File(pathwithoutname)
            }
        }
        return null
    }

    /**
     * @return The MIME type for the given file.
     */
    fun getMimeType(file: File): String? {
        val extension = getExtension(file.name)
        if (extension!!.isNotEmpty()) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1))
        } else {
            return "application/octet-stream"
        }
    }

    /**
     * @return The MIME type for the give Uri.
     */
    fun getMimeType(context: Context, uri: Uri): String? {
        val file = File(getPathFromURI(context, uri)!!)
        return getMimeType(file)
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is local.
     */
    fun isLocalStorageDocument(uri: Uri): Boolean {
        return AUTHORITY == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Drive.
     */
    private fun isGoogleDriveUri(uri: Uri): Boolean {
        return "com.google.android.apps.docs.storage" == uri.authority
    }

//    content://com.google.android.apps.docs.storage/document/acc%3D2%3Bdoc%3Dencoded%3D%2FP3zS16atq1%2BrvfyBnM0yC2kS5QgeIjVUIDs6ID1qL21wXU1
    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    @Suppress("DEPRECATION")
    fun getDataColumn(context: Context, uri: Uri?, selection: String?, selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = MediaStore.Files.FileColumns.DATA
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                if (DEBUG)
                    DatabaseUtils.dumpCursor(cursor)

                val columnIndex = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(columnIndex)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    private fun contentResolverCopy( context:Context, uri: Uri ):String? {
        val pfd = context.contentResolver.openInputStream(uri)
        return pfd?.use { input ->
            var ret: String? = null
            DocumentFile.fromSingleUri(context, uri)?.apply {
                this.name?.let { name ->
                    ret = File( DirUtils.getWriteTempDir(context), name).absolutePath
                    ret?.let {
                        FileOutputStream(it).use { output ->
                            input.copyTo(output)
                        }
                    }

                }
            }
            return ret
        }
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.<br></br>
     * <br></br>
     * Callers should check whether the path is local before assuming it
     * represents a local file.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @see .isLocal
     * @see .getFile
     */
    @Suppress("DEPRECATION")
    fun getPathFromURI(context: Context, uri: Uri? ): String? {
        if( uri == null )
            return null


//        Timber.d(
//            "[app] FileHelper - Authority: ${uri.authority}, " +
//                    "Fragment: ${uri.fragment}, " +
//                    "Port: ${uri.port}, " +
//                    "Query: ${uri.query}, " +
//                    "Scheme: ${uri.scheme}, " +
//                    "Host: ${uri.host}, " +
//                    "Segments: ${uri.pathSegments}"
//        )

//        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
//        if (isKitKat && DocumentsContract.isDocumentUri( context, uri ) ) {
        if ( DocumentsContract.isDocumentUri( context, uri ) ) {
            // LocalStorageProvider
            if( isLocalStorageDocument( uri ) ) {
                // The path is the id
                return DocumentsContract.getDocumentId(uri)
            }
            // ExternalStorageProvider
            else if( isExternalStorageDocument( uri ) ) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split((":").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return (Environment.getExternalStorageDirectory()).toString() + "/" + split[1]
                }
            }
            // DownloadsProvider
            else if( isDownloadsDocument( uri ) ) {

                if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {

                    var cursor:Cursor? = null
                    try{
                        cursor = context.contentResolver.query( uri, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME), null, null, null )
                        if( cursor != null && cursor.moveToFirst() ) {
                            val fileName = cursor.getString( 0 )
                            val path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                            if( !TextUtils.isEmpty( path ) ) {
                                return path
                            }
                        }
                    } finally {
                        cursor?.close()
                    }

                    val id = DocumentsContract.getDocumentId(uri)
                    if (id != null && id.startsWith("raw:")) {
                        return id.substring(4)
                    }
                    val contentUriPrefixesToTry = arrayOf("content://downloads/public_downloads", "content://downloads/my_downloads")
                    for (contentUriPrefix in contentUriPrefixesToTry) {
                        try {
                            val contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), java.lang.Long.valueOf(id!!))
                            val path = getDataColumn(context, contentUri, null, null)
                            if (path != null) {
                                return path
                            }
                        } catch (e: Exception) {
                        }
                    }

                    // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                    val fileName = getFileName(context, uri)
                    val cacheDir = getDocumentCacheDir(context)
                    val file = generateFileName(fileName, cacheDir)
                    var destinationPath: String? = null
                    if (file != null) {
                        destinationPath = file.absolutePath
                        saveFileFromUri(context, uri, destinationPath)
                    }
                    return destinationPath

                } else {

                    val id = DocumentsContract.getDocumentId(uri)
                    if( id.startsWith("raw:" ) ) {
                        return id.replaceFirst("raw:".toRegex(), "")
                    }
                    val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                    return getDataColumn( context, contentUri, null, null )
                }

            }
            // MediaProvider
            else if ( isMediaDocument( uri ) ) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split((":").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return if( contentUri != null ) {
                    getDataColumn(context, contentUri, selection, selectionArgs)
                } else {
                    contentResolverCopy( context, uri )
                }
            }  else if( "content".equals(uri.scheme!!, ignoreCase = true ) ) {
                if( isGooglePhotosUri( uri ) || isGoogleDriveUri( uri ) ) {
                    return contentResolverCopy( context, uri )
                }

                return getDataColumn( context, uri, null, null )
            }
        }
        else if ("content".equals( uri.scheme, ignoreCase = true ) ) {

            if( isGooglePhotosUri( uri ) || isGoogleDriveUri( uri ) ) {
                return contentResolverCopy( context, uri )
            }

            return getDataColumn( context, uri, null, null )
        }
        else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }

        return null
    }

    /**
     * Convert Uri into File, if possible.
     *
     * @return file A local file that the Uri was pointing to, or null if the
     * Uri is unsupported or pointed to a remote resource.
     * @author paulburke
     * @see .getPath
     */
    fun getFile(context: Context, uri: Uri?): File? {
        if (uri != null) {
            val path = getPathFromURI(context, uri)
            if (path != null && isLocal(path)) {
                return File(path)
            }
        }
        return null
    }

    /**
     * Get the file size in a human-readable string.
     *
     * @param size
     * @return
     * @author paulburke
     */
    fun getReadableFileSize(size: Int): String {
        val bytesInKilobytes = 1024
        val dec = DecimalFormat("###.#")
        val kb = " KB"
        val mb = " MB"
        val gb = " GB"
        var fileSize = 0f
        var suffix = kb

        if (size > bytesInKilobytes) {
            fileSize = (size / bytesInKilobytes).toFloat()
            if (fileSize > bytesInKilobytes) {
                fileSize /= bytesInKilobytes
                if (fileSize > bytesInKilobytes) {
                    fileSize /= bytesInKilobytes
                    suffix = gb
                } else {
                    suffix = mb
                }
            }
        }
        return (dec.format(fileSize.toDouble()) + suffix).toString()
    }

    /**
     * Get the Intent for selecting content to be used in an Intent Chooser.
     *
     * @return The intent for opening a file with Intent.createChooser()
     */
    fun createGetContentIntent(): Intent {
        // Implicitly allow the user to select a particular kind of data
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        // The MIME data type filter
        intent.type = "*/*"
        // Only return URIs that can be opened with ContentResolver
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        return intent
    }

    /**
     * Creates View intent for given file
     *
     * @param file
     * @return The intent for viewing file
     */
    fun getViewIntent(context: Context, file: File): Intent {
        // Uri uri = Uri.fromFile(file);
        val uri = FileProvider.getUriForFile(context, AUTHORITY, file)
        val intent = Intent(Intent.ACTION_VIEW)
        val url = file.toString()
        if (url.contains(".doc") || url.contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (url.contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (url.contains(".ppt") || url.contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (url.contains(".xls") || url.contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (url.contains(".zip") || url.contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav")
        } else if (url.contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (url.contains(".wav") || url.contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (url.contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (url.contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if ((url.contains(".3gp") || url.contains(".mpg") || url.contains(".mpeg") ||
                    url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi"))
        ) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            intent.setDataAndType(uri, "*/*")
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        return intent
    }

    fun getDocumentCacheDir(context: Context): File {
        val dir = File(context.cacheDir, DOCUMENTS_DIR)
        if (!dir.exists()) {
            dir.mkdirs()
        }
        logDir(context.cacheDir)
        logDir(dir)

        return dir
    }

    private fun logDir(dir: File) {
        if (!DEBUG) return
        Timber.d("[app] FileHelper log dir=$dir")
        val files = dir.listFiles()
        for (file in files!!) {
            Timber.d("[app] FileHelper path:${file.path}")
        }
    }

    fun generateFileName(name: String?, directory: File): File? {
        var fName: String? = name ?: return null

        var file = File(directory, fName!!)

        if (file.exists()) {
            var fileName: String = fName
            var extension = ""
            val dotIndex = fName.lastIndexOf('.')
            if (dotIndex > 0) {
                fileName = fName.substring(0, dotIndex)
                extension = fName.substring(dotIndex)
            }

            var index = 0

            while (file.exists()) {
                index++
                fName = "$fileName($index)$extension"
                file = File(directory, fName)
            }
        }

        try {
            if (!file.createNewFile()) {
                return null
            }
        } catch (e: IOException) {
            Timber.w(e, "[app] FileHelper")
            return null
        }

        logDir(directory)

        return file
    }

    /**
     * Writes response body to disk
     *
     * @param body ResponseBody
     * @param path file path
     * @return File
     */
    fun writeResponseBodyToDisk(body: ResponseBody, path: String): File? {
        try {
            val target = File(path)

            var inputStream: InputStream? = null
            var outputStream: OutputStream? = null

            try {
                val fileReader = ByteArray(4096)

                inputStream = body.byteStream()
                outputStream = FileOutputStream(target)

                while (true) {
                    val read = inputStream.read(fileReader)

                    if (read == -1) {
                        break
                    }

                    outputStream.write(fileReader, 0, read)
                }

                outputStream.flush()

                return target
            } catch (e: IOException) {
                return null
            } finally {
                inputStream?.close()

                outputStream?.close()
            }
        } catch (e: IOException) {
            return null
        }
    }

    private fun saveFileFromUri(context: Context, uri: Uri, destinationPath: String) {
        var inputStream: InputStream? = null
        var bos: BufferedOutputStream? = null
        try {
            inputStream = context.contentResolver.openInputStream(uri)
            bos = BufferedOutputStream(FileOutputStream(destinationPath, false))
            val buf = ByteArray(1024)
            inputStream!!.read(buf)
            do {
                bos.write(buf)
            } while (inputStream.read(buf) != -1)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                inputStream?.close()
                bos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun readBytesFromFile(filePath: String): ByteArray? {
        var fileInputStream: FileInputStream? = null
        var bytesArray: ByteArray? = null

        try {
            val file = File(filePath)
            bytesArray = ByteArray(file.length().toInt())

            // read file into bytes[]
            fileInputStream = FileInputStream(file)
            fileInputStream.read(bytesArray)
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        return bytesArray
    }

//    @Throws(IOException::class)
    fun createTempImageFile(context: Context, fileName: String): File {
        // Create an image file name
        val storageDir = File(context.cacheDir, DOCUMENTS_DIR)
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    fun getFileName(context: Context, uri: Uri): String? {
        val mimeType = context.contentResolver.getType(uri)
        var filename: String? = null

        if (mimeType == null) {
            val path = getPathFromURI(context, uri)
            if (path == null) {
                filename = getName(uri.toString())
            } else {
                val file = File(path)
                filename = file.name
            }
        } else {
            val returnCursor = context.contentResolver.query(uri, null, null, null, null)
            if (returnCursor != null) {
                val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                returnCursor.moveToFirst()
                filename = returnCursor.getString(nameIndex)
                returnCursor.close()
            }
        }

        return filename
    }

    fun getName(filename: String?): String? {
        if (filename == null) {
            return null
        }
        val index = filename.lastIndexOf('/')
        return filename.substring(index + 1)
    }
}
