package baycon.android.common.utils.phonenumber

import android.telephony.PhoneNumberUtils
import com.google.i18n.phonenumbers.PhoneNumberUtil
import com.google.i18n.phonenumbers.Phonenumber

/**
 * gradle write in 2021.07.09
 * gradle with: implementation 'com.googlecode.libphonenumber:libphonenumber:8.2.0'
 */
class PhoneNumberUtilsCompat{
    companion object {

        @Suppress("DEPRECATION")
        fun formatPhoneNumber(phoneNumber: String, defaultCountryIso:String? ): String {

            var ret = phoneNumber
            try {

                var parsePhoneNumber = phoneNumber
                if( defaultCountryIso == "KR" || defaultCountryIso == "JP" ) {
                    if( !parsePhoneNumber.startsWith( "0") && parsePhoneNumber.length > 8 ) {
                        parsePhoneNumber = "0$parsePhoneNumber"
                    }
                }

                ret = if( defaultCountryIso == "KR") {
                    PhoneNumberUtils.formatNumber(parsePhoneNumber, parsePhoneNumber, defaultCountryIso )
                } else {
                    val numberProto: Phonenumber.PhoneNumber = PhoneNumberUtil.getInstance().parse(parsePhoneNumber, defaultCountryIso)
                    PhoneNumberUtil.getInstance().format(numberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL)
                }

                if( ret.length < phoneNumber.length )
                    ret = phoneNumber

            }catch( e:Exception ) {

            }

            return ret
        }

        fun formatInternalPhoneNumber(phoneNumber: String, defaultCountryIso:String ): String {
            var ret = phoneNumber
            try {
                val numberProto: Phonenumber.PhoneNumber = PhoneNumberUtil.getInstance().parse(phoneNumber, defaultCountryIso)
                ret = PhoneNumberUtil.getInstance().format(numberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL)
            }catch( e:Exception ) {
            }

            return ret
        }


        /**
         * 국가별 국제전화 프리픽스에 해당되는 번호는 해당 프리픽스를 제거후 리턴한다.
         * (Ex : +123456789 -> 123456789, 한국 00700123456789 -> 123456789, 미국 011123456789 -> 123456789
         * @param subCtr			통신가입국가( ISO 표기법을 대문자로 넘긴다 Ex: KR, US, JP )
         * @param phoneNumber		전화번호
         * @return 프리픽스에 해당되면 프리픽스가 제거된 전화번호리턴 아닐경우 입력된 전화번호와 동일한 전화번호 리턴
         */
        fun analysisPhoneNumber( subCtr:String, phoneNumber:String ): String {

            return when {
                phoneNumber.startsWith( "+" ) -> {
                    phoneNumber.substring( 1, phoneNumber.length )
                }
                phoneNumber.length < 6 -> {
                    phoneNumber
                }
                else -> {
                    when(subCtr) {
                        "KR" -> {
                            when {
                                phoneNumber.startsWith( "007" ) || phoneNumber.startsWith( "003" ) -> {
                                    phoneNumber.substring( 5, phoneNumber.length )
                                }
                                phoneNumber.startsWith( "001" ) || phoneNumber.startsWith( "002" ) || phoneNumber.startsWith( "005" ) ||
                                        phoneNumber.startsWith( "006" ) || phoneNumber.startsWith( "008" ) -> {
                                    phoneNumber.substring( 3, phoneNumber.length )
                                }
                                else -> {
                                    phoneNumber
                                }
                            }
                        }
                        "JP" -> {
                            when {
                                phoneNumber.startsWith( "0041" ) || phoneNumber.startsWith( "0061" ) || phoneNumber.startsWith( "0033" ) -> {
                                    phoneNumber.substring( 4, phoneNumber.length )
                                }
                                phoneNumber.startsWith( "001" ) -> {
                                    phoneNumber.substring( 3, phoneNumber.length )
                                }
                                else -> {
                                    phoneNumber
                                }
                            }
                        }
                        "US", "CA" -> {
                            if( phoneNumber.startsWith( "011" ) ) phoneNumber.substring( 3, phoneNumber.length ) else phoneNumber
                        }
                        else -> {
                            if( phoneNumber.startsWith("00" ) ) phoneNumber.substring( 2, phoneNumber.length ) else phoneNumber
                        }
                    }

                }
            }
        }

    }
}