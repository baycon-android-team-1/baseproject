package baycon.android.common.utils.country

import baycon.android.common.utils.string.StringUtil


class CountryItem {

    companion object {
        const val TYPE_TAG = 0
        const val TYPE_COUNTRY = 1
    }

    constructor( countryName:String, type: Int = TYPE_TAG ) {
        this.countryName = countryName
        this.type = type

    }

    constructor( uniqueId:String, iSOCountryCode:String, countryCode: String,
                 countryName:String, countrySortName:String, capitalName:String, gmt:String,
                 type: Int = TYPE_COUNTRY ) {

        this.uniqueId = uniqueId
        this.iSOCountryCode = iSOCountryCode
        this.countryCode = countryCode
        this.countryName = countryName
        this.countrySortName = countrySortName
        this.capitalName = capitalName
        this.gmt = gmt
        this.type = type
    }

    var type = TYPE_COUNTRY

    //    var type: Int = TYPE_TAG

    var imgRes = 0
    var chosung = ""
    //ex KOR
    var uniqueId = ""
    //ex KR
    var iSOCountryCode = ""
    //ex 82
    var countryCode = ""
    var countryName = ""

    var countrySortName = ""
        set(value) {
            field = value
            chosung = StringUtil.convertToChosung( value )
        }

    var capitalName = ""
    var gmt = ""



}