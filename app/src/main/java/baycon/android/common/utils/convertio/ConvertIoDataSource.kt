package baycon.android.common.utils.convertio

import baycon.android.common.api.RetrofitCreator
import baycon.android.common.api.contract.BaseDataSource
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

class ConvertIoDataSource: BaseDataSource() {

    suspend fun convertIoKeyGenerate(): ResultConvertIoKey? = getConvertResult {
        ConvertRetrofit.createConvertIo( IConvertIOService::class.java ).convertIodKeyGenerate( MakeConvertIoKeyGenerate())
    }

    suspend fun convertIoUploadStatus( id: String ): ResultConvertIoKey? = getConvertResult {
        ConvertRetrofit.createConvertIo( IConvertIOService::class.java ).convertIoUploadStatus( id )
    }

    suspend fun faxFileUpload( id: String, fileName: String, file: RequestBody ): ResultConvertIoKey? = getConvertResult {
        ConvertRetrofit.createConvertIo( IConvertIOService::class.java ).convertIoUpload( id, fileName, file )
    }

    suspend fun convertIoDownload( baseUrl: String, lastPath: String ): ResponseBody? = getConvertResult {
        ConvertRetrofit.createConvertIo( IConvertIOService::class.java ).convertIoDownload( lastPath )
    }

}

interface IConvertIOService {

    @POST( RetrofitCreator.BASE_URL )
    suspend fun convertIodKeyGenerate( @Body make:MakeConvertIoKeyGenerate ): Response<ResultConvertIoKey>

    @GET( "${RetrofitCreator.BASE_URL}/{id}/status" )
    suspend fun convertIoUploadStatus( @Path( "id" ) id: String ): Response<ResultConvertIoKey>

    @PUT( "${RetrofitCreator.BASE_URL}/{id}/{fileName}")
    suspend fun convertIoUpload( @Path( "id" ) id: String,
                                 @Path( "fileName" ) fileName: String,
                                 @Body file: RequestBody ) : Response<ResultConvertIoKey>

    @Streaming
    @GET( "{lastPath}" )
    suspend fun convertIoDownload( @Path( "lastPath" ) lastPath: String ): Response<ResponseBody>

    @Multipart
    @POST( RetrofitCreator.BASE_URL )
    suspend fun convertIoMultipart( @Query( "apikey" ) apikey: String,
                                    @Query( "input" ) input: String,
                                    @Query( "outputformat") outputformat: String,
                                    @Part file: MultipartBody.Part ): Response<ResultConvertIoKey>

}