package baycon.android.common.utils.binding.adapter

import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import androidx.core.view.isInvisible
import androidx.databinding.BindingAdapter
import androidx.core.view.isVisible
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.flow
import timber.log.Timber

typealias OnClickListener = (View) -> Unit

fun <T> debounce( delayMillis: Long = 300L, scope: CoroutineScope, action: (T) -> Unit ): (T) -> Unit {
    var debounceJob: Job? = null
    return { param: T ->
        if (debounceJob == null) {
            debounceJob = scope.launch {
                action(param)
                delay(delayMillis)
                debounceJob = null
            }
        }
    }
}


@BindingAdapter("android:visibleIf")
fun View.setVisibleIf( value: Boolean) {

    this.isVisible = value

}


@BindingAdapter("android:invisibleIf")
fun View.setInvisibleIf( value: Boolean) {

    this.isInvisible = value

}


@BindingAdapter("android:onClick")
fun Button.setDebounceListener( onClickListener: View.OnClickListener ) {

    val clickWithDebounce: OnClickListener =
        debounce( scope = MainScope() ) {

            onClickListener.onClick(it)

        }

    this.setOnClickListener( clickWithDebounce )

}


