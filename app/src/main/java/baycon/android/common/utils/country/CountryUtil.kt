package baycon.android.common.utils.country

import android.content.Context
import javax.inject.Inject
import com.softbay.android.baseproject.R

class CountryUtil @Inject constructor( private val context: Context) {

    val flagTable:HashMap<String, Int>
    val countryList:ArrayList<CountryItem>

    /**
     * KOR
     */
    val countryIdTable:HashMap<String, CountryItem>
    /**
     * KR
     */
    val countryISOTable:HashMap<String, CountryItem>
    /**
     * 82
     */
    val countryCodeTable:HashMap<String, CountryItem>


    init {
        flagTable = HashMap<String, Int>()
        countryList = ArrayList<CountryItem>()
        countryIdTable = HashMap<String, CountryItem>()
        countryISOTable = HashMap<String, CountryItem>()
        countryCodeTable = HashMap<String, CountryItem>()

        initCountryData()
        initFlag()
    }


    private fun initCountryData(){

        countryList.clear()
        countryIdTable.clear()
        countryISOTable.clear()
        countryCodeTable.clear()

        val tempList = ArrayList<CountryItem>()
        tempList.add(CountryItem("ABW", "AW", "297", context.getString(R.string.country_aruba), context.getString(R.string.country_aruba_sort), context.getString(R.string.capital_aruba), "GMT-04:00"))
        tempList.add(CountryItem("AFG", "AF", "93", context.getString(R.string.country_afghanistan), context.getString(R.string.country_afghanistan_sort), context.getString(R.string.capital_afghanistan), "GMT+04:30"))
        tempList.add(CountryItem("AGO", "AO", "244", context.getString(R.string.country_angola), context.getString(R.string.country_angola_sort), context.getString(R.string.capital_angola), "GMT+01:00"))
        tempList.add(CountryItem("AIA", "AI", "1264", context.getString(R.string.country_anguilla), context.getString(R.string.country_anguilla_sort), context.getString(R.string.capital_anguilla), "GMT-04:00"))
        tempList.add(CountryItem("ALA", "AX", "35818", context.getString(R.string.country_aland_islands), context.getString(R.string.country_aland_islands_sort), context.getString(R.string.capital_aland_islands), "GMT+02:00"))
        tempList.add(CountryItem("ALB", "AL", "355", context.getString(R.string.country_albania), context.getString(R.string.country_albania_sort), context.getString(R.string.capital_albania), "GMT+02:00"))
        tempList.add(CountryItem("AND", "AD", "376", context.getString(R.string.country_andorra), context.getString(R.string.country_andorra_sort), context.getString(R.string.capital_andorra), "GMT+02:00"))
        tempList.add(CountryItem("ARE", "AE", "971", context.getString(R.string.country_united_arab_emirates), context.getString(R.string.country_united_arab_emirates_sort), context.getString(R.string.capital_united_arab_emirates), "GMT+04:00"))
        tempList.add(CountryItem("ARG", "AR", "54", context.getString(R.string.country_argentina), context.getString(R.string.country_argentina_sort), context.getString(R.string.capital_argentina), "GMT-03:00"))
        tempList.add(CountryItem("ARM", "AM", "374", context.getString(R.string.country_armenia), context.getString(R.string.country_armenia_sort), context.getString(R.string.capital_armenia), "GMT+05:00"))
        tempList.add(CountryItem("ASC", "AC", "247", context.getString(R.string.country_ascension_island), context.getString(R.string.country_ascension_island_sort), context.getString(R.string.capital_ascension_island), "GMT"))
        tempList.add(CountryItem("ASM", "AS", "1684", context.getString(R.string.country_american_samoa), context.getString(R.string.country_american_samoa_sort), context.getString(R.string.capital_american_samoa), "GMT-11:00"))
        tempList.add(CountryItem("ATA", "AQ", "672", context.getString(R.string.country_antarctica), context.getString(R.string.country_antarctica_sort), context.getString(R.string.capital_antarctica), "GMT-04:00"))
        tempList.add(CountryItem("ATF", "TF", "262", context.getString(R.string.country_french_southern_and_antarctic_lands), context.getString(R.string.country_french_southern_and_antarctic_lands_sort), context.getString(R.string.capital_french_southern_and_antarctic_lands), "GMT+03:00"))
        tempList.add(CountryItem("ATG", "AG", "1268", context.getString(R.string.country_antigua_and_barbuda), context.getString(R.string.country_antigua_and_barbuda_sort), context.getString(R.string.capital_antigua_and_barbuda), "GMT-04:00"))
        tempList.add(CountryItem("AUS", "AU", "61", context.getString(R.string.country_australia), context.getString(R.string.country_australia_sort), context.getString(R.string.capital_australia), "GMT+10:00"))
        tempList.add(CountryItem("AUT", "AT", "43", context.getString(R.string.country_austria), context.getString(R.string.country_austria_sort), context.getString(R.string.capital_austria), "GMT+02:00"))
        tempList.add(CountryItem("AZE", "AZ", "994", context.getString(R.string.country_azerbaijan), context.getString(R.string.country_azerbaijan_sort), context.getString(R.string.capital_azerbaijan), "GMT+05:00"))
        tempList.add(CountryItem("BDI", "BI", "257", context.getString(R.string.country_burundi), context.getString(R.string.country_burundi_sort), context.getString(R.string.capital_burundi), "GMT+02:00"))
        tempList.add(CountryItem("BEL", "BE", "32", context.getString(R.string.country_belgium), context.getString(R.string.country_belgium_sort), context.getString(R.string.capital_belgium), "GMT+01:00"))
        tempList.add(CountryItem("BEN", "BJ", "229", context.getString(R.string.country_benin), context.getString(R.string.country_benin_sort), context.getString(R.string.capital_benin), "GMT+01:00"))
        tempList.add(CountryItem("BES", "BQ", "599", context.getString(R.string.country_caribbean_netherlands), context.getString(R.string.country_caribbean_netherlands_sort), context.getString(R.string.capital_caribbean_netherlands), "GMT+02:00"))
        tempList.add(CountryItem("BFA", "BF", "226", context.getString(R.string.country_burkina_faso), context.getString(R.string.country_burkina_faso_sort), context.getString(R.string.capital_burkina_faso), "GMT"))
        tempList.add(CountryItem("BGD", "BD", "880", context.getString(R.string.country_bangladesh), context.getString(R.string.country_bangladesh_sort), context.getString(R.string.capital_bangladesh), "GMT+06:00"))
        tempList.add(CountryItem("BGR", "BG", "359", context.getString(R.string.country_bulgaria), context.getString(R.string.country_bulgaria_sort), context.getString(R.string.capital_bulgaria), "GMT+03:00"))
        tempList.add(CountryItem("BHR", "BH", "973", context.getString(R.string.country_bahrain), context.getString(R.string.country_bahrain_sort), context.getString(R.string.capital_bahrain), "GMT+03:00"))
        tempList.add(CountryItem("BHS", "BS", "1242", context.getString(R.string.country_bahamas), context.getString(R.string.country_bahamas_sort), context.getString(R.string.capital_bahamas), "GMT-04:00"))
        tempList.add(CountryItem("BIH", "BA", "387", context.getString(R.string.country_bosnia_and_herzegovina), context.getString(R.string.country_bosnia_and_herzegovina_sort), context.getString(R.string.capital_bosnia_and_herzegovina), "GMT+02:00"))
        tempList.add(CountryItem("BLM", "BL", "590", context.getString(R.string.country_saint_barthelemy), context.getString(R.string.country_saint_barthelemy_sort), context.getString(R.string.capital_saint_barthelemy), "GMT-04:00"))
        tempList.add(CountryItem("BLR", "BY", "375", context.getString(R.string.country_belarus), context.getString(R.string.country_belarus_sort), context.getString(R.string.capital_belarus), "GMT+03:00"))
        tempList.add(CountryItem("BLZ", "BZ", "501", context.getString(R.string.country_belize), context.getString(R.string.country_belize_sort), context.getString(R.string.capital_belize), "GMT-06:00"))
        tempList.add(CountryItem("BMU", "BM", "1441", context.getString(R.string.country_bermuda), context.getString(R.string.country_bermuda_sort), context.getString(R.string.capital_bermuda), "GMT-03:00"))
        tempList.add(CountryItem("BOL", "BO", "591", context.getString(R.string.country_bolivia), context.getString(R.string.country_bolivia_sort), context.getString(R.string.capital_bolivia), "GMT-04:00"))
        tempList.add(CountryItem("BRA", "BR", "55", context.getString(R.string.country_brazil), context.getString(R.string.country_brazil_sort), context.getString(R.string.capital_brazil), "GMT-03:00"))
        tempList.add(CountryItem("BRB", "BB", "1246", context.getString(R.string.country_barbados), context.getString(R.string.country_barbados_sort), context.getString(R.string.capital_barbados), "GMT-04:00"))
        tempList.add(CountryItem("BRN", "BN", "673", context.getString(R.string.country_brunei), context.getString(R.string.country_brunei_sort), context.getString(R.string.capital_brunei), "GMT+08:00"))
        tempList.add(CountryItem("BTN", "BT", "975", context.getString(R.string.country_bhutan), context.getString(R.string.country_bhutan_sort), context.getString(R.string.capital_bhutan), "GMT+06:00"))
        tempList.add(CountryItem("BWA", "BW", "267", context.getString(R.string.country_botswana), context.getString(R.string.country_botswana_sort), context.getString(R.string.capital_botswana), "GMT+02:00"))
        tempList.add(CountryItem("CAF", "CF", "236", context.getString(R.string.country_central_african_republic), context.getString(R.string.country_central_african_republic_sort), context.getString(R.string.capital_central_african_republic), "GMT+01:00"))
        val canModel = CountryItem("CAN", "CA", "1", context.getString(R.string.country_canada), context.getString(R.string.country_canada_sort), context.getString(R.string.capital_canada), "GMT-05:00")
        tempList.add( canModel)
        tempList.add(CountryItem("CCK", "CC", "61891", context.getString(R.string.country_cocos_islands), context.getString(R.string.country_cocos_islands_sort), context.getString(R.string.capital_cocos_islands), "GMT+06:30"))
        tempList.add(CountryItem("CHE", "CH", "41", context.getString(R.string.country_switzertland), context.getString(R.string.country_switzertland_sort), context.getString(R.string.capital_switzertland), "GMT+01:00"))
        tempList.add(CountryItem("CHL", "CL", "56", context.getString(R.string.country_chile), context.getString(R.string.country_chile_sort), context.getString(R.string.capital_chile), "GMT-03:00"))
        tempList.add(CountryItem("CHN", "CN", "86", context.getString(R.string.country_china), context.getString(R.string.country_china_sort), context.getString(R.string.capital_china), "GMT+08:00"))
        tempList.add(CountryItem("CIV", "CI", "225", context.getString(R.string.country_cote_d_lvoire), context.getString(R.string.country_cote_d_lvoire_sort), context.getString(R.string.capital_cote_d_lvoire), "GMT"))
        tempList.add(CountryItem("CMR", "CM", "237", context.getString(R.string.country_cameroon), context.getString(R.string.country_cameroon_sort), context.getString(R.string.capital_cameroon), "GMT+01:00"))
        tempList.add(CountryItem("COD", "CD", "243", context.getString(R.string.country_democratic_republic_of_the_congo), context.getString(R.string.country_democratic_republic_of_the_congo_sort), context.getString(R.string.capital_democratic_republic_of_the_congo), "GMT+02:00"))
        tempList.add(CountryItem("COG", "CG", "242", context.getString(R.string.country_republic_of_the_congo), context.getString(R.string.country_republic_of_the_congo_sort), context.getString(R.string.capital_republic_of_the_congo), "GMT+01:00"))
        tempList.add(CountryItem("COK", "CK", "682", context.getString(R.string.country_cook_islands), context.getString(R.string.country_cook_islands_sort), context.getString(R.string.capital_cook_islands), "GMT-10:00"))
        tempList.add(CountryItem("COL", "CO", "57", context.getString(R.string.country_colombia), context.getString(R.string.country_colombia_sort), context.getString(R.string.capital_colombia), "GMT+19:00"))
        tempList.add(CountryItem("COM", "KM", "269", context.getString(R.string.country_comoros), context.getString(R.string.country_comoros_sort), context.getString(R.string.capital_comoros), "GMT+03:00"))
        tempList.add(CountryItem("CPV", "CV", "238", context.getString(R.string.country_cape_verde), context.getString(R.string.country_cape_verde_sort), context.getString(R.string.capital_cape_verde), "GMT-01:00"))
        tempList.add(CountryItem("CRI", "CR", "506", context.getString(R.string.country_costa_rica), context.getString(R.string.country_costa_rica_sort), context.getString(R.string.capital_costa_rica), "GMT-06:00"))
        tempList.add(CountryItem("CUB", "CU", "53", context.getString(R.string.country_cuba), context.getString(R.string.country_cuba_sort), context.getString(R.string.capital_cuba), "GMT-04:00"))
        tempList.add(CountryItem("CUW", "CW", "5999", context.getString(R.string.country_curacao), context.getString(R.string.country_curacao_sort), context.getString(R.string.capital_curacao), "GMT-04:00"))
        tempList.add(CountryItem("CXR", "CX", "61891", context.getString(R.string.country_christmas_island), context.getString(R.string.country_christmas_island_sort), context.getString(R.string.capital_christmas_island), "GMT+07:00"))
        tempList.add(CountryItem("CYM", "KY", "1345", context.getString(R.string.country_cayman_islands), context.getString(R.string.country_cayman_islands_sort), context.getString(R.string.capital_cayman_islands), "GMT-05:00"))
        tempList.add(CountryItem("CYP", "CY", "357", context.getString(R.string.country_cyprus), context.getString(R.string.country_cyprus_sort), context.getString(R.string.capital_cyprus), "GMT+02:00"))
        tempList.add(CountryItem("CZE", "CZ", "420", context.getString(R.string.country_czech_republic), context.getString(R.string.country_czech_republic_sort), context.getString(R.string.capital_czech_republic), "GMT+02:00"))
        tempList.add(CountryItem("DEU", "DE", "49", context.getString(R.string.country_germany), context.getString(R.string.country_germany_sort), context.getString(R.string.capital_germany), "GMT+01:00"))
        tempList.add(CountryItem("DGA", "DG", "246", context.getString(R.string.country_diego_garcia), context.getString(R.string.country_diego_garcia_sort), context.getString(R.string.capital_diego_garcia), "GMT-06:00"))
        tempList.add(CountryItem("DJI", "DJ", "253", context.getString(R.string.country_djibouti), context.getString(R.string.country_djibouti_sort), context.getString(R.string.capital_djibouti), "GMT+03:00"))
        tempList.add(CountryItem("DMA", "DM", "1767", context.getString(R.string.country_dominica), context.getString(R.string.country_dominica_sort), context.getString(R.string.capital_dominica), "GMT-04:00"))
        tempList.add(CountryItem("DNK", "DK", "45", context.getString(R.string.country_denmark), context.getString(R.string.country_denmark_sort), context.getString(R.string.capital_denmark), "GMT+01:00"))
        tempList.add(CountryItem("DOMA", "DO", "1809", context.getString(R.string.country_dominican_republic), context.getString(R.string.country_dominican_republic_sort), context.getString(R.string.capital_dominican_republic), "GMT-04:00"))
        tempList.add(CountryItem("DOMB", "DO", "1829", context.getString(R.string.country_dominican_republic), context.getString(R.string.country_dominican_republic_sort), context.getString(R.string.capital_dominican_republic), "GMT-04:00"))
        tempList.add(CountryItem("DOMC", "DO", "1849", context.getString(R.string.country_dominican_republic), context.getString(R.string.country_dominican_republic_sort), context.getString(R.string.capital_dominican_republic), "GMT-04:00"))
        tempList.add(CountryItem("DZA", "DZ", "213", context.getString(R.string.country_algeria), context.getString(R.string.country_algeria_sort), context.getString(R.string.capital_algeria), "GMT+01:00"))
        tempList.add(CountryItem("ECU", "EC", "593", context.getString(R.string.country_ecuador), context.getString(R.string.country_ecuador_sort), context.getString(R.string.capital_ecuador), "GMT-05:00"))
        tempList.add(CountryItem("EGY", "EG", "20", context.getString(R.string.country_egypt), context.getString(R.string.country_egypt_sort), context.getString(R.string.capital_egypt), "GMT+02:00"))
        tempList.add(CountryItem("ERI", "ER", "291", context.getString(R.string.country_eritrea), context.getString(R.string.country_eritrea_sort), context.getString(R.string.capital_eritrea), "GMT+03:00"))
        tempList.add(CountryItem("ESP", "ES", "34", context.getString(R.string.country_spain), context.getString(R.string.country_spain_sort), context.getString(R.string.capital_spain), "GMT+02:00"))
        tempList.add(CountryItem("EST", "EE", "372", context.getString(R.string.country_estonia), context.getString(R.string.country_estonia_sort), context.getString(R.string.capital_estonia), "GMT+03:00"))
        tempList.add(CountryItem("ETH", "ET", "251", context.getString(R.string.country_ethiopia), context.getString(R.string.country_ethiopia_sort), context.getString(R.string.capital_ethiopia), "GMT+03:00"))
        tempList.add(CountryItem("FIN", "FI", "358", context.getString(R.string.country_finland), context.getString(R.string.country_finland_sort), context.getString(R.string.capital_finland), "GMT+03:00"))
        tempList.add(CountryItem("FJI", "FJ", "679", context.getString(R.string.country_fiji), context.getString(R.string.country_fiji_sort), context.getString(R.string.capital_fiji), "GMT+12:00"))
        tempList.add(CountryItem("FLK", "FK", "500", context.getString(R.string.country_falkland_islands), context.getString(R.string.country_falkland_islands_sort), context.getString(R.string.capital_falkland_islands), "GMT-03:00"))
        tempList.add(CountryItem("FRA", "FR", "33", context.getString(R.string.country_france), context.getString(R.string.country_france_sort), context.getString(R.string.capital_france), "GMT+02:00"))
        tempList.add(CountryItem("FRO", "FO", "298", context.getString(R.string.country_faroe_islands), context.getString(R.string.country_faroe_islands_sort), context.getString(R.string.capital_faroe_islands), "GMT+01:00"))
        tempList.add(CountryItem("FSM", "FM", "691", context.getString(R.string.country_micronesia), context.getString(R.string.country_micronesia_sort), context.getString(R.string.capital_micronesia), "GMT"))
        tempList.add(CountryItem("GAB", "GA", "241", context.getString(R.string.country_gabon), context.getString(R.string.country_gabon_sort), context.getString(R.string.capital_gabon), "GMT+01:00"))
        tempList.add(CountryItem("GBR", "GB", "44", context.getString(R.string.country_united_kingdom), context.getString(R.string.country_united_kingdom_sort), context.getString(R.string.capital_united_kingdom), "GMT+01:00"))
        tempList.add(CountryItem("GEO", "GE", "995", context.getString(R.string.country_georgia), context.getString(R.string.country_georgia_sort), context.getString(R.string.capital_georgia), "GMT+04:00"))
        tempList.add(CountryItem("GGY", "GG", "44", context.getString(R.string.country_guernsey), context.getString(R.string.country_guernsey_sort), context.getString(R.string.capital_guernsey), "GMT+01:00"))
        tempList.add(CountryItem("GHA", "GH", "233", context.getString(R.string.country_ghana), context.getString(R.string.country_ghana_sort), context.getString(R.string.capital_ghana),"GMT"))
        tempList.add(CountryItem("GIB", "GI", "350", context.getString(R.string.country_gibraltar), context.getString(R.string.country_gibraltar_sort), context.getString(R.string.capital_gibraltar), "GMT+02:00"))
        tempList.add(CountryItem("GIN", "GN", "224", context.getString(R.string.country_guinea), context.getString(R.string.country_guinea_sort), context.getString(R.string.capital_guinea), "GMT"))
        tempList.add(CountryItem("GLP", "GP", "590", context.getString(R.string.country_guadeloupe), context.getString(R.string.country_guadeloupe_sort), context.getString(R.string.capital_guadeloupe), "GMT-04:00"))
        tempList.add(CountryItem("GMB", "GM", "220", context.getString(R.string.country_gambia), context.getString(R.string.country_gambia_sort), context.getString(R.string.capital_gambia), "GMT"))
        tempList.add(CountryItem("GNB", "GW", "245", context.getString(R.string.country_guinea_bissau), context.getString(R.string.country_guinea_bissau_sort), context.getString(R.string.capital_guinea_bissau), "GMT"))
        tempList.add(CountryItem("GNQ", "GQ", "240", context.getString(R.string.country_equatorial_guinea), context.getString(R.string.country_equatorial_guinea_sort), context.getString(R.string.capital_equatorial_guinea), "GMT+01:00"))
        tempList.add(CountryItem("GRC", "GR", "30", context.getString(R.string.country_greece), context.getString(R.string.country_greece_sort), context.getString(R.string.capital_greece), "GMT+02:00"))
        tempList.add(CountryItem("GRD", "GD", "1473", context.getString(R.string.country_grenada), context.getString(R.string.country_grenada_sort), context.getString(R.string.capital_grenada), "GMT-04:00"))
        tempList.add(CountryItem("GRL", "GL", "299", context.getString(R.string.country_greenland), context.getString(R.string.country_greenland_sort), context.getString(R.string.capital_greenland), "GMT-02:00"))
        tempList.add(CountryItem("GTM", "GT", "502", context.getString(R.string.country_guatemala), context.getString(R.string.country_guatemala_sort), context.getString(R.string.capital_guatemala), "GMT+06:00"))
        tempList.add(CountryItem("GUF", "GF", "594", context.getString(R.string.country_french_guiana), context.getString(R.string.country_french_guiana_sort), context.getString(R.string.capital_french_guiana), "GMT-03:00"))
        tempList.add(CountryItem("GUM", "GU", "1671", context.getString(R.string.country_usa_guam), context.getString(R.string.country_usa_guam_sort), context.getString(R.string.capital_usa_guam), "GMT+10:00"))
        tempList.add(CountryItem("GUY", "GY", "592", context.getString(R.string.country_guyana), context.getString(R.string.country_guyana_sort), context.getString(R.string.capital_guyana), "GMT-04:00"))
        tempList.add(CountryItem("HKG", "HK", "852", context.getString(R.string.country_hongkong), context.getString(R.string.country_hongkong_sort), context.getString(R.string.capital_hongkong), "GMT+07:00"))
        tempList.add(CountryItem("HND", "HN", "504", context.getString(R.string.country_honduras), context.getString(R.string.country_honduras_sort), context.getString(R.string.capital_honduras), "GMT-06:00"))
        tempList.add(CountryItem("HRV", "HR", "385", context.getString(R.string.country_croatia), context.getString(R.string.country_croatia_sort), context.getString(R.string.capital_croatia), "GMT+02:00"))
        tempList.add(CountryItem("HTI", "HT", "509", context.getString(R.string.country_haiti), context.getString(R.string.country_haiti_sort), context.getString(R.string.capital_haiti), "GMT-05:00"))
        tempList.add(CountryItem("HUN", "HU", "36", context.getString(R.string.country_hungary), context.getString(R.string.country_hungary_sort), context.getString(R.string.capital_hungary), "GMT+02:00"))
        tempList.add(CountryItem("IDN", "ID", "62", context.getString(R.string.country_indonesia), context.getString(R.string.country_indonesia_sort), context.getString(R.string.capital_indonesia), "GMT+07:00"))
        tempList.add(CountryItem("IMN", "IM", "44", context.getString(R.string.country_isle_of_man), context.getString(R.string.country_isle_of_man_sort), context.getString(R.string.capital_isle_of_man), "GMT"))
        tempList.add(CountryItem("IND", "IN", "91", context.getString(R.string.country_india), context.getString(R.string.country_india_sort), context.getString(R.string.capital_india), "GMT+05:30"))
        tempList.add(CountryItem("IOT", "IO", "246", context.getString(R.string.country_british_indian_ocean_territory), context.getString(R.string.country_british_indian_ocean_territory_sort), context.getString(R.string.capital_british_indian_ocean_territory), "GMT+06:00"))
        tempList.add(CountryItem("IRL", "IE", "353", context.getString(R.string.country_ireland), context.getString(R.string.country_ireland_sort), context.getString(R.string.capital_ireland), "GMT+01:00"))
        tempList.add(CountryItem("IRN", "IR", "98", context.getString(R.string.country_iran), context.getString(R.string.country_iran_sort), context.getString(R.string.capital_iran), "GMT+03:30"))
        tempList.add(CountryItem("IRQ", "IQ", "964", context.getString(R.string.country_iraq), context.getString(R.string.country_iraq_sort), context.getString(R.string.capital_iraq), "GMT+03:00"))
        tempList.add(CountryItem("ISL", "IS", "354", context.getString(R.string.country_iceland), context.getString(R.string.country_iceland_sort), context.getString(R.string.capital_iceland), "GMT"))
        tempList.add(CountryItem("ISR", "IL", "972", context.getString(R.string.country_israel), context.getString(R.string.country_israel_sort), context.getString(R.string.capital_israel), "GMT+03:00"))
        val isrpModel = CountryItem("ISRP", "IL", "972", context.getString(R.string.country_israel_palestinian), context.getString(R.string.country_israel_palestinian_sort), context.getString(R.string.capital_israel), "GMT+03:00")
        tempList.add(isrpModel)
        tempList.add(CountryItem("ITA", "IT", "39", context.getString(R.string.country_italy), context.getString(R.string.country_italy_sort), context.getString(R.string.capital_italy), "GMT+02:00"))
        tempList.add(CountryItem("JAM", "JM", "1876", context.getString(R.string.country_jamaica), context.getString(R.string.country_jamaica_sort), context.getString(R.string.capital_jamaica), "GMT+19:00"))
        tempList.add(CountryItem("JEY", "JE", "44", context.getString(R.string.country_bailiwick_of_jersey), context.getString(R.string.country_bailiwick_of_jersey_sort), context.getString(R.string.capital_bailiwick_of_jersey), "GMT"))
        tempList.add(CountryItem("JOR", "JO", "962", context.getString(R.string.country_jordan), context.getString(R.string.country_jordan_sort), context.getString(R.string.capital_jordan), "GMT+03:00"))
        tempList.add(CountryItem("JPN", "JP", "81", context.getString(R.string.country_japan), context.getString(R.string.country_japan_sort), context.getString(R.string.capital_japan), "GMT+09:00"))
        val kazModel = CountryItem("KAZ", "KZ", "7", context.getString(R.string.country_kazakhstan), context.getString(R.string.country_kazakhstan_sort), context.getString(R.string.capital_kazakhstan), "GMT+06:00")
        tempList.add(kazModel)
        tempList.add(CountryItem("KEN", "KE", "254", context.getString(R.string.country_kenya), context.getString(R.string.country_kenya_sort), context.getString(R.string.capital_kenya), "GMT+03:00"))
        tempList.add(CountryItem("KGZ", "KG", "996", context.getString(R.string.country_kyrgyzstan), context.getString(R.string.country_kyrgyzstan_sort), context.getString(R.string.capital_kyrgyzstan), "GMT+06:00"))
        tempList.add(CountryItem("KHM", "KH", "855", context.getString(R.string.country_cambodia), context.getString(R.string.country_cambodia_sort), context.getString(R.string.capital_cambodia), "GMT+07:00"))
        tempList.add(CountryItem("KIR", "KI", "686", context.getString(R.string.country_kiribati), context.getString(R.string.country_kiribati_sort), context.getString(R.string.capital_kiribati), "GMT+14:00"))
        tempList.add(CountryItem("KNA", "KN", "1869", context.getString(R.string.country_st_kitts_nevis), context.getString(R.string.country_st_kitts_nevis_sort), context.getString(R.string.capital_st_kitts_nevis), "GMT-04:00"))
        tempList.add(CountryItem("KOR", "KR", "82", context.getString(R.string.country_korea), context.getString(R.string.country_korea_sort), context.getString(R.string.capital_korea), "GMT+09:00"))
        tempList.add(CountryItem("KWT", "KW", "965", context.getString(R.string.country_kuwait), context.getString(R.string.country_kuwait_sort), context.getString(R.string.capital_kuwait), "GMT+03:00"))
        tempList.add(CountryItem("LAO", "LA", "856", context.getString(R.string.country_laos), context.getString(R.string.country_laos_sort), context.getString(R.string.capital_laos), "GMT+07:00"))
        tempList.add(CountryItem("LBN", "LB", "961", context.getString(R.string.country_lebanon), context.getString(R.string.country_lebanon_sort), context.getString(R.string.capital_lebanon), "GMT+03:00"))
        tempList.add(CountryItem("LBR", "LR", "231", context.getString(R.string.country_liberia), context.getString(R.string.country_liberia_sort), context.getString(R.string.capital_liberia), "GMT"))
        tempList.add(CountryItem("LBY", "LY", "218", context.getString(R.string.country_libya), context.getString(R.string.country_libya_sort), context.getString(R.string.capital_libya), "GMT+02:00"))
        tempList.add(CountryItem("LCA", "LC", "1758", context.getString(R.string.country_saint_lucia), context.getString(R.string.country_saint_lucia_sort), context.getString(R.string.capital_saint_lucia), "GMT-04:00"))
        tempList.add(CountryItem("LIE", "LI", "423", context.getString(R.string.country_liechtenein), context.getString(R.string.country_liechtenein_sort), context.getString(R.string.capital_liechtenein), "GMT+02:00"))
        tempList.add(CountryItem("LKA", "LK", "94", context.getString(R.string.country_sri_lanka), context.getString(R.string.country_sri_lanka_sort), context.getString(R.string.capital_sri_lanka), "GMT+05:30"))
        tempList.add(CountryItem("LSO", "LS", "266", context.getString(R.string.country_lesotho), context.getString(R.string.country_lesotho_sort), context.getString(R.string.capital_lesotho), "GMT+02:00"))
        tempList.add(CountryItem("LTU", "LT", "370", context.getString(R.string.country_lithuania), context.getString(R.string.country_lithuania_sort), context.getString(R.string.capital_lithuania), "GMT+02:00"))
        tempList.add(CountryItem("LUX", "LU", "352", context.getString(R.string.country_luxembourg), context.getString(R.string.country_luxembourg_sort), context.getString(R.string.capital_luxembourg), "GMT+01:00"))
        tempList.add(CountryItem("LVA", "LV", "371", context.getString(R.string.country_latvia), context.getString(R.string.country_latvia_sort), context.getString(R.string.capital_latvia), "GMT+03:00"))
        tempList.add(CountryItem("MAC", "MO", "853", context.getString(R.string.country_macau), context.getString(R.string.country_macau_sort), context.getString(R.string.capital_macau), "GMT+08:00"))
        tempList.add(CountryItem("MAF", "MF", "590", context.getString(R.string.country_collectivity_of_saint_martin), context.getString(R.string.country_collectivity_of_saint_martin_sort), context.getString(R.string.capital_collectivity_of_saint_martin), "GMT-04:00"))
        tempList.add(CountryItem("MAR", "MA", "212", context.getString(R.string.country_morocco), context.getString(R.string.country_morocco_sort), context.getString(R.string.capital_morocco), "GMT"))
        tempList.add(CountryItem("MCO", "MC", "377", context.getString(R.string.country_monaco), context.getString(R.string.country_monaco_sort), context.getString(R.string.capital_monaco), "GMT+02:00"))
        tempList.add(CountryItem("MDA", "MD", "373", context.getString(R.string.country_moldova), context.getString(R.string.country_moldova_sort), context.getString(R.string.capital_moldova), "GMT+03:00"))
        tempList.add(CountryItem("MDG", "MG", "261", context.getString(R.string.country_madagascar), context.getString(R.string.country_madagascar_sort), context.getString(R.string.capital_madagascar), "GMT+03:00"))
        tempList.add(CountryItem("MDV", "MV", "960", context.getString(R.string.country_maldives), context.getString(R.string.country_maldives_sort), context.getString(R.string.capital_maldives), "GMT+05:00"))
        tempList.add(CountryItem("MEX", "MX", "52", context.getString(R.string.country_mexico), context.getString(R.string.country_mexico_sort), context.getString(R.string.capital_mexico), "GMT-07:00"))
        tempList.add(CountryItem("MHL", "MH", "692", context.getString(R.string.country_marshall_islands), context.getString(R.string.country_marshall_islands_sort), context.getString(R.string.capital_marshall_islands), "GMT+12:00"))
        tempList.add(CountryItem("MKD", "MK", "389", context.getString(R.string.country_macedonia), context.getString(R.string.country_macedonia_sort), context.getString(R.string.capital_macedonia), "GMT+02:00"))
        tempList.add(CountryItem("MLI", "ML", "223", context.getString(R.string.country_mali), context.getString(R.string.country_mali_sort), context.getString(R.string.capital_mali), "GMT"))
        tempList.add(CountryItem("MLT", "MT", "356", context.getString(R.string.country_malta), context.getString(R.string.country_malta_sort), context.getString(R.string.capital_malta), "GMT+01:00"))
        tempList.add(CountryItem("MMR", "MM", "95", context.getString(R.string.country_myanmar), context.getString(R.string.country_myanmar_sort), context.getString(R.string.capital_myanmar), "GMT+06:30"))
        tempList.add(CountryItem("MNE", "ME", "382", context.getString(R.string.country_montenegro), context.getString(R.string.country_montenegro_sort), context.getString(R.string.capital_montenegro), "GMT+02:00"))
        tempList.add(CountryItem("MNG", "MN", "976", context.getString(R.string.country_mongolia), context.getString(R.string.country_mongolia_sort), context.getString(R.string.capital_mongolia), "GMT+08:00"))
        tempList.add(CountryItem("MNP", "MP", "1670", context.getString(R.string.country_saipan), context.getString(R.string.country_saipan_sort), context.getString(R.string.capital_saipan), "GMT+10:00"))
        tempList.add(CountryItem("MOZ", "MZ", "258", context.getString(R.string.country_mozambique), context.getString(R.string.country_mozambique_sort), context.getString(R.string.capital_mozambique), "GMT+02:00"))
        tempList.add(CountryItem("MRT", "MR", "222", context.getString(R.string.country_mauritania), context.getString(R.string.country_mauritania_sort), context.getString(R.string.capital_mauritania), "GMT"))
        tempList.add(CountryItem("MSR", "MS", "1664", context.getString(R.string.country_montserrat), context.getString(R.string.country_montserrat_sort), context.getString(R.string.capital_montserrat), "GMT-04:00"))
        tempList.add(CountryItem("MTQ", "MQ", "596", context.getString(R.string.country_fench_martinique), context.getString(R.string.country_fench_martinique_sort), context.getString(R.string.capital_fench_martinique), "GMT-04:00"))
        tempList.add(CountryItem("MUS", "MU", "230", context.getString(R.string.country_mauritius), context.getString(R.string.country_mauritius_sort), context.getString(R.string.capital_mauritius), "GMT+04:00"))
        tempList.add(CountryItem("MWI", "MW", "265", context.getString(R.string.country_malawi), context.getString(R.string.country_malawi_sort), context.getString(R.string.capital_malawi), "GMT+02:00"))
        tempList.add(CountryItem("MYS", "MY", "60", context.getString(R.string.country_malaysia), context.getString(R.string.country_malaysia_sort), context.getString(R.string.capital_malaysia), "GMT+08:00"))
        tempList.add(CountryItem("MYT", "YT", "262", context.getString(R.string.country_mayotte), context.getString(R.string.country_mayotte_sort), context.getString(R.string.capital_mayotte), "GMT+03:00"))
        tempList.add(CountryItem("NAM", "NA", "264", context.getString(R.string.country_namibia), context.getString(R.string.country_namibia_sort), context.getString(R.string.capital_namibia), "GMT+02:00"))
        tempList.add(CountryItem("NCL", "NC", "687", context.getString(R.string.country_new_caledonia), context.getString(R.string.country_new_caledonia_sort), context.getString(R.string.capital_new_caledonia), "GMT+11:00"))
        tempList.add(CountryItem("NER", "NE", "227", context.getString(R.string.country_niger), context.getString(R.string.country_niger_sort), context.getString(R.string.capital_niger), "GMT+01:00"))
        tempList.add(CountryItem("NFK", "NF", "672", context.getString(R.string.country_norfolk_island), context.getString(R.string.country_norfolk_island_sort), context.getString(R.string.capital_norfolk_island), "GMT+11:30"))
        tempList.add(CountryItem("NGA", "NG", "234", context.getString(R.string.country_nigeria), context.getString(R.string.country_nigeria_sort), context.getString(R.string.capital_nigeria), "GMT+01:00"))
        tempList.add(CountryItem("NIC", "NI", "505", context.getString(R.string.country_nicaragua), context.getString(R.string.country_nicaragua_sort), context.getString(R.string.capital_nicaragua), "GMT-06:00"))
        tempList.add(CountryItem("NIU", "NU", "683", context.getString(R.string.country_niue), context.getString(R.string.country_niue_sort), context.getString(R.string.capital_niue), "GMT-11:00"))
        tempList.add(CountryItem("NLD", "NL", "31", context.getString(R.string.country_netherlands), context.getString(R.string.country_netherlands_sort), context.getString(R.string.capital_netherlands), "GMT+01:00"))
        tempList.add(CountryItem("NOR", "NO", "47", context.getString(R.string.country_norway), context.getString(R.string.country_norway_sort), context.getString(R.string.capital_norway), "GMT+02:00"))
        tempList.add(CountryItem("NPL", "NP", "977", context.getString(R.string.country_nepal), context.getString(R.string.country_nepal_sort), context.getString(R.string.capital_nepal), "GMT+05:45"))
        tempList.add(CountryItem("NRU", "NR", "674", context.getString(R.string.country_nauru), context.getString(R.string.country_nauru_sort), context.getString(R.string.capital_nauru), "GMT+12:00"))
        tempList.add(CountryItem("NZL", "NZ", "64", context.getString(R.string.country_new_zealand), context.getString(R.string.country_new_zealand_sort), context.getString(R.string.capital_new_zealand), "GMT+12:00"))
        tempList.add(CountryItem("OMN", "OM", "968", context.getString(R.string.country_oman), context.getString(R.string.country_oman_sort), context.getString(R.string.capital_oman), "GMT+04:00"))
        tempList.add(CountryItem("PAK", "PK", "92", context.getString(R.string.country_pakistan), context.getString(R.string.country_pakistan_sort), context.getString(R.string.capital_pakistan), "GMT+05:00"))
        tempList.add(CountryItem("PAN", "PA", "507", context.getString(R.string.country_panama), context.getString(R.string.country_panama_sort), context.getString(R.string.capital_panama), "GMT-05:00"))
        tempList.add(CountryItem("PER", "PE", "51", context.getString(R.string.country_peru), context.getString(R.string.country_peru_sort), context.getString(R.string.capital_peru), "GMT-05:00"))
        tempList.add(CountryItem("PHL", "PH", "63", context.getString(R.string.country_philippines), context.getString(R.string.country_philippines_sort), context.getString(R.string.capital_philippines), "GMT+08:00"))
        tempList.add(CountryItem("PLW", "PW", "680", context.getString(R.string.country_palau), context.getString(R.string.country_palau_sort), context.getString(R.string.capital_palau), "GMT+09:00"))
        tempList.add(CountryItem("PNG", "PG", "675", context.getString(R.string.country_papua_new_guinea), context.getString(R.string.country_papua_new_guinea_sort), context.getString(R.string.capital_papua_new_guinea), "GMT+10:00"))
        tempList.add(CountryItem("POL", "PL", "48", context.getString(R.string.country_poland), context.getString(R.string.country_poland_sort), context.getString(R.string.capital_poland), "GMT+02:00"))
        tempList.add(CountryItem("PRIA", "PR", "1787", context.getString(R.string.country_puerto_rico), context.getString(R.string.country_puerto_rico_sort), context.getString(R.string.capital_puerto_rico), "GMT-04:00"))
        tempList.add(CountryItem("PRIB", "PR", "1939", context.getString(R.string.country_puerto_rico), context.getString(R.string.country_puerto_rico_sort), context.getString(R.string.capital_puerto_rico), "GMT-04:00"))
        tempList.add(CountryItem("PRT", "PT", "351", context.getString(R.string.country_portugal), context.getString(R.string.country_portugal_sort), context.getString(R.string.capital_portugal), "GMT+01:00"))
        tempList.add(CountryItem("PRY", "PY", "595", context.getString(R.string.country_paraguay), context.getString(R.string.country_paraguay_sort), context.getString(R.string.capital_paraguay), "GMT-04:00"))
        tempList.add(CountryItem("PSE", "PS", "970", context.getString(R.string.country_palestinian_territory), context.getString(R.string.country_palestinian_territory_sort), context.getString(R.string.capital_palestinian_territory), "GMT+02:00"))
        tempList.add(CountryItem("PYF", "PF", "689", context.getString(R.string.country_french_polynesia), context.getString(R.string.country_french_polynesia_sort), context.getString(R.string.capital_french_polynesia), "GMT-10:00"))
        tempList.add(CountryItem("QAT", "QA", "974", context.getString(R.string.country_qatar), context.getString(R.string.country_qatar_sort), context.getString(R.string.capital_qatar), "GMT+03:00"))
        tempList.add(CountryItem("REU", "RE", "262", context.getString(R.string.country_reunion), context.getString(R.string.country_reunion_sort), context.getString(R.string.capital_reunion), "GMT+04:00"))
        tempList.add(CountryItem("ROU", "RO", "40", context.getString(R.string.country_romania), context.getString(R.string.country_romania_sort), context.getString(R.string.capital_romania), "GMT+02:00"))
        tempList.add(CountryItem("RUS", "RU", "7", context.getString(R.string.country_russia), context.getString(R.string.country_russia_sort), context.getString(R.string.capital_russia), "GMT+07:00"))
        tempList.add(CountryItem("RWA", "RW", "250", context.getString(R.string.country_rwanda), context.getString(R.string.country_rwanda_sort), context.getString(R.string.capital_rwanda), "GMT+02:00"))
        tempList.add(CountryItem("SAU", "SA", "966", context.getString(R.string.country_saudi_arabia), context.getString(R.string.country_saudi_arabia_sort), context.getString(R.string.capital_saudi_arabia), "GMT+03:00"))
        tempList.add(CountryItem("SDN", "SD", "249", context.getString(R.string.country_sudan), context.getString(R.string.country_sudan_sort), context.getString(R.string.capital_sudan), "GMT+03:00"))
        tempList.add(CountryItem("SEN", "SN", "221", context.getString(R.string.country_senegal), context.getString(R.string.country_senegal_sort), context.getString(R.string.capital_senegal), "GMT"))
        tempList.add(CountryItem("SGP", "SG", "65", context.getString(R.string.country_singapore), context.getString(R.string.country_singapore_sort), context.getString(R.string.capital_singapore), "GMT+08:00"))
        tempList.add(CountryItem("SHN", "SH", "290", context.getString(R.string.country_saint_helena), context.getString(R.string.country_saint_helena_sort), context.getString(R.string.capital_saint_helena), "GMT"))
        tempList.add(CountryItem("SLB", "SB", "677", context.getString(R.string.country_solomon_islands), context.getString(R.string.country_solomon_islands_sort), context.getString(R.string.capital_solomon_islands), "GMT+11:00"))
        tempList.add(CountryItem("SLE", "SL", "232", context.getString(R.string.country_sierra_leone), context.getString(R.string.country_sierra_leone_sort), context.getString(R.string.capital_sierra_leone), "GMT"))
        tempList.add(CountryItem("SLV", "SV", "503", context.getString(R.string.country_el_salvador), context.getString(R.string.country_el_salvador_sort), context.getString(R.string.capital_el_salvador), "GMT-06:00"))
        tempList.add(CountryItem("SMR", "SM", "378", context.getString(R.string.country_san_marino), context.getString(R.string.country_san_marino_sort), context.getString(R.string.capital_san_marino), "GMT+02:00"))
        tempList.add(CountryItem("SOM", "SO", "252", context.getString(R.string.country_somalia), context.getString(R.string.country_somalia_sort), context.getString(R.string.capital_somalia), "GMT+03:00"))
        tempList.add(CountryItem("SPM", "PM", "508", context.getString(R.string.country_st_pierre_et_miquelon), context.getString(R.string.country_st_pierre_et_miquelon_sort), context.getString(R.string.capital_st_pierre_et_miquelon), "GMT-03:00"))
        tempList.add(CountryItem("SRB", "RS", "381", context.getString(R.string.country_serbia), context.getString(R.string.country_serbia_sort), context.getString(R.string.capital_serbia), "GMT+02:00"))
//        tempList.add(CountryItem("SSD", "SS", "211", context.getString(R.string.country_south_sudan), context.getString(R.string.capital_south_sudan), "GMT+03:00"))
        tempList.add(CountryItem("STP", "ST", "239", context.getString(R.string.country_sao_tome_and_principe), context.getString(R.string.country_sao_tome_and_principe_sort), context.getString(R.string.capital_sao_tome_and_principe), "GMT"))
        tempList.add(CountryItem("SUR", "SR", "597", context.getString(R.string.country_suriname), context.getString(R.string.country_suriname_sort), context.getString(R.string.capital_suriname), "GMT-03:00"))
        tempList.add(CountryItem("SVK", "SK", "421", context.getString(R.string.country_slovakia), context.getString(R.string.country_slovakia_sort), context.getString(R.string.capital_slovakia), "GMT+02:00"))
        tempList.add(CountryItem("SVN", "SI", "386", context.getString(R.string.country_slovenia), context.getString(R.string.country_slovenia_sort), context.getString(R.string.capital_slovenia), "GMT+02:00"))
        tempList.add(CountryItem("SWE", "SE", "46", context.getString(R.string.country_sweden), context.getString(R.string.country_sweden_sort), context.getString(R.string.capital_sweden), "GMT+01:00"))
        tempList.add(CountryItem("SWZ", "SZ", "268", context.getString(R.string.country_swaziland), context.getString(R.string.country_swaziland_sort), context.getString(R.string.capital_swaziland), "GMT+02:00"))
        tempList.add(CountryItem("SXM", "SX", "1721", context.getString(R.string.country_sint_maarten), context.getString(R.string.country_sint_maarten_sort), context.getString(R.string.capital_sint_maarten), "GMT"))
        tempList.add(CountryItem("SYC", "SC", "248", context.getString(R.string.country_seychelles), context.getString(R.string.country_seychelles_sort), context.getString(R.string.capital_seychelles), "GMT+04:00"))
        tempList.add(CountryItem("SYR", "SY", "963", context.getString(R.string.country_syria), context.getString(R.string.country_syria_sort), context.getString(R.string.capital_syria), "GMT+03:00"))
        tempList.add(CountryItem("TAA", "TA", "290", context.getString(R.string.country_tristan_da_cunha), context.getString(R.string.country_tristan_da_cunha_sort), context.getString(R.string.capital_tristan_da_cunha), "GMT"))
        tempList.add(CountryItem("TCA", "TC", "1649", context.getString(R.string.country_turks_and_caicos_islands), context.getString(R.string.country_turks_and_caicos_islands_sort), context.getString(R.string.capital_turks_and_caicos_islands), "GMT-04:00"))
        tempList.add(CountryItem("TCD", "TD", "235", context.getString(R.string.country_chad), context.getString(R.string.country_chad_sort), context.getString(R.string.capital_chad), "GMT+01:00"))
        tempList.add(CountryItem("TGO", "TG", "228", context.getString(R.string.country_togo), context.getString(R.string.country_togo_sort), context.getString(R.string.capital_togo), "GMT"))
        tempList.add(CountryItem("THA", "TH", "66", context.getString(R.string.country_thailand), context.getString(R.string.country_thailand_sort), context.getString(R.string.capital_thailand), "GMT+07:00"))
        tempList.add(CountryItem("TJK", "TJ", "992", context.getString(R.string.country_tajikistan), context.getString(R.string.country_tajikistan_sort), context.getString(R.string.capital_tajikistan), "GMT+05:00"))
        tempList.add(CountryItem("TKL", "TK", "690", context.getString(R.string.country_tokelau), context.getString(R.string.country_tokelau_sort), context.getString(R.string.capital_tokelau), "GMT-10:00"))
        tempList.add(CountryItem("TKM", "TM", "993", context.getString(R.string.country_turkmenistan), context.getString(R.string.country_turkmenistan_sort), context.getString(R.string.capital_turkmenistan), "GMT+05:00"))
        tempList.add(CountryItem("TLS", "TL", "670", context.getString(R.string.country_east_timor), context.getString(R.string.country_east_timor_sort), context.getString(R.string.capital_east_timor), "GMT+09:00"))
        tempList.add(CountryItem("TON", "TO", "676", context.getString(R.string.country_tonga), context.getString(R.string.country_tonga_sort), context.getString(R.string.capital_tonga), "GMT+13:00"))
        tempList.add(CountryItem("TTO", "TT", "1868", context.getString(R.string.country_trinidad_and_tobago), context.getString(R.string.country_trinidad_and_tobago_sort), context.getString(R.string.capital_trinidad_and_tobago), "GMT-04:00"))
        tempList.add(CountryItem("TUN", "TN", "216", context.getString(R.string.country_tunisia), context.getString(R.string.country_tunisia_sort), context.getString(R.string.capital_tunisia), "GMT+01:00"))
        tempList.add(CountryItem("TUR", "TR", "90", context.getString(R.string.country_turkey), context.getString(R.string.country_turkey_sort), context.getString(R.string.capital_turkey), "GMT+03:00"))
        tempList.add(CountryItem("TUV", "TV", "688", context.getString(R.string.country_tuvalu), context.getString(R.string.country_tuvalu_sort), context.getString(R.string.capital_tuvalu), "GMT"))
        tempList.add(CountryItem("TWN", "TW", "886", context.getString(R.string.country_taiwan), context.getString(R.string.country_taiwan_sort), context.getString(R.string.capital_taiwan), "GMT+08:00"))
        tempList.add(CountryItem("TZA", "TZ", "255", context.getString(R.string.country_tanzania), context.getString(R.string.country_tanzania_sort), context.getString(R.string.capital_tanzania), "GMT+03:00"))
        tempList.add(CountryItem("UGA", "UG", "256", context.getString(R.string.country_uganda), context.getString(R.string.country_uganda_sort), context.getString(R.string.capital_uganda), "GMT+03:00"))
        tempList.add(CountryItem("UKR", "UA", "380", context.getString(R.string.country_ukraine), context.getString(R.string.country_ukraine_sort), context.getString(R.string.capital_ukraine), "GMT+03:00"))
        tempList.add(CountryItem("URY", "UY", "598", context.getString(R.string.country_uruguay), context.getString(R.string.country_uruguay_sort), context.getString(R.string.capital_uruguay), "GMT-03:00"))
        tempList.add(CountryItem("USA", "US", "1", context.getString(R.string.country_usa), context.getString(R.string.country_usa_sort), context.getString(R.string.capital_usa), "GMT-05:00"))
        tempList.add(CountryItem("USAA", "US", "1907", context.getString(R.string.country_usa_alaska), context.getString(R.string.country_usa_alaska_sort), context.getString(R.string.capital_usa_alaska), "GMT-08:00"))
        tempList.add(CountryItem("USAH", "US", "1808", context.getString(R.string.country_usa_hawaii), context.getString(R.string.country_usa_hawaii_sort), context.getString(R.string.capital_usa_hawaii), "GMT-08:00"))
        tempList.add(CountryItem("UZB", "UZ", "998", context.getString(R.string.country_uzbekistan), context.getString(R.string.country_uzbekistan_sort), context.getString(R.string.capital_uzbekistan), "GMT+05:00"))
        tempList.add(CountryItem("VAT", "VA", "379", context.getString(R.string.country_vatican_city), context.getString(R.string.country_vatican_city_sort), context.getString(R.string.capital_vatican_city), "GMT+01:00"))
        tempList.add(CountryItem("VCT", "VC", "1784", context.getString(R.string.country_st_vincent_and_the_grenadines), context.getString(R.string.country_st_vincent_and_the_grenadines_sort), context.getString(R.string.capital_st_vincent_and_the_grenadines), "GMT-04:00"))
        tempList.add(CountryItem("VEN", "VE", "58", context.getString(R.string.country_venezuela), context.getString(R.string.country_venezuela_sort), context.getString(R.string.capital_venezuela), "GMT-04:30"))
        tempList.add(CountryItem("VGB", "VG", "1284", context.getString(R.string.country_british_virgin_islands), context.getString(R.string.country_british_virgin_islands_sort), context.getString(R.string.capital_british_virgin_islands), "GMT-04:00"))
        tempList.add(CountryItem("VIR", "VI", "1340", context.getString(R.string.country_us_virgin_islands), context.getString(R.string.country_us_virgin_islands_sort), context.getString(R.string.capital_us_virgin_islands), "GMT-04:00"))
        tempList.add(CountryItem("VNM", "VN", "84", context.getString(R.string.country_vietnam), context.getString(R.string.country_vietnam_sort), context.getString(R.string.capital_vietnam), "GMT+07:00"))
        tempList.add(CountryItem("VUT", "VU", "678", context.getString(R.string.country_vanuatu), context.getString(R.string.country_vanuatu_sort), context.getString(R.string.capital_vanuatu), "GMT+11:00"))
        tempList.add(CountryItem("WLF", "WF", "681", context.getString(R.string.country_wallis_and_futuna_islands), context.getString(R.string.country_wallis_and_futuna_islands_sort), context.getString(R.string.capital_wallis_and_futuna_islands), "GMT+12:00"))
        tempList.add(CountryItem("WSM", "WS", "685", context.getString(R.string.country_western_samoa), context.getString(R.string.country_western_samoa_sort), context.getString(R.string.capital_western_samoa), "GMT-11:00"))
        tempList.add(CountryItem("YEM", "YE", "967", context.getString(R.string.country_yemen), context.getString(R.string.country_yemen_sort), context.getString(R.string.capital_yemen), "GMT+03:00"))
        tempList.add(CountryItem("ZAF", "ZA", "27", context.getString(R.string.country_south_africa), context.getString(R.string.country_south_africa_sort), context.getString(R.string.capital_south_africa), "GMT+02:00"))
        tempList.add(CountryItem("ZMB", "ZM", "260", context.getString(R.string.country_zambia), context.getString(R.string.country_zambia_sort), context.getString(R.string.capital_zambia), "GMT+02:00"))
        tempList.add(CountryItem("ZWE", "ZW", "263", context.getString(R.string.country_zimbabwe), context.getString(R.string.country_zimbabwe_sort), context.getString(R.string.capital_zimbabwe), "GMT+02:00"))

        tempList.sortBy { it.countrySortName }

//        var tag = ""
        for( tempItem in tempList ) {
//            var currentTag = tempItem.chosung.substring( 0, 1 )

//            if( tag.isNullOrBlank() || tag != currentTag ) {
//                tag = currentTag
//
//                countryList.add( CountryItem( currentTag ) )
//            }

            countryList.add( tempItem )

            countryIdTable[ tempItem.uniqueId ] = tempItem

            if( tempItem.uniqueId != "USAA" && tempItem.uniqueId != "USAH")
                countryISOTable[ tempItem.iSOCountryCode ] = tempItem

            if( tempItem.uniqueId != "CAN" && tempItem.uniqueId != "KAZ" && tempItem.uniqueId != "JEY" && tempItem.uniqueId != "IMN" &&
                tempItem.uniqueId != "GGY" && tempItem.uniqueId != "MAF" && tempItem.uniqueId != "BLM" && tempItem.uniqueId != "BESS" &&
                tempItem.uniqueId != "BESE" && tempItem.uniqueId != "TAA" && tempItem.uniqueId != "ATA" && tempItem.uniqueId != "CXR" &&
                tempItem.uniqueId != "ESH" && tempItem.uniqueId != "ISRP" ) {
                countryCodeTable[ tempItem.countryCode ] = tempItem

            }
        }

        countryCodeTable["1204"] = canModel
        countryCodeTable["1226"] = canModel
        countryCodeTable["1236"] = canModel
        countryCodeTable["1249"] = canModel
        countryCodeTable["1250"] = canModel
        countryCodeTable["1289"] = canModel
        countryCodeTable["1306"] = canModel
        countryCodeTable["1343"] = canModel
        countryCodeTable["1365"] = canModel
        countryCodeTable["1403"] = canModel
        countryCodeTable["1416"] = canModel
        countryCodeTable["1418"] = canModel
        countryCodeTable["1431"] = canModel
        countryCodeTable["1437"] = canModel
        countryCodeTable["1438"] = canModel
        countryCodeTable["1450"] = canModel
        countryCodeTable["1506"] = canModel
        countryCodeTable["1514"] = canModel
        countryCodeTable["1519"] = canModel
        countryCodeTable["1579"] = canModel
        countryCodeTable["1581"] = canModel
        countryCodeTable["1587"] = canModel
        countryCodeTable["1604"] = canModel
        countryCodeTable["1613"] = canModel
        countryCodeTable["1639"] = canModel
        countryCodeTable["1647"] = canModel
        countryCodeTable["1705"] = canModel
        countryCodeTable["1709"] = canModel
        countryCodeTable["1778"] = canModel
        countryCodeTable["1780"] = canModel
        countryCodeTable["1782"] = canModel
        countryCodeTable["1807"] = canModel
        countryCodeTable["1819"] = canModel
        countryCodeTable["1867"] = canModel
        countryCodeTable["1873"] = canModel
        countryCodeTable["1902"] = canModel
        countryCodeTable["1905"] = canModel

        countryCodeTable["77"] = kazModel

        countryCodeTable["97222"] = isrpModel
        countryCodeTable["97242"] = isrpModel
        countryCodeTable["97282"] = isrpModel
        countryCodeTable["97292"] = isrpModel
        countryCodeTable["97259"] = isrpModel
        countryCodeTable["97256"] = isrpModel
    }

    private fun initFlag(){
        flagTable.clear()

        flagTable["GHA"] = R.drawable.flag_ghana
        flagTable["GAB"] = R.drawable.flag_gabon
        flagTable["GUY"] = R.drawable.flag_guyana
        flagTable["GMB"] = R.drawable.flag_gambia
        flagTable["GLP"] = R.drawable.flag_france_guadeloupe
        flagTable["GTM"] = R.drawable.flag_guatemala
        flagTable["GUM"] = R.drawable.flag_guam
        flagTable["GRD"] = R.drawable.flag_grenada
        flagTable["GEO"] = R.drawable.flag_georgia
        flagTable["GRC"] = R.drawable.flag_greece
        flagTable["GRL"] = R.drawable.flag_greenland
        flagTable["GNB"] = R.drawable.flag_guinea_bissau
        flagTable["GIN"] = R.drawable.flag_guinea
        flagTable["NAM"] = R.drawable.flag_namibia
        flagTable["NGA"] = R.drawable.flag_nigeria
        flagTable["ZAF"] = R.drawable.flag_south_africa
        flagTable["NLD"] = R.drawable.flag_netherlands
        flagTable["NPL"] = R.drawable.flag_nepal
        flagTable["NOR"] = R.drawable.flag_norway
        flagTable["NZL"] = R.drawable.flag_new_zealand
        flagTable["NCL"] = R.drawable.flag_france
        flagTable["NER"] = R.drawable.flag_niger
        flagTable["NIC"] = R.drawable.flag_nicaragua
        flagTable["TWN"] = R.drawable.flag_taiwan
        flagTable["DNK"] = R.drawable.flag_denmark
        flagTable["DMA"] = R.drawable.flag_dominica
        flagTable["DEU"] = R.drawable.flag_germany
        flagTable["LAO"] = R.drawable.flag_laos
        flagTable["LBR"] = R.drawable.flag_liberia
        flagTable["LVA"] = R.drawable.flag_latvia
        flagTable["RUS"] = R.drawable.flag_russian_federation
        flagTable["LBN"] = R.drawable.flag_lebanon
        flagTable["LSO"] = R.drawable.flag_lesotho
        flagTable["REU"] = R.drawable.flag_france
        flagTable["ROU"] = R.drawable.flag_romania
        flagTable["LUX"] = R.drawable.flag_luxembourg
        flagTable["RWA"] = R.drawable.flag_rwanda
        flagTable["LBY"] = R.drawable.flag_libya
        flagTable["LTU"] = R.drawable.flag_lithuania
        flagTable["LIE"] = R.drawable.flag_liechtenstein
        flagTable["MDG"] = R.drawable.flag_madagascar
        flagTable["MTQ"] = R.drawable.flag_martinique
        flagTable["MHL"] = R.drawable.flag_marshall_islands
        flagTable["MYTA"] = R.drawable.flag_france
        flagTable["MYTB"] = R.drawable.flag_france
        flagTable["MAC"] = R.drawable.flag_macao
        flagTable["MKD"] = R.drawable.flag_macedonia
        flagTable["MWI"] = R.drawable.flag_malawi
        flagTable["MYS"] = R.drawable.flag_malaysia
        flagTable["MLI"] = R.drawable.flag_mali
        flagTable["MEX"] = R.drawable.flag_mexico
        flagTable["MCO"] = R.drawable.flag_monaco
        flagTable["MAR"] = R.drawable.flag_morocco
        flagTable["MUS"] = R.drawable.flag_mauritius
        flagTable["MRT"] = R.drawable.flag_mauritania
        flagTable["MOZ"] = R.drawable.flag_mozambique
        flagTable["MSR"] = R.drawable.flag_montserrat
        flagTable["MDA"] = R.drawable.flag_moldova
        flagTable["MDV"] = R.drawable.flag_maldives
        flagTable["MLT"] = R.drawable.flag_malta
        flagTable["MNG"] = R.drawable.flag_mongolia
        flagTable["USA"] = R.drawable.flag_america
        flagTable["VGB"] = R.drawable.flag_british_virgin_islands
        flagTable["MMR"] = R.drawable.flag_myanmar
        flagTable["FSM"] = R.drawable.flag_micronesia
        flagTable["VUT"] = R.drawable.flag_vanuatu
        flagTable["BHR"] = R.drawable.flag_bahrain
        flagTable["BRB"] = R.drawable.flag_barbados
        flagTable["VAT"] = R.drawable.flag_vatican_city
        flagTable["BHS"] = R.drawable.flag_bahamas
        flagTable["BGD"] = R.drawable.flag_bangladesh
        flagTable["BMU"] = R.drawable.flag_bermuda
        flagTable["BEN"] = R.drawable.flag_benin
        flagTable["VEN"] = R.drawable.flag_venezuela
        flagTable["VNM"] = R.drawable.flag_vietnam
        flagTable["BLR"] = R.drawable.flag_belarus
        flagTable["BLZ"] = R.drawable.flag_belize
        flagTable["BEL"] = R.drawable.flag_belgium
        flagTable["BIH"] = R.drawable.flag_bosnia
        flagTable["BWA"] = R.drawable.flag_botswana
        flagTable["BOL"] = R.drawable.flag_bolivia
        flagTable["BDI"] = R.drawable.flag_burundi
        flagTable["BFA"] = R.drawable.flag_burkina_faso
        flagTable["BTN"] = R.drawable.flag_bhutan
        flagTable["BGR"] = R.drawable.flag_bulgaria
        flagTable["BRA"] = R.drawable.flag_brazil
        flagTable["BRN"] = R.drawable.flag_brunei
        flagTable["SAU"] = R.drawable.flag_saudi_arabia
        flagTable["MNP"] = R.drawable.flag_saipan
        flagTable["CYP"] = R.drawable.flag_cyprus
        flagTable["SMR"] = R.drawable.flag_san_marino
        flagTable["WSM"] = R.drawable.flag_samoa
        flagTable["SEN"] = R.drawable.flag_senegal
        flagTable["SRB"] = R.drawable.flag_serbia_and_montenegro
        flagTable["SYC"] = R.drawable.flag_seychelles
        flagTable["LCA"] = R.drawable.flag_saint_lucia
        flagTable["VCT"] = R.drawable.flag_saint_vicent_and_the_grenadines
        flagTable["KNA"] = R.drawable.flag_saint_kitts_and_nevis
        flagTable["SDN"] = R.drawable.flag_sudan
        flagTable["SUR"] = R.drawable.flag_suriname
        flagTable["LKA"] = R.drawable.flag_sri_lanka
        flagTable["SWZ"] = R.drawable.flag_swaziland
        flagTable["SWE"] = R.drawable.flag_sweden
        flagTable["CHE"] = R.drawable.flag_switzerland
        flagTable["ESP"] = R.drawable.flag_spain
        flagTable["SVK"] = R.drawable.flag_slovakia
        flagTable["SVN"] = R.drawable.flag_slovenia
        flagTable["SYR"] = R.drawable.flag_syria
        flagTable["SLE"] = R.drawable.flag_sierra_leone
        flagTable["SGP"] = R.drawable.flag_singapore
        flagTable["ARE"] = R.drawable.flag_uae
        flagTable["ABW"] = R.drawable.flag_aruba
        flagTable["ARM"] = R.drawable.flag_armenia
        flagTable["ARG"] = R.drawable.flag_argentina
        flagTable["ASM"] = R.drawable.flag_american_samoa
        flagTable["ASC"] = R.drawable.flag_ascension_island
        flagTable["ISL"] = R.drawable.flag_iceland
        flagTable["HTI"] = R.drawable.flag_haiti
        flagTable["IRL"] = R.drawable.flag_ireland
        flagTable["AZE"] = R.drawable.flag_azerbaijan
        flagTable["AFG"] = R.drawable.flag_afghanistan
        flagTable["AND"] = R.drawable.flag_andorra
        flagTable["AIA"] = R.drawable.flag_anguilla
        flagTable["ATG"] = R.drawable.flag_antigua_and_barbuda
        flagTable["USAA"] = R.drawable.flag_alaska
        flagTable["ALB"] = R.drawable.flag_albania
        flagTable["DZA"] = R.drawable.flag_algeria
        flagTable["AGO"] = R.drawable.flag_angola
        flagTable["ERI"] = R.drawable.flag_eritrea
        flagTable["EST"] = R.drawable.flag_estonia
        flagTable["ECU"] = R.drawable.flag_ecuador
        flagTable["ETH"] = R.drawable.flag_ethiopia
        flagTable["SLV"] = R.drawable.flag_el_salvador
        flagTable["GBR"] = R.drawable.flag_united_kingdom
        flagTable["VIR"] = R.drawable.flag_us_virgin_islands
        flagTable["YEM"] = R.drawable.flag_yemen
        flagTable["OMN"] = R.drawable.flag_oman
        flagTable["AUT"] = R.drawable.flag_austria
        flagTable["HND"] = R.drawable.flag_honduras
        flagTable["JOR"] = R.drawable.flag_jordan
        flagTable["UGA"] = R.drawable.flag_uganda
        flagTable["URY"] = R.drawable.flag_uruguay
        flagTable["UZB"] = R.drawable.flag_uzbekistan
        flagTable["UKR"] = R.drawable.flag_ukraine
        flagTable["IRQ"] = R.drawable.flag_iraq
        flagTable["IRN"] = R.drawable.flag_iran
        flagTable["ISR"] = R.drawable.flag_israel
        flagTable["ISRP"] = R.drawable.flag_israel
        flagTable["EGY"] = R.drawable.flag_egypt
        flagTable["ITA"] = R.drawable.flag_italy
        flagTable["IND"] = R.drawable.flag_india
        flagTable["IDN"] = R.drawable.flag_indonesia
        flagTable["JPN"] = R.drawable.flag_japan
        flagTable["JAM"] = R.drawable.flag_jamaica
        flagTable["ZMB"] = R.drawable.flag_zambia
        flagTable["GNQ"] = R.drawable.flag_equatorial_guinea
        flagTable["CHN"] = R.drawable.flag_china
        flagTable["CAF"] = R.drawable.flag_central_african_republic
        flagTable["DJI"] = R.drawable.flag_djibouti
        flagTable["GIB"] = R.drawable.flag_gibraltar
        flagTable["ZWE"] = R.drawable.flag_zimbabwe
        flagTable["TCD"] = R.drawable.flag_chad
        flagTable["CZE"] = R.drawable.flag_czech_republic
        flagTable["CHL"] = R.drawable.flag_chile
        flagTable["CMR"] = R.drawable.flag_cameroon
        flagTable["KAZ"] = R.drawable.flag_kazakhstan
        flagTable["QAT"] = R.drawable.flag_qatar
        flagTable["CPV"] = R.drawable.flag_cape_verde
        flagTable["KHM"] = R.drawable.flag_cambodia
        flagTable["CAN"] = R.drawable.flag_canada
        flagTable["KEN"] = R.drawable.flag_kenya
        flagTable["CYM"] = R.drawable.flag_cayman_islands
        flagTable["COM"] = R.drawable.flag_comoros
        flagTable["CRI"] = R.drawable.flag_costa_rica
        flagTable["COL"] = R.drawable.flag_colombia
        flagTable["COD"] = R.drawable.flag_congo
        flagTable["CUB"] = R.drawable.flag_cuba
        flagTable["KWT"] = R.drawable.flag_kuwait
        flagTable["HRV"] = R.drawable.flag_croatia
        flagTable["KGZ"] = R.drawable.flag_kyrgyzstan
        flagTable["KIR"] = R.drawable.flag_kiribati
        flagTable["TJK"] = R.drawable.flag_tajikistan
        flagTable["TZA"] = R.drawable.flag_tanzania
        flagTable["THA"] = R.drawable.flag_thailand
        flagTable["TUR"] = R.drawable.flag_turkey
        flagTable["TCA"] = R.drawable.flag_turks_and_caicos_islands
        flagTable["TGO"] = R.drawable.flag_togo
        flagTable["TON"] = R.drawable.flag_tonga
        flagTable["TKM"] = R.drawable.flag_turkmenistan
        flagTable["TUN"] = R.drawable.flag_tunisia
        flagTable["TTO"] = R.drawable.flag_trinidad_and_tobago
        flagTable["PAN"] = R.drawable.flag_panama
        flagTable["PRY"] = R.drawable.flag_paraguay
        flagTable["FRO"] = R.drawable.flag_faroe_islands
        flagTable["PAK"] = R.drawable.flag_pakistan
        flagTable["PNG"] = R.drawable.flag_papua_new_guinea
        flagTable["PER"] = R.drawable.flag_peru
        flagTable["PRT"] = R.drawable.flag_portugal
        flagTable["FLK"] = R.drawable.flag_falkland_islands
        flagTable["POL"] = R.drawable.flag_poland
        flagTable["FRA"] = R.drawable.flag_france
        flagTable["PRIA"] = R.drawable.flag_puerto_rico
        flagTable["PRIB"] = R.drawable.flag_puerto_rico
        flagTable["GUF"] = R.drawable.flag_france_guiana
        flagTable["FJI"] = R.drawable.flag_fiji
        flagTable["FIN"] = R.drawable.flag_finland
        flagTable["PHL"] = R.drawable.flag_philippines
        flagTable["USAH"] = R.drawable.flag_hawaii
        flagTable["HUN"] = R.drawable.flag_hungary
        flagTable["AUS"] = R.drawable.flag_australia
        flagTable["HKG"] = R.drawable.flag_hong_kong
        flagTable["ATA"] = R.drawable.flag_antartica
        flagTable["COG"] = R.drawable.flag_democratic_republic_congo
        flagTable["COK"] = R.drawable.flag_cook_islands
        flagTable["DGA"] = R.drawable.flag_diego_garcia_i
        flagTable["DOMA"] = R.drawable.flag_dominican_republic
        flagTable["DOMB"] = R.drawable.flag_dominican_republic
        flagTable["DOMC"] = R.drawable.flag_dominican_republic
        flagTable["PYF"] = R.drawable.flag_french_polynesia
        flagTable["MNE"] = R.drawable.flag_montenegro
        flagTable["NRU"] = R.drawable.flag_nauru
        flagTable["NIU"] = R.drawable.flag_niue
        flagTable["PLW"] = R.drawable.flag_palau
        flagTable["SOM"] = R.drawable.flag_somalia
        flagTable["SPM"] = R.drawable.flag_saint_pierre
        flagTable["STP"] = R.drawable.flag_sao_tome_and_principe
        flagTable["SXM"] = R.drawable.flag_sint_maarten
        flagTable["SHN"] = R.drawable.flag_saint_helena
        flagTable["TKL"] = R.drawable.flag_tokelau
        flagTable["TUV"] = R.drawable.flag_tuvalu
        flagTable["WLF"] = R.drawable.flag_wallis_and_futuna
        flagTable["TLS"] = R.drawable.flag_timor_leste
        flagTable["KOR"] = R.drawable.flag_korea
        flagTable["ALA"] = R.drawable.flag_aland_islands
        flagTable["BESB"] = R.drawable.flag_bonaire
        flagTable["IOT"] = R.drawable.flag_british_indian_ocean_territory
        flagTable["CXR"] = R.drawable.flag_christmas_island
        flagTable["CCK"] = R.drawable.flag_cocos_islands
        flagTable["CIV"] = R.drawable.flag_cote_d_ivoire
        flagTable["CUW"] = R.drawable.flag_curacao
        flagTable["GGY"] = R.drawable.flag_guernsey
        flagTable["IMN"] = R.drawable.flag_isle_of_man
        flagTable["JEY"] = R.drawable.flag_jersey
        flagTable["PRK"] = R.drawable.flag_dominican_republic
        flagTable["NFK"] = R.drawable.flag_norfolk_island
        flagTable["PSE"] = R.drawable.flag_palestinian_territory
        flagTable["BESS"] = R.drawable.flag_saba
        flagTable["BLM"] = R.drawable.flag_saint_barthelemy
        flagTable["MAF"] = R.drawable.flag_saint_martin
        flagTable["BESE"] = R.drawable.flag_sint_eustatius
        flagTable["ESH"] = R.drawable.flag_western_sahara
        flagTable["TAA"] = R.drawable.flag_tristan_da_cunha
        flagTable["SLB"] = R.drawable.flag_soloman_islands
        flagTable["BES"] = R.drawable.flag_netherlands
        flagTable["MYT"] = R.drawable.flag_france
        flagTable["DOM"] = R.drawable.flag_dominican_republic
        flagTable["PRI"] = R.drawable.flag_puerto_rico
        flagTable["SSD"] = R.drawable.flag_south_sudan
        flagTable["ATF"] = R.drawable.flag_french_southern_and_antarctic_lands
        flagTable["EUR"] = R.drawable.flag_euro

        for( item in flagTable.iterator() ){
            countryIdTable[ item.key ]?.imgRes = item.value

        }
    }

    fun getCountry( uniqueId:String ): CountryItem? {

        return when {
            uniqueId.length > 3 -> {
                countryIdTable[uniqueId.substring(0, 3)]
            }
            uniqueId.length == 3 -> {
                countryIdTable[uniqueId]
            }
            uniqueId.length == 2 -> {
                countryISOTable[uniqueId]
            }
            else -> {
                null
            }

        }


    }

    fun getFlag( uniqueId:String? ): Int {
        if( uniqueId == null )
            return R.drawable.flag_blank

        val key = if( uniqueId.length == 2 ) countryISOTable[uniqueId]?.uniqueId ?: uniqueId else uniqueId

        return flagTable[key] ?: R.drawable.flag_blank

    }

    /**
     * 번호를 분석해 어느나라 전화번호인지 알기위한 함수
     * @param phoneNumber    전화번호
     * @return
     */
    fun getCountryFromCountryCode(phoneNumber: String): CountryItem? {

        val length = if( phoneNumber.length > 5 ) 5 else phoneNumber.length

        var item: CountryItem? = null

        if( length >= 5) {
            item = countryCodeTable[ phoneNumber.substring(0, 5) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 4) {
            item = countryCodeTable[ phoneNumber.substring(0, 4) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 3) {
            item = countryCodeTable[ phoneNumber.substring(0, 3) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 2) {
            item = countryCodeTable[ phoneNumber.substring(0, 2) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 1) {
            item = countryCodeTable[ phoneNumber.substring(0, 1) ]
            if( item != null ) {
                return item
            }
        }

        return item
    }

    /**
     * 번호를 분석해 어느나라 전화번호인지 알기위한 함수
     * @param uniqueId      현재 세팅되어 있는 국가(다이얼)
     * @param phoneNumber	전화번호
     * @return
     */
    fun getCountryFromCountryCode( uniqueId:String?, phoneNumber:String ): CountryItem? {

        val length = if( phoneNumber.length > 5 ) 5 else phoneNumber.length

        var item: CountryItem? = null

        if( length >= 5) {
            item = countryCodeTable[ phoneNumber.substring(0, 5) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 4) {
            item = countryCodeTable[ phoneNumber.substring(0, 4) ]
            if( item != null ) {
                return item
            }
        }

        if( length >= 3) {
            item = countryCodeTable[ phoneNumber.substring(0, 3) ]
            if( item != null ) {
                return  if( item.uniqueId == "MAF" || item.uniqueId == "BLM" || item.uniqueId == "GLP" || item.uniqueId == "ATA" || item.uniqueId == "NFK" ) {
                    if( uniqueId == "MAF" || uniqueId == "BLM" || uniqueId == "GLP" || uniqueId == "ATA" || uniqueId == "NFK" ) {
                        countryIdTable[uniqueId]
                    } else {
                        countryCodeTable[phoneNumber.substring(0, 3)]
                    }
                } else
                    item

            }
        }

        if( length >= 2) {
            item = countryCodeTable[ phoneNumber.substring(0, 2) ]
            if( item != null ) {
                return if( item.uniqueId == "JEY" || item.uniqueId == "GBR" || item.uniqueId == "GGY" || item.uniqueId == "IMN" ) {
                    if( uniqueId == "JEY" || uniqueId == "GBR" || uniqueId == "GGY" || uniqueId == "IMN" ) {
                        countryIdTable[uniqueId]
                    } else {
                        countryCodeTable[phoneNumber.substring(0, 2)]
                    }
                } else
                    item


//                if( uniqueId.isNullOrEmpty() && ( item.uniqueId == "JEY" || item.uniqueId == "IMN" || item.uniqueId == "GGY" ) )
//                            countryCodeTable[phoneNumber.substring(0, 2)]
//                        else
//                            item
//                    countryIdTable[uniqueId]
            }
        }

        if( length >= 1 ) {

            if( !uniqueId.isNullOrEmpty() && (uniqueId == "USA" || uniqueId == "CAN" ) && phoneNumber.startsWith("1" ) ) {
                item = countryIdTable[uniqueId]
                if(item != null) {
                    return item
                }
            }

            if( !uniqueId.isNullOrEmpty() && (uniqueId == "KAZ" || uniqueId == "RUS") && phoneNumber.startsWith("7")) {
                item = countryIdTable[uniqueId]
                if(item != null) {
                    return item
                }
            }

            item = countryCodeTable[ phoneNumber.substring(0, 1) ]
            if( item != null ) {
                return item
            }
        }

        return item
    }

}