package baycon.android.common.utils.convertio

import android.content.Context
import android.graphics.Bitmap
import android.graphics.pdf.PdfRenderer
import android.net.Uri
import android.os.ParcelFileDescriptor
import android.webkit.MimeTypeMap
import androidx.lifecycle.liveData
import baycon.android.common.api.Resource
import baycon.android.common.utils.file.DirUtils
import baycon.android.common.utils.file.FileHelperKt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList

/** use this util with hilt module */
class ConvertIoUtil( private val context: Context, private val convertIODataSource: ConvertIoDataSource ) {

    companion object {
        const val FULL_SIZE_ERROR = "FULL_SIZE_ERROR"
        const val NO_TYPE = "NO_TYPE"
        const val CONVERT_ERROR = "CONVERT_ERROR"
    }

    fun convertIO(fileUri: Uri?, outFileIndex: Int ) = liveData {

        emit( Resource.loading( null ) )


        val internalFile = FileHelperKt.copyInternal( context, DirUtils.getWriteTempDir( context ), fileUri )
        val internalFileMime = getMimeType( internalFile )

        if( internalFile == null ) {

            emit( Resource.error( NO_TYPE, null ) )
            return@liveData

        } else {
            //파일 사이즈가 10Mb 넘을 경우 에러 처리를한다.
            val fileLength = internalFile.length()
            if( fileLength >= 1024 * 1024 * 10 ) {

                emit( Resource.error( FULL_SIZE_ERROR, null ) )
                return@liveData

            }
        }



        val job = withContext( Dispatchers.IO ) {

            var ret: Pair<ArrayList<File>, String?>? = null

            if ( internalFileMime?.toLowerCase(Locale.getDefault()) == "application/pdf" ) {

                ret = docToPdf(internalFile)?.let { files ->

                    files to internalFileMime

                }

            }
            else {
                // 1 키생성
                convertIoKeyGenerate()?.let { key ->
                    // 2 파일 업로드
                    uploadFile( key, internalFile, internalFileMime )?.let { id ->
                        // 3 업로드 상태 체크
                        val result = checkStatus( id )
                        if( result.first ) {
                            val downloadUrl = result.second
                            // 4 파일 다운로드
                            downloadFile( downloadUrl, outFileIndex )?.let { file ->

                                val pdf = docToPdf(file)

                                if ( pdf != null )
                                    ret = pdf to internalFileMime
                            }

                        }

                    }

                }

            }
            ret
        }

        job?.let {

            emit( Resource.success( it ) )

        } ?: emit( Resource.error( CONVERT_ERROR, null ) )


    }

    private suspend fun convertIoKeyGenerate(): ResultConvertIoKey? {

        val generateResult = convertIODataSource.convertIoKeyGenerate()

        generateResult?.let {
//            return operation( response = it, onOk = { it }, onNo = { null } )

            return when (it.retCode) {
                200 -> {
                    it
                }
                else -> {
                    null
                }
            }

        } ?: return null

    }

    private suspend fun uploadFile(key: ResultConvertIoKey, file: File?, fileMime: String? ): String? {
        Timber.d("convertIo :: uploadFile ")
        val requestFile = file?.asRequestBody( fileMime?.toMediaTypeOrNull() )
        val result = convertIODataSource.faxFileUpload( key.data?.id ?: "", file?.name ?: "", requestFile!! )

        result?.let {

            when ( it.retCode ) {

                200 -> {

                    return it.data?.id
                    //status check

                }
                else -> {

                    return null
                    //fail

                }

            }

        }

        return null

    }

    private suspend fun checkStatus( id: String ): Pair<Boolean, String> {
        Timber.d("convertIo :: checkStatus ")
        val result = convertIODataSource.convertIoUploadStatus( id )

        result?.let {

            when ( it.retCode ) {

                200 -> {

                    if( it.data?.step == "finish" ) {

                        val output = result.data?.output
                        return if( output == null ) {

                            false to ""

                        } else {

                            val url = output["url"]
                            ( url != null ) to ( url ?: "" )

                        }

                    } else {

                        delay( 1000 )
                        return checkStatus( id )

                    }

                }
                else -> {

                    return false to ""

                }

            }

        }

        return false to ""

    }

    private suspend fun downloadFile( url: String, outFileIndex: Int ): File? {
        Timber.d("convertIo :: download File ")
        val index = url.indexOf( "/", url.indexOf( "://" ) + 3 )
        val baseUrl = url.substring( 0, index )
        val lastPth = url.substring( index, url.length )
        val extension = url.substring( url.lastIndexOf( "." ) )

        convertIODataSource.convertIoDownload( baseUrl, lastPth )?.let {


            val byteStream = it.byteStream()

            val dir = DirUtils.getWriteTempDir( context )

            val outPdfFile = File( dir, "convertIo$outFileIndex$extension" )

            val fos = FileOutputStream( outPdfFile )

            val writeSize = byteStream.copyTo( fos )

            fos.close()

            return if( writeSize > 0 ) {

                outPdfFile

            } else {

                null

            }

        }

        return null

    }

    private fun docToPdf(pdfFile: File): ArrayList<File>? {
        Timber.d("convertIo :: doc to pdf ")
        var pdfFiles: ArrayList<File>? = null


        val fileList = runCatching {
            val dir = DirUtils.getWriteTempDir( context )

            val fileList = ArrayList<File>()

            val parcelFileDescriptor = ParcelFileDescriptor.open( pdfFile, ParcelFileDescriptor.MODE_READ_ONLY )
            val pdfRenderer = PdfRenderer( parcelFileDescriptor )

            val pdfPageCount = pdfRenderer.pageCount

            for( i in 0 until pdfPageCount ) {

                val pdfPage = pdfRenderer.openPage( i )

                val bitmap = Bitmap.createBitmap( pdfPage.width, pdfPage.height, Bitmap.Config.ARGB_8888 )
                pdfPage.render( bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY )
                pdfPage.close()

                val file = File( dir, "convertIo${System.currentTimeMillis()}.png" )
                fileList.add( file )

                bitmap.compress( Bitmap.CompressFormat.PNG, 90, FileOutputStream( file ) )

                bitmap.recycle()

            }
            fileList

        }
        fileList.onSuccess {

            pdfFiles = it

        }.onFailure {

            Timber.e("Doc to Pdf error")
            pdfFiles = null

        }

        return pdfFiles

    }


    private fun getMimeType( file: File? ): String? {

        if( file == null )
            return null

        val extension = file.absolutePath.substring( file.absolutePath.lastIndexOf(".") + 1);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)

    }

}