package baycon.android.common.utils.encrypt

import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object AES128Util {

    fun encrypt( str:String?, enKey:String? ): String? {

        if( str == null || enKey == null )
            return null

        val key = StringBuffer()
        key.append( enKey.substring( 10, 16 ) )
        key.append( enKey.substring( 3, 8 ) )
        key.append( enKey.substring( 20, 25 ) )

        val keySpec = SecretKeySpec( key.toString().toByteArray(), "AES" )

        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding" )
        cipher.init( Cipher.ENCRYPT_MODE, keySpec, IvParameterSpec( key.toString().toByteArray() ) )

        val encrypted = cipher.doFinal( str.toByteArray() )

        return Base64.encodeToString( encrypted, Base64.NO_WRAP )
    }


    fun decode( str:String?, enKey:String? ): String? {

        if( str == null || enKey == null )
            return null

        val key = StringBuffer()
        key.append( enKey.substring( 10, 16 ) )
        key.append( enKey.substring( 3, 8 ) )
        key.append( enKey.substring( 20, 25 ) )

        val keySpec = SecretKeySpec( key.toString().toByteArray(), "AES" )

        val cipher: Cipher = Cipher.getInstance("AES/CBC/PKCS5Padding" )
        cipher.init( Cipher.DECRYPT_MODE, keySpec, IvParameterSpec( key.toString().toByteArray() ) )

        val decrypted = cipher.doFinal( Base64.decode( str, Base64.NO_WRAP ) )

        return String( decrypted )
    }

}