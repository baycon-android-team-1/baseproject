package baycon.android.common.utils.tiff

import android.content.Context
import android.graphics.*
import android.net.Uri
import baycon.android.common.extension.setBrightnessContrast
import org.beyka.tiffbitmapfactory.*
import java.io.File
import java.io.FileInputStream
import java.io.InputStream

object TiffUtil {
//
//    fun imageToTiff( outFilePath:String, data:String, index:Int, scale:Int, brightness:Float ){
//
//        getRotateToBrightnessContrast( data, brightness )?.let{
//            transImageToTifFile( outFilePath, it, scale, index != 0 )
//        }
//    }


    private fun getInputStream( context:Context, src:Any ): InputStream? {
        return if ( src is Uri) {
            context.contentResolver.openInputStream( src )
        } else {
            FileInputStream( ( src as File ).absolutePath )
        }
    }

    fun imageToTiff(context:Context, src:Any, outFilePath:String, index:Int, scale:Int, brightness:Float ){

        val bitmap = getRotateToBrightnessContrast( context, src, brightness )
        transImageToTifFile( outFilePath, bitmap, scale, index != 0 )

    }

    private fun getRotateToBrightnessContrast( context:Context, src:Any, brightness:Float ) :Bitmap {

        val bitmap = BitmapFactory.decodeStream( getInputStream( context, src ) )
        val degree = BitmapUtils.getOrientation( getInputStream( context, src ) )
        val matrix = Matrix()
        matrix.setRotate( degree.toFloat() )

        val ret = Bitmap.createBitmap( bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true ).setBrightnessContrast( brightness )
        bitmap.recycle()

        return ret
        //return bitmap.setRotateToBrightnessContrast( matrix, brightness )
//        return bitmap.setBrightnessContrast( brightness )
    }

//    fun getRotateBitMap( filePath: String? ) :Bitmap? {
//
//        if( filePath == null )
//            return null
//
//        val bitmap = BitmapFactory.decodeFile( filePath )
//        val degree = BitmapUtils.getOrientation( filePath )
//        val matrix = Matrix()
//        matrix.setRotate( degree.toFloat() )
//
//        return Bitmap.createBitmap( bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true )
//
//    }

    fun getRotateBitMap( inputStream: InputStream ) :Bitmap? {

        val bitmap = BitmapFactory.decodeStream( inputStream )
        val degree = BitmapUtils.getOrientation( inputStream )
        val matrix = Matrix()
        matrix.setRotate( degree.toFloat() )

        return Bitmap.createBitmap( bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true )

    }

//    fun getFileToFaxImage( filePath: String? ) :Bitmap? {
//        if( filePath == null )
//            return null
//
//        val bitmap = BitmapFactory.decodeFile( filePath )
//        val degree = BitmapUtils.getOrientation( filePath )
//        val matrix = Matrix()
//        matrix.setRotate( degree.toFloat() )
//
//        return getImageToFaxImage( Bitmap.createBitmap( bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true ) )
//
//    }

    fun getFileToFaxImage( inputStream: InputStream ) :Bitmap {

        val bitmap = BitmapFactory.decodeStream( inputStream )
        val degree = BitmapUtils.getOrientation( inputStream )
        val matrix = Matrix()
        matrix.setRotate( degree.toFloat() )

        return getImageToFaxImage( Bitmap.createBitmap( bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true ) )

    }

    private fun getImageToFaxImage( src:Bitmap ): Bitmap {
        val maxWidth = 1728
        val maxHeight = 2341

        var bmpWidth = src.width
        var bmpHeight = src.height

//        if(bmpWidth > maxWidth) {
        val scaleWidth = if( maxWidth.toFloat() / bmpWidth.toFloat() == 0f ) 1f else maxWidth.toFloat() / bmpWidth.toFloat()
//        }

//        if(bmpHeight > maxHeight) {
        val scaleHeight = if( maxHeight.toFloat() / bmpHeight.toFloat() == 0f) 1f else maxHeight.toFloat() / bmpHeight.toFloat()
//        }

        if(scaleWidth > scaleHeight) {
            bmpWidth = (bmpWidth * scaleHeight).toInt()
            bmpHeight = (bmpHeight * scaleHeight).toInt()
        } else {
            bmpWidth = (bmpWidth * scaleWidth).toInt()
            bmpHeight = (bmpHeight * scaleWidth).toInt()

        }

        val bitmap = Bitmap.createScaledBitmap( src, bmpWidth, bmpHeight, true)

        val backBitmap = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.RGB_565)
        val backCanvas = Canvas(backBitmap)
        backCanvas.drawColor(Color.WHITE)
        backCanvas.drawBitmap(bitmap, ((maxWidth - bitmap.width) / 2).toFloat(), ((maxHeight - bitmap.height) / 2).toFloat(), null)

        bitmap.recycle()

        return backBitmap
    }

    private fun transImageToTifFile( outFilePath:String, rotateBitmap: Bitmap, scale:Int, isAppend:Boolean ) {

//        val maxWidth = 1728
//        val maxHeight = 2341
//
//        var bmpWidth = rotateBitmap.width
//        var bmpHeight = rotateBitmap.height
//
////        if(bmpWidth > maxWidth) {
//        val scaleWidth = if(maxWidth.toFloat() / bmpWidth.toFloat() == 0f) 1f else maxWidth.toFloat() / bmpWidth.toFloat()
////        }
//
////        if(bmpHeight > maxHeight) {
//        val scaleHeight = if(maxHeight.toFloat() / bmpHeight.toFloat() == 0f) 1f else maxHeight.toFloat() / bmpHeight.toFloat()
////        }
//
//        if(scaleWidth > scaleHeight) {
//            bmpWidth = (bmpWidth * scaleHeight).toInt()
//            bmpHeight = (bmpHeight * scaleHeight).toInt()
//        } else {
//            bmpWidth = (bmpWidth * scaleWidth).toInt()
//            bmpHeight = (bmpHeight * scaleWidth).toInt()
//
//        }
//
//        val bitmap = Bitmap.createScaledBitmap(rotateBitmap, bmpWidth, bmpHeight, true)
//
//        val backBitmap = Bitmap.createBitmap(maxWidth, maxHeight, Bitmap.Config.RGB_565)
//        val backCanvas = Canvas(backBitmap)
//        backCanvas.drawColor(Color.WHITE)
//        backCanvas.drawBitmap(bitmap, ((maxWidth - bitmap.width) / 2).toFloat(), ((maxHeight - bitmap.height) / 2).toFloat(), null)

        val backBitmap = getImageToFaxImage( rotateBitmap )

        val tifOptions = TiffSaver.SaveOptions()
        tifOptions.compressionScheme = CompressionScheme.CCITTFAX3
        tifOptions.orientation = Orientation.UNAVAILABLE

        tifOptions.resUnit = ResolutionUnit.INCH
        tifOptions.xResolution = 204f //204f
        tifOptions.yResolution = 196f

        //Add author tag to output file
        tifOptions.author = "softbay"
        //Add copyright tag to output file
        tifOptions.copyright = "Copyright 2020. Softbay Co.,Ltd"

        //Save image as tif. If image saved succesfull true will be returned
        if( isAppend )
            TiffSaver.appendBitmap( outFilePath, backBitmap, tifOptions, scale )
        else
            TiffSaver.saveBitmap( outFilePath, backBitmap, tifOptions, scale )

        rotateBitmap.recycle()

//        bitmap.recycle()

    }

    fun getTiffBitmap( tiffFile:File?, position:Int, maxWidth:Int ): Bitmap {
        val options = TiffBitmapFactory.Options()
        options.inJustDecodeBounds = true
        options.inDirectoryNumber = position
        TiffBitmapFactory.decodeFile( tiffFile, options )
        val bmpWidth = options.outWidth
        var inSampleSize = 1
        if( bmpWidth > maxWidth ) {
            val halfWidth = bmpWidth / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ( (halfWidth / inSampleSize) > maxWidth ) {
                inSampleSize *= 2;
            }
        }

        options.inJustDecodeBounds = false
        options.inSampleSize = inSampleSize

        // Specify the amount of memory available for the final bitmap and temporary storage.
        options.inAvailableMemory = 20000000 // bytes

        return TiffBitmapFactory.decodeFile( tiffFile, options )
    }

}