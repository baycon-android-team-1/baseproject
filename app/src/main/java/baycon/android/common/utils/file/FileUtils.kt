package com.baycon.android.common.utils

import android.content.Context
import java.io.*

object FileUtils {

    fun deleteRecursive( fileOrDir: File) {
        if( fileOrDir.isDirectory ) {
            val listFile = fileOrDir.listFiles()
            if( listFile != null ) {
                for( file in listFile )
                    deleteRecursive( file )
            }
        }

        fileOrDir.delete()
    }
}