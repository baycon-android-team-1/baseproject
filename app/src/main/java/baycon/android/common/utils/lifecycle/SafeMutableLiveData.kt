package baycon.android.common.utils.lifecycle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.lang.ref.WeakReference
import java.util.*

class SafeMutableLiveData<T> : MutableLiveData<T>() {

//    private var weakLifecycleOwner: WeakReference<LifecycleOwner>? = null

    private val weakHashMap = WeakHashMap<String, WeakReference<LifecycleOwner> >()

//    override fun observe( owner: LifecycleOwner, observer: Observer<in T> ) {
//        weakLifecycleOwner?.get()?.let {
//            removeObservers(it)
//        }
//        weakLifecycleOwner = WeakReference(owner)
//        super.observe(owner, observer)
//    }

    fun safeObserve( tag: String, owner: LifecycleOwner,  observer: Observer<in T> ) {

        weakHashMap[ tag ]?.get()?.let { removeObservers( it ) }

        weakHashMap[ tag ] = WeakReference(owner)
        super.observe(owner, observer)
    }

    override fun setValue(value: T) {
        try {
            super.setValue(value)
        } catch (e: Exception) {
            super.postValue(value)
        }
    }
}