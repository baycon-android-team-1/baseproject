package baycon.android.common.utils.lifecycle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 */
open class Event<out T>(private val content: T) {

    var hasBeenHandled = false
        private set // Allow external read but not write

    /**
     * Returns the content and prevents its use again.
     */
    fun getContentIfNotHandled(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}

fun <T> LiveData<Event<T>>.eventObserve(owner: LifecycleOwner, onChanged: (T) -> Unit ): Observer<Event<T>> {
    val wrappedObserver = Observer<Event<T>> { t ->
        t.getContentIfNotHandled()?.let {
            onChanged.invoke(it)
        }
    }
    observe(owner, wrappedObserver)
    return wrappedObserver
}

//example

//viewModel
//private val _openEvent = MutableLiveData<Event<String>>()
//val openEvent: LiveData<Event<String>> get() = _openEvent
//
//val sampleText: MutableLiveData<String> = MutableLiveData()
//
//fun onClickEvent(text: String) {
//    _openEvent.value = Event(text)
//}

//activity
//viewModel.openEvent.eventObserve(this) { sampleText ->
//    val intent = Intent(this, NextActivity::class.java)
//    intent.putExtra("sample", sampleText)
//    startActivity(intent)
//}

