package baycon.android.common.utils.convertio

import com.google.gson.annotations.SerializedName
import com.google.gson.internal.LinkedTreeMap

open class ResultConvertIoKey {

    @SerializedName( "code" )
    val retCode: Int = 0

    @SerializedName( "status" )
    val errorMsg: String? = null

    @SerializedName( "data" )
    var data:KeyModel? = null

    class KeyModel{
        var id:String = ""
        var file:String = ""
        var size:Long = 0L

        var step:String = ""
        @SerializedName( "step_percent" )
        var stepPercent:Any? = null
        var minutes:Any? = null

        var output: LinkedTreeMap<String, String>? = null
        override fun toString(): String {
            return "KeyModel(id='$id', file='$file', size=$size, step='$step', stepPercent=$stepPercent, minutes=$minutes, output=$output)"
        }


    }

    override fun toString(): String {
        return "ResultConvertIoKey(retCode=$retCode, errorMsg=$errorMsg, data=$data)"
    }

}