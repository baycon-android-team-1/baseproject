package baycon.android.common.utils.string

import android.os.Build
import android.text.Html
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.regex.Pattern

@Suppress("DEPRECATION", "unused")
object StringUtil {

    private fun isNumber(phoneNumber: String ): Boolean {
        return Pattern.matches("^[0-9]*$", phoneNumber)
    }

    fun isNewVersion( clientVersion:String?, serverVersion:String?, checkLength:Int = 3 ):Boolean {

        if( clientVersion == null )
            return false

        if( serverVersion == null ) {
            return false
        }

        var parseClientVersion = clientVersion.replace( ".", "" )
        var parseServerVersion = serverVersion.replace( ".", "" )

        if(checkLength > 0) {

            if(parseClientVersion.length < checkLength || parseServerVersion.length < checkLength) return false

            parseServerVersion = parseServerVersion.substring(0, checkLength)
            parseClientVersion = parseClientVersion.substring(0, checkLength)
        }

        if( !isNumber( parseServerVersion ) || !isNumber( parseClientVersion ) ) return false

        return parseServerVersion.toInt() > parseClientVersion.toInt()

    }

    fun transStringToLong(value: String?): Long {

        if( value == null )
            return -1

        var result: Long = -1
        try {
            result = value.toLong()
        } catch(e: Exception) {}

        return result
    }

    fun transStringToInteger(value: String?): Int {
        if( value == null )
            return -1

        var result = -1
        try {
            result = value.toInt()
        } catch(e: Exception) {
        }

        return result
    }

    fun getMoneyFormat( num: Double? ): String {
        if( num == null )
            return "0"

        val df = DecimalFormat("#,##0")
        val dfs = DecimalFormatSymbols()
        dfs.groupingSeparator = ','
        df.groupingSize = 3
        df.decimalFormatSymbols = dfs

        return df.format( num )
    }

    fun isEng(ch: Char): Boolean {

        return when( ch.toInt() ) {
            in 0x0041..0x005A -> true
            in 0x0061..0x007A -> true
            else -> false

        }
    }

    fun isJapan(unicodeBlock: Character.UnicodeBlock?): Boolean {
        return Character.UnicodeBlock.HIRAGANA === unicodeBlock || Character.UnicodeBlock.KATAKANA === unicodeBlock

    }

    fun isHangle(unicodeBlock: Character.UnicodeBlock?): Boolean {
        return Character.UnicodeBlock.HANGUL_SYLLABLES === unicodeBlock || Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO === unicodeBlock || Character.UnicodeBlock.HANGUL_JAMO === unicodeBlock

    }

    fun isChinese(unicodeBlock: Character.UnicodeBlock?): Boolean {
        return unicodeBlock === Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || unicodeBlock === Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || unicodeBlock === Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || unicodeBlock === Character.UnicodeBlock.GENERAL_PUNCTUATION || unicodeBlock === Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || unicodeBlock === Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
    }

    fun formatTime(secs: Int): String {
        return String.format("%02d:%02d:%02d", secs / 3600, secs % 3600 / 60, secs % 60)
    }

    fun formatTimeMin(secs: Int): String {
        return String.format("%02d:%02d", secs / 60, secs % 60)
    }

    fun remainingMin(secs: Int?): String {
        return if( secs == null )
            "0"
        else
            String.format("%d", secs / 60)
    }


    fun getNumberAndPlusOtherTrim(str: String): String {

        var strs = ""
        for( i in str.indices ) {
            val st = str.substring( i, i + 1 )
            if( st == "1" || st == "2" || st == "3" || st == "4" || st == "5" || st == "6" || st == "7" || st == "8" || st == "9" || st == "+" || st == "0" ) {
                if(st == "+" && i != 0) {
                    continue
                }

                strs += st
            }
        }
        return strs
    }

    fun getNumberTrim(str: String): String {

        var strs = ""
        for( i in str.indices ) {
            val st = str.substring( i, i + 1 )
            if( st == "1" || st == "2" || st == "3" || st == "4" || st == "5" || st == "6" || st == "7" || st == "8" || st == "9" || st == "0" ) {
                strs += st
            }
        }
        return strs
    }


    fun stringToHtml(str:String ): CharSequence {
        return if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ){
            Html.fromHtml( str, Html.FROM_HTML_MODE_LEGACY )
        } else {
            Html.fromHtml( str )
        }
    }

    private val CHOSUNG = charArrayOf(0x3131.toChar(), 0x3132.toChar(), 0x3134.toChar(), 0x3137.toChar(), 0x3138.toChar(), 0x3139.toChar(), 0x3141.toChar(), 0x3142.toChar(), 0x3143.toChar(), 0x3145.toChar(), 0x3146.toChar(), 0x3147.toChar(), 0x3148.toChar(), 0x3149.toChar(), 0x314a.toChar(), 0x314b.toChar(), 0x314c.toChar(), 0x314d.toChar(), 0x314e.toChar())

    fun zeroLeftTrim(str: String): String {
        var ret: String = str
        while( ret.isNotEmpty() ) {
            if( ret.length == 1 && ret.indexOf("0" ) == 0 ) {
                return ""
            } else if( ret.indexOf("0" ) == 0 ) {
                ret = ret.substring( 1, ret.length )
            } else {
                break
            }
        }

        return ret
    }

    fun chosungToNumber( src:String ): String{

        var result = ""

        for( ch in src.toUpperCase() ) {
            result += when( ch ) {
                'A', 'B', 'C' -> "2"
                'D', 'E', 'F' -> "3"
                'G', 'H', 'I' -> "4"
                'J', 'K', 'L' -> "5"
                'M', 'N', 'O' -> "6"
                'P', 'Q', 'R', 'S' -> "7"
                'T', 'U', 'V' -> "8"
                'W', 'X', 'Y', 'Z' -> "9"
                else -> ""
            }

        }

        return result
    }

    fun convertToChosung(s: String): String {

        var result = ""

        val index = s.length
        for( i in 0 until index ) {          // 문자열의 길이만큼

            val ch = s[i]                    // 문자열의 한char형을 가져온다

            val hCh =
                JapaneseCharacter.toKatakana(
                    ch
                )
            result += when( hCh.toInt() ) {
                in 0xAC00..0xD7a3 -> {
                    val a = (hCh.toInt() - 0xAC00) / (21 * 28)        // 초성 구하는 공식
                    CHOSUNG[a]
                }
                in '！'.toInt()..'～'.toInt() -> {
                    hCh - 0xfee0
                }
                else -> hCh
            }

        }

        return result

    }

    private const val PATTERN_NUMBER = 100
    private const val PATTERN_ENGLISH = 101
    private const val PATTERN_KOREAN = 102
    private const val PATTERN_EMAIL = 103
    private const val PATTERN_SIGN_UP = 104
    private const val PATTERN_PWD = 105

    fun isPhoneNumber(phoneNumber: String?): Boolean {
        if( phoneNumber == null )
            return false

        return checkParameter(PATTERN_NUMBER, phoneNumber) && phoneNumber.length > 3

    }

    fun isEmail(email: String?): Boolean {

        return checkParameter(PATTERN_EMAIL, email )

    }

    fun isPwd(pwd: String?): Boolean {

        return checkParameter(PATTERN_PWD, pwd )

    }
    /**
     * 파라미터의 패턴을 체크하는 함수.
     * @param mode - 패턴 모드
     * @param inputStr - 입력 문자열
     * @return 패턴이 맞는지 체크하는 boolean
     */
    private fun checkParameter(mode: Int, inputStr: String?): Boolean {
        if( inputStr.isNullOrEmpty() ) return false
        var okPattern = false
        when(mode) {
            PATTERN_NUMBER -> {
                val patternRegex = "^[0-9]*$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }
            PATTERN_ENGLISH -> {
                val patternRegex = "^[a-zA-Z]*$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }
            PATTERN_KOREAN -> {
                val patternRegex = "^[ㄱ-ㅎ가-힣ㅏ-ㅣ]*$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }
            PATTERN_EMAIL -> {
                val patternRegex = "^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\\-+)|([A-Za-z0-9]+\\.+)|([A-Za-z0-9]+\\++))*[A-Za-z0-9]+@((\\w+\\-+)|(\\w+\\.))*\\w{1,63}\\.[a-zA-Z]{2,6}$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }
            PATTERN_SIGN_UP -> {
                val patternRegex = "^[a-zA-Z0-9]*$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }
            PATTERN_PWD -> {
//                val patternRegex = "^(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[!@#\$%^*()\\-_=+\\\\\\|\\[\\]{};:\\'\",.<>\\/?])[A-Za-z0-9!@#\$%^*()\\-_=+\\\\\\|\\[\\]{};:\\'\",.<>\\/?]{8,16}\$"
                val patternRegex = "^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9!@#\\\$%^*()\\\\-_=+\\\\\\\\\\\\|\\\\[\\\\]{};:\\\\'\\\",.<>\\\\/?]{8,16}\$"
                okPattern = Pattern.matches(patternRegex, inputStr)
            }

        }
        return okPattern
    }


}



