package baycon.android.common.utils.convertio

import com.softbay.android.baseproject.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class ConvertRetrofit {

    companion object {

        const val CONVERT_IO_URL = "https://api.convertio.co"
        const val CONVERT_IO_BASE_PATH = "/convert"

        private const val CONVERT_CONNECT_TIME_OUT: Long = 5
        private const val CONVERT_READ_TIME_OUT: Long = 60
        private const val CONVERT_WRITE_TIME_OUT: Long = 60

        private fun convertIoRetrofit(): Retrofit {
            return Retrofit.Builder()
                .baseUrl( CONVERT_IO_URL )
                .addConverterFactory( GsonConverterFactory.create() )
                .addConverterFactory( ScalarsConverterFactory.create() )
                .client( createOkHttpClient() )
                .build()
        }

        fun <T> createConvertIo( service: Class<T>): T {
            return convertIoRetrofit().create( service )
        }

        private fun createOkHttpClient(): OkHttpClient {

            val interceptor = HttpLoggingInterceptor()
            if ( BuildConfig.DEBUG ) {
                interceptor.level = HttpLoggingInterceptor.Level.BODY
            } else {
                interceptor.level = HttpLoggingInterceptor.Level.NONE
            }

            val builder = OkHttpClient.Builder().apply {
                addNetworkInterceptor( interceptor )
                connectTimeout( CONVERT_CONNECT_TIME_OUT, TimeUnit.SECONDS )
                readTimeout( CONVERT_READ_TIME_OUT, TimeUnit.SECONDS )
                writeTimeout( CONVERT_WRITE_TIME_OUT, TimeUnit.SECONDS )
            }

            return builder.build()

        }
    }
}