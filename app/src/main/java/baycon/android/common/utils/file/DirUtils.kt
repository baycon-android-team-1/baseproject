package baycon.android.common.utils.file

import android.content.Context
import java.io.File

object DirUtils {

    fun getSignFile( context:Context ):File {
        return File( context.filesDir,  "SignImage1.png" )
    }

    fun getSignDrawFile( context:Context ):File {
        return File( context.filesDir,  "SignImageDraw.png" )
    }


    fun getSavePayloadDir( context:Context, payload:String ):File {
        val tiffDir = File( context.cacheDir,  "list" )
        if( !tiffDir.exists() )
            tiffDir.mkdirs()

        return File( tiffDir,  "$payload.tif" )
    }

    fun getWriteTempDir( context:Context ):File {
        val dir = File( context.cacheDir, "convert" )
        if( !dir.exists() )
            dir.mkdirs()

        return dir

    }
}