package baycon.android.common.constant


object CommonCode {


    object SignType {
        const val IMAGE = 1
        const val TEXT = 2
        const val DRAW = 3
    }


    object FileType {
        const val IMG = 1
        const val PPT = 2
        const val DOC = 3
        const val CAPTURE = 4
        const val PDF = 5
        const val XLS = 6
    }

    object TermsCode {
        const val TERMS = "ATCT0001"
        const val CONDITIONS = "ATCT0002"

    }

    object NotificationCode {
        const val DEFAULT = 8001024
        const val SERVICE_SAFE = 8001025
        const val OUTBOUND_CALL = 8001026
        const val MISSED_CALL = 8001027
        const val INBOUND_CALL = 8001028
        const val INBOUND_CALL_V10 = 8001029

    }

    object RootingType {

        const val ROOTING = "1"
        const val NOT_ROOTING = "2"
        const val CHECK_ERROR = "3"
    }

    object CallType {
        const val OUTGOING_CALL = 0
        const val INCOMING_CALL = 1
        const val MISSED_CALL = 2
        const val MISSED_CALL_WAIT = 3
        const val INTERCEPT_CALL = 4

    }


    object InboundEndType {
        const val ANSWER = "0"
        const val REJECT = "1"
        const val BUSY = "2"
        const val NO_ANSWER = "3"
    }

    object NotificationChannel {


        const val NOTIFICATION_ID_EVENT = "NOTIFICATION_ID_FAX"
        const val NOTIFICATION_NAME_EVENT = "Fax"

    }

    object MsgType {

        const val FAX_SEND_OUTBOUND_FAIL = "51"
        const val FAX_SEND_OUTBOUND_SUCCESS = "52"
        const val FAX_SEND_INBOUND_SUCCESS = "54"

        const val EXPIRE_FAX_NUMBER = "7773"
    }


    object PushData {
        const val PUSH_TYPE_NORMAL = "1"
        const val PUSH_TYPE_LINK = "2"

        const val PUSH_KEY_TYPE = "T"
        const val PUSH_KEY_URL = "U"
        const val PUSH_KEY_NAME = "N"
        const val PUSH_KEY_CONTENT = "C"

        const val PUSH_KEY_MSG_TYPE = "MT"
        const val PUSH_KEY_UNIQUE_ID = "PL"
        const val PUSH_KEY_CALL_START = "NU"
        const val PUSH_KEY_CALLER_NATIONAL_ID = "NI"
        const val PUSH_KEY_CALLER_PHONE_ID = "PI"
        const val PUSH_KEY_CALLED_NATIONAL_ID = "DNI"
        const val PUSH_KEY_CALLED_PHONE_ID = "DPI"
    }

    object ContactCheckState {
        const val NONE = 0
        const val CHECK = 1
        const val CHECKED = 2

    }

    object LoginType {
        const val EMAIL = "UAT0001"
        const val GOOGLE = "UAT0002"
        const val APPLE = "UAT0003"
    }
}