package baycon.android.common.constant.sharedpreference

import android.content.Context
import android.content.SharedPreferences

class AppPreference constructor(private val context: Context) {

    private val sharedPreferences: SharedPreferences = context.getSharedPreferences("sharedPreferences", 0)

    var prefString: String?
        get() = sharedPreferences.getString("prefString", null)
        set(value) = sharedPreferences.edit().putString("prefString", value).apply()

    var prefInt: Int
        get() = sharedPreferences.getInt("prefInt", 0)
        set(value) = sharedPreferences.edit().putInt("prefInt", value).apply()

    var prefBoolean: Boolean
        get() =  sharedPreferences.getBoolean("prefBoolean", false)
        set(value) = sharedPreferences.edit().putBoolean("prefBoolean", value).apply()

    var prefLong: Long
        get() = sharedPreferences.getLong("prefLong", 0)
        set(value) = sharedPreferences.edit().putLong("prefLong", value).apply()

    var prefFloat: Float
        get() = sharedPreferences.getFloat("prefFloat", 0f)
        set(value) = sharedPreferences.edit().putFloat("prefFloat", value).apply()

    /**
     * 기본 이벤트 노티 채널 생성 유무
     */
    var isCreateEventNotification : Boolean
        get() = sharedPreferences.getBoolean( "isCreateEventNotification", false )
        set(value) = sharedPreferences.edit().putBoolean( "isCreateEventNotification", value).apply()

    /**
     * 주소록 업데이트 여부
     */
    var isContactUpdate: Boolean
        get() = sharedPreferences.getBoolean("isContactUpdate", false)
        set(value) = sharedPreferences.edit().putBoolean("isContactUpdate", value).apply()


}