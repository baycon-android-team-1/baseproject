package baycon.android.common.api.contract

import baycon.android.common.api.Resource
import retrofit2.Response
import timber.log.Timber

abstract class BaseDataSource {

    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.success(
                    body
                )
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error("Network call has failed for a following reason: $message")
    }


    protected suspend fun <T> getConvertResult (call: suspend  () -> Response<T> ): T? {

        val response = call()

        Timber.d("convertIo :: response result : ${response.isSuccessful}")

        if ( response.isSuccessful ) {

            val body = response.body()
            if (body != null) {

                return body

            }

        }
        return null

    }

}