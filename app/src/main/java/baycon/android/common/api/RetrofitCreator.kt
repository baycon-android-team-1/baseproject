package baycon.android.common.api

import com.softbay.android.baseproject.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitCreator {

    companion object {

        const val BASE_URL = "https://jsonplaceholder.typicode.com"

        private fun defaultRetrofit( baseUrl:String, connectTimeout:Long, readTimeout:Long, writeTimeout:Long ): Retrofit {
            return Retrofit.Builder()
                .baseUrl( baseUrl )
                .addConverterFactory( GsonConverterFactory.create() )
                .addConverterFactory( ScalarsConverterFactory.create() )
                .client( createOkHttpClient( connectTimeout, readTimeout, writeTimeout ) )
                .build()
        }

        fun <T> createDefault(service: Class<T>, baseUrl:String, connectTimeout:Long = 5, readTimeout:Long = 5, writeTimeout:Long = 1 ): T {
            return defaultRetrofit( baseUrl, connectTimeout, readTimeout, writeTimeout ).create( service )
        }

        private fun createOkHttpClient( connectTimeout:Long, readTimeout:Long, writeTimeout:Long ): OkHttpClient {

            val interceptor = HttpLoggingInterceptor()
            if( BuildConfig.DEBUG ) {
                interceptor.level = HttpLoggingInterceptor.Level.BODY
            } else {
                interceptor.level = HttpLoggingInterceptor.Level.NONE
            }

            val builder = OkHttpClient.Builder();
            builder.addNetworkInterceptor(interceptor)
            builder.connectTimeout( connectTimeout, TimeUnit.SECONDS )
            builder.readTimeout( readTimeout, TimeUnit.SECONDS )
            builder.writeTimeout( writeTimeout, TimeUnit.SECONDS )

            return builder.build()

        }

    }

}