package baycon.android.common.api

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


fun <T> performOperation(networkCall: suspend () -> Resource<T>, saveCallResult: (suspend (T) -> Unit)? = null, uiCallResult: (suspend (T) -> Unit)? = null ): LiveData<Resource<T>> =
        liveData( Dispatchers.IO ) {
            emit( Resource.loading() )
            val responseStatus = networkCall.invoke()
            if( responseStatus.status == Resource.Status.SUCCESS ) {
                responseStatus.data?.let { saveCallResult?.invoke( it ) }

                uiCallResult?.let { uiFun ->
                    withContext( Dispatchers.Main ) {
                        responseStatus.data?.let { uiFun.invoke( it ) }
                    }
                }

            }
            emit( responseStatus )
        }

//fun <T> performOperation(networkCall: suspend () -> Resource<T>, saveCallResult: suspend (T) -> Unit ): LiveData<Resource<T>> =
//    liveData( Dispatchers.IO ) {
//        emit( Resource.loading() )
//        val responseStatus = networkCall.invoke()
//        if( responseStatus.status == Resource.Status.SUCCESS ) {
//            withContext( Dispatchers.Main ) {
//                saveCallResult(responseStatus.data!!)
//            }
//        }
//        emit( responseStatus )
//    }
//
//
//fun <T> performOperation( networkCall: suspend () -> Resource<T>): LiveData<Resource<T>> =
//    liveData( Dispatchers.IO ) {
//        emit( Resource.loading() )
//        emit( networkCall.invoke() )
//    }
